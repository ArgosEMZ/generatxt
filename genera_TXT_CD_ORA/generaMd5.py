from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_MD5(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cd-qa"
    BUCKET = 'amco-cd-qa'
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        select  ID_PAIS as ID_PAIS, -- ID_TIENDA
                DS_PAIS as DS_PAIS
        from amco-cd-qa.GRAL.DIM_PAIS_TXT
        where id_pais not in (760, 759)
        """
    )
    resId = query_id.result()
    
    print ("Inicia a generar los MD5_")
    for reg in resId:
        
        nombreArchivo_S = "Usuarios_Eliminados_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_S     = "Usuarios_Eliminados_"+reg.DS_PAIS+"_"+fecha+".md5"

        nombreArchivo_C = "Estado_Medio_Pago_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_C     = "Estado_Medio_Pago_"+reg.DS_PAIS+"_"+fecha+".md5"
        
        nombreArchivo_D = "Consumos_Pyme_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_D     = "Consumos_Pyme_"+reg.DS_PAIS+"_"+fecha+".md5"
        
        nombreArchivo_R = "Consumos_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_R     = "Consumos_"+reg.DS_PAIS+"_"+fecha+".md5"
        
        nombreArchivo_U = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_U     = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".md5"
        
        nombreArchivo_P = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_P     = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".md5"
        
        ################### Generar archivo MD5 ######################
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_S+"  | awk '{ print $1 }' > "+nombreMD5_S)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_C+"  | awk '{ print $1 }' > "+nombreMD5_C)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_D+"  | awk '{ print $1 }' > "+nombreMD5_D)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_R+"  | awk '{ print $1 }' > "+nombreMD5_R)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_U+"  | awk '{ print $1 }' > "+nombreMD5_U)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_P+"  | awk '{ print $1 }' > "+nombreMD5_P)
        
    print ("Se termino de generar los MD5_")
