from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os
import logging

def genera_archivos(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    logging.basicConfig(level = logging.INFO, filename = path_arc+'txt_Log.log', format= '[%(levelname)s] (%(threadName)-s %(message)s')
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cd-qa"
    BUCKET = 'amco-cd-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        select  ID_PAIS as ID_PAIS, -- ID_TIENDA
                DS_PAIS as DS_PAIS
        from amco-cd-qa.GRAL.DIM_PAIS_TXT
        where id_pais not in (760, 759)
        """
    )
    resId = query_id.result()
    
    for reg in resId:
        
        nombreArchivo = "Estado_Medio_Pago_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5 = "Estado_Medio_Pago_"+reg.DS_PAIS+"_"+fecha+".md5"
        nombreCtl = "Estado_Medio_Pago_"+reg.DS_PAIS+"_"+fecha+".ctl"
        nombreZip = "Estado_Medio_Pago_"+reg.DS_PAIS+"_"+fecha+".zip"

        #Query para obtener los datos
        client = bigquery.Client(project=PROYECTO)
        query_job = client.query(
            """
            SELECT  DISTINCT(
                    CONCAT('"',
                    IFNULL(REPLACE(CAST(PAIS AS STRING),'"','""')  ,''),'"|"',
                    IFNULL(REPLACE(CAST(ID_CLIENTE AS STRING),'"','""')  ,''),'"|"',
                    IFNULL(REPLACE(CAST(MSISDN AS STRING),'"','""')  ,''),'"|"',
                    IFNULL(REPLACE(CAST(FORMAT_DATETIME("%Y-%m-%d %H:%M:%S.0", FECHA_ALTA) AS STRING),'"','""')  ,''),'"|"',
                    IFNULL(REPLACE(CAST(MEDIO_PAGO_NOMBRE AS STRING),'"','""')  ,''),'"|"',
                    IFNULL(REPLACE(CAST(COD_ORIGEN_PAGO AS STRING),'"','""')  ,''),'"|"',
                    IFNULL(REPLACE(CAST(TX_ORIGEN_PAGO AS STRING),'"','""')  ,''),'"|"',
                    IFNULL(REPLACE(CAST(PAQUETE AS STRING),'"','""')  ,''),'"|"',
                    IFNULL(REPLACE(CAST(ESTADO_MEDIO_PAGO AS STRING),'"','""')  ,''),'"'
                    )) AS dato
            FROM amco-cd-qa.GRAL.TXT_MEDIO_PAGO
            WHERE PAIS = '"""+str(reg.DS_PAIS)+"""'
            --WHERE TXT_ESTADO_MEDIO_PAGO_VW.ID_FECHA = DATE_SUB(DATE_TRUNC(CURRENT_DATETIME('America/Mexico_City'),DAY), INTERVAL 1 DAY)
            """
        )

        results = query_job.result()

        f = open(path_arc+'ArchivosTXT/' + nombreArchivo, 'w' ,encoding='UTF8')
        ## Primero escribir el cabecero
        f.write('"PAIS"|"ID_CLIENTE"|"MSISDN"|"FECHA_ALTA"|"MEDIO_PAGO_NOMBRE"|"COD_ORIGEN_PAGO"|"TX_ORIGEN_PAGO"|"PAQUETE"|"ESTADO_MEDIO_PAGO"')
        f.write('\r\n')    
        
        for row in results:
            registro = row.dato
            f.write(registro)
            f.write('\r\n')
            totalReg += 1
            
        f.close()    
        print ("Se genero el archivo correctamente: " + nombreArchivo)
        logging.info(str(datetime.now())+': '+"Se genero el archivo correctamente: " + nombreArchivo)
        print ("Total de registros " + str(totalReg))
        logging.info(str(datetime.now())+': '+"Total de registros " + str(totalReg))
    
        ################### Generar archivo CTL ######################
        os.system("echo \""+str(totalReg)+"\" >> "+path_arc+"ArchivosTXT/"+nombreCtl)

        totalReg = 0
