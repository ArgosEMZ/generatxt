from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os
import logging

def validaCarga(diasAtras,key_path,path_arc):
    
    logging.basicConfig(level = logging.INFO, filename = path_arc+'txt_Log.log', format= '[%(levelname)s] (%(threadName)-s %(message)s')
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    #fecha = datetime.now() + timedelta(days=-diasAtras)
    #fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cd-qa"
    BUCKET = 'amco-cd-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    v_per_carga = 0
    
    #diasAtrasQ = diasAtras + 1
    
    REG_TXT_USR = 0
    REG_TXT_SUS = 0
    REG_TXT_CON = 0
    REG_TXT_CON_PYME = 0
    REG_TXT_MED_PAG = 0
    REG_TXT_USR_ELI = 0
    REG_TXT_CD_DIA_ORA = 0
    
    EST_TXT_USR = ''
    EST_TXT_SUS = ''
    EST_TXT_CON = ''
    EST_TXT_CON_PYME = ''
    EST_TXT_MED_PAG = ''
    EST_TXT_USR_ELI = ''
    EST_TXT_CD_DIA_ORA = ''
    
    FD_TXT_USR = ''
    FD_TXT_SUS = ''
    FD_TXT_CON = ''
    FD_TXT_CON_PYME = ''
    FD_TXT_MED_PAG = ''
    FD_TXT_USR_ELI = ''
    FD_TXT_CD_DIA_ORA = ''
    
    fechaMax = datetime.now() + timedelta(days=-100)
    ultimoPro = ''
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE NOMBRE_CARGA = 'TXT_USUARIOS'
        AND DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL 0 DAY), DAY)
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE NOMBRE_CARGA = 'TXT_SUSCRIPCIONES'
        AND DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL 0 DAY), DAY)
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE NOMBRE_CARGA = 'TXT_CONSUMOS'
        AND DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL 0 DAY), DAY)
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE NOMBRE_CARGA = 'TXT_CONSUMOS_PYME'
        AND DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL 0 DAY), DAY)
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE NOMBRE_CARGA = 'TXT_MEDIO_PAGO'
        AND DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL 0 DAY), DAY)
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE NOMBRE_CARGA = 'TXT_USUARIOS_ELIMINADOS'
        AND DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL 0 DAY), DAY)
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE NOMBRE_CARGA = 'TXT_CD_DIARIO_ORA'
        AND DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL 0 DAY), DAY)
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        ORDER BY FECHA_CARGA DESC
        """
    )
    resId = query_id.result()
    
    for reg in resId:
        # Asignacion de variables 
        if reg.NOMBRE_CARGA == 'TXT_USUARIOS':
            REG_TXT_USR = reg.REGISTROS_AFECTADOS
            EST_TXT_USR = reg.ESTADO_CARGA
            FD_TXT_USR = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_SUSCRIPCIONES':
            REG_TXT_SUS = reg.REGISTROS_AFECTADOS
            EST_TXT_SUS = reg.ESTADO_CARGA
            FD_TXT_SUS = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_CONSUMOS':
            REG_TXT_CON = reg.REGISTROS_AFECTADOS
            EST_TXT_CON = reg.ESTADO_CARGA
            FD_TXT_CON = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_CONSUMOS_PYME':
            REG_TXT_CON_PYME = reg.REGISTROS_AFECTADOS
            EST_TXT_CON_PYME = reg.ESTADO_CARGA
            FD_TXT_CON_PYME = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_MEDIO_PAGO':
            REG_TXT_MED_PAG = reg.REGISTROS_AFECTADOS
            EST_TXT_MED_PAG = reg.ESTADO_CARGA
            FD_TXT_MED_PAG = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_USUARIOS_ELIMINADOS':
            REG_TXT_USR_ELI = reg.REGISTROS_AFECTADOS
            EST_TXT_USR_ELI = reg.ESTADO_CARGA
            FD_TXT_USR_ELI = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_CD_DIARIO_ORA':
            REG_TXT_CD_DIA_ORA = reg.REGISTROS_AFECTADOS
            EST_TXT_CD_DIA_ORA = reg.ESTADO_CARGA
            FD_TXT_CD_DIA_ORA = reg.FECHA_CARGA
            
        if reg.FECHA_CARGA >= fechaMax:
            fechaMax = reg.FECHA_CARGA
            ultimoPro = reg.NOMBRE_CARGA
    
    logging.info('Ultimo proceso ejecutado: ' + ultimoPro)
    logging.info('Hora de ultima actividad del proceso: ' + str(fechaMax))
    
    if ultimoPro != 'TXT_CD_DIARIO_ORA':
        if EST_TXT_CD_DIA_ORA == 'INICIO':
            logging.info('El proceso de generacion de TXT esta corriendo')
        else:
            logging.info('El proceso se puede ejecutar despues de evaluar que todas las tablas tienen registros')
            if REG_TXT_USR > 0 and REG_TXT_SUS > 0 and REG_TXT_CON > 0 and REG_TXT_CON_PYME > 0 and REG_TXT_MED_PAG > 0 and REG_TXT_USR_ELI > 0:
                logging.info('Se valido que todas las tablas estan cargadas....')
                logging.info('Inserta en la ETL Log para avisar del inicio de la generacion de los TXT')
                client = bigquery.Client(project=PROYECTO)
                query_id = client.query(
                    """
                    call GRAL.SP_INSERT_INICIO_ETL_LOG ('TXT_CD_DIARIO_ORA',
                                                        'Inicio TXT', 
                                                        'ST_REP', 
                                                        'ST_REP',
                                                        'CRON', 
                                                        'LINUX', 
                                                        0, 
                                                        'Inicia el proceso de generar los TXT', 
                                                        'Inicia todo correcto', 
                                                        0) 
                    """
                    )
                
                resId = query_id.result()
                logging.info('Se inicia la generacion de los TXT =D')
                v_per_carga = 1
            else:
                logging.info('Los TXT no se pueden ejecutar por que no todas las tablas tienen registros')
             
    elif ultimoPro == 'TXT_CD_DIARIO_ORA' and EST_TXT_CD_DIA_ORA == 'INICIO':
        logging.info('El proceso de generacion de TXT esta corriendo')
        
    elif ultimoPro == 'TXT_CD_DIARIO_ORA' and EST_TXT_CD_DIA_ORA == 'FIN':
        logging.info('El proceso de generacion de TXT ya se ejecuto')
        
    return v_per_carga
