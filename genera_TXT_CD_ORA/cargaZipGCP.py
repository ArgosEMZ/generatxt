from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def carga_zip(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    ##############################################################
    ##################### Carga de archivos ######################
    
    # Nombre del proyecto
    PROYECTO = "amco-cd-qa"
    BUCKET = 'amco-cd-qa-txt'
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
                            """
                            select  ID_PAIS as ID_PAIS, -- ID_TIENDA
                                    DS_PAIS as DS_PAIS
                            from amco-cd-qa.GRAL.DIM_PAIS_TXT
                            where id_pais not in (760, 759)
                            """
                           )
    resId = query_id.result()
    
    for reg in resId:
        nombreZip = "txt_"+reg.DS_PAIS+"_"+fecha+".zip"
        
        # Path de lectura del archivo
        source_file_name = path_arc+'ArchivosTXT/'+nombreZip
        # Destino y nombre del archivo
        destination_blob_name = "TXT/" + nombreZip

        storage_client = storage.Client(project=PROYECTO)
        bucket = storage_client.bucket(BUCKET)
        blob = bucket.blob(destination_blob_name)

        blob.upload_from_filename(source_file_name)

        print(
            "Archivo {} cargado en {}.".format(
                source_file_name, destination_blob_name
            )
        )
