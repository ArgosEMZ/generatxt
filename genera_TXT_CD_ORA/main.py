from datetime import date, datetime, timedelta
from numpy import random
from time import sleep
import os
import threading
import logging

import TXT_SUSCRIPCIONES as TXT_SUSCRIPCIONES
import TXT_USUARIOS as TXT_USUARIOS
import TXT_CONSUMOS as TXT_CONSUMOS
import TXT_CONSUMOS_PYME as TXT_CONSUMOS_PYME
import TXT_MEDIO_PAGO as TXT_MEDIO_PAGO
import TXT_USUARIOS_ELIMINADOS as TXT_USUARIOS_ELIMINADOS

import generaZIP as generaZIP
import cargaZipGCP as cargaZipGCP
import generaMd5 as generaMd5
import confirmaCargaGCP as confirmaCargaGCP
import validarEjecucion as validarEjecucion

###### Desarollo ###### 
#path_arc = "/home/jupyter/script_TXT/genera_TXT_CD_ORA/"
#key_path = "/home/jupyter/accesos/amco-cd-qa-df33df0378be.json"

###### Produccion ###### ArchivosTXT
path_arc = "/home/argos_amco_mx/generatxt/genera_TXT_CD_ORA/"
key_path = "/home/argos_amco_mx/accesos/amco-cd-qa-df33df0378be.json"

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path

logging.basicConfig(level = logging.INFO, filename = path_arc+'txt_Log.log', format= '[%(levelname)s] (%(threadName)-s %(message)s')

sleeptime = random.uniform(1, 3)
print ("sleeping de:"+ str(sleeptime)+ "segundos")
logging.info(str(datetime.now())+': '+"sleeping de:"+ str(sleeptime)+ "segundos")
sleep(sleeptime)
print("sleeping termino")
logging.info(str(datetime.now())+': '+"sleeping termino")

# Variable que nos indica con que fecha se tiene que generar el archivo (Numero de dias atras)
diasAtras = 5

######### Muestra un print con la informacion a procesar ###########
fecha = datetime.now() + timedelta(days=-diasAtras)
fecha = fecha.strftime("%Y%m%d")
diasAtrasQ = diasAtras + 1
fechaQ = datetime.now() + timedelta(days=-diasAtrasQ)
fechaQ = fechaQ.strftime("%Y%m%d")
logging.info(str(datetime.now())+': '+'El archivo se creara con la fecha: ' + fecha)
logging.info(str(datetime.now())+': '+'El query se ejecuta con la fecha: ' + fechaQ)
v_per_carga = 0

v_per_carga = validarEjecucion.validaCarga(diasAtras, key_path, path_arc)

logging.info(str(datetime.now())+': '+'El resultado de la funcion de PY es: ' + str(v_per_carga))

if v_per_carga == 1:
    ################### Limpia espacio de trabajo ######################
    os.system('mkdir '+path_arc+'ArchivosTXT')
    os.system('cd '+path_arc+'ArchivosTXT/; '+ 'sudo rm *')

    ####################### Genera los TXT #############################
    logging.info(str(datetime.now())+': '+"Inicia a crear los TXT")
    t1 = threading.Thread(name="TXT_SUSCRIPCIONES", target=TXT_SUSCRIPCIONES.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t2 = threading.Thread(name="TXT_USUARIOS", target=TXT_USUARIOS.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t3 = threading.Thread(name="TXT_CONSUMOS", target=TXT_CONSUMOS.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t4 = threading.Thread(name="TXT_CONSUMOS_PYME", target=TXT_CONSUMOS_PYME.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t5 = threading.Thread(name="TXT_MEDIO_PAGO", target=TXT_MEDIO_PAGO.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t6 = threading.Thread(name="TXT_USUARIOS_ELIMINADOS", target=TXT_USUARIOS_ELIMINADOS.genera_archivos, args=(diasAtras, key_path, path_arc,))
    
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t5.start()
    t6.start()

    t1.join()
    t2.join()
    t3.join()
    t4.join()
    t5.join()
    t6.join()

    logging.info(str(datetime.now())+': '+"Termino de crear los TXT")
    ##################### Carga los archivos ###########################
    ########renombraArchivos.renombra_arc(diasAtras)
    logging.info(str(datetime.now())+': '+"Inicia a generar los MD5")
    generaMd5.genera_MD5(diasAtras, key_path, path_arc)
    logging.info(str(datetime.now())+': '+"Inicia a generar los ZIP")
    generaZIP.genera_zip(diasAtras, key_path, path_arc)
    logging.info(str(datetime.now())+': '+"Carga los ZIP a GCP")
    cargaZipGCP.carga_zip(diasAtras, key_path, path_arc)

    ############# Confirma la carga de los archivos a GCP ################
    logging.info(str(datetime.now())+': '+"Manda archivo de confirmacion a GCP")
    confirmaCargaGCP.confirmaCargaGCP(diasAtras, key_path, path_arc)
