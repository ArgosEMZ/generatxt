from google.cloud import bigquery
from google.cloud import storage
from openpyxl import Workbook
from openpyxl import load_workbook
from datetime import date, datetime, timedelta
import os
import pandas as pd
import gc

def crea_csv_activos(diasAtras):
    ###### Desarollo ###### 
    #path_arc = "/home/jupyter/script_TXT/genera_CVMAX/"
    #key_path = "/home/jupyter/accesos/amco-cv-qa-d7be3f5c7e10.json"

    ###### Produccion ###### ArchivosTXT
    path_arc = "/home/txtamco/generatxt/genera_CVMAX/"
    key_path = "/home/txtamco/accesos/amco-cv-qa-d7be3f5c7e10.json"

    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path

    #diasAtras = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y-%m-%dT%H_%M_%S.000Z")
    PROYECTO = "amco-cv-qa"
    BUCKET = 'amco-cv-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"

    ################### Limpia espacio de trabajo ######################
    os.system('mkdir '+path_arc+'ArchivosTXT')
    os.system('cd '+path_arc+'ArchivosTXT/; '+ 'rm -r '+path_arc+'ArchivosTXT/*')

    ################# Establece la ruta del archivo ####################
    nombreArchivo = 'Reporte_Telcel_Cvmax_'+ str(fecha) + '.xlsx'
    nomArchivo = path_arc+'ArchivosTXT/' + nombreArchivo
    print ('Nombre archivo: '+nomArchivo)

    ################### Inicia un Libro de excel #######################
    wb = Workbook()

    ################################################################################################
    ############### Activos ############### Activos ################### Activos ####################
    ################################################################################################
    print('Se iniciar a crear la hoja de Activos')

    sheet = wb.create_sheet('Activos', 0)

    ##Se escribe el cabecero##
    reggistroA=['ID_SUBSCRIPCION','USUARIO_ID','FECHA_ALTA','FECHA_ULTIMA_RENOVACION',
                'FECHA_CANCELACION','FECHA_TERMINADO','MONTO_SUBSCRIPCION','MEDIO_DE_PAGO',
                'TELEFONO','PAQUETE','CODIGO','CENTRO_VENTAS','NOMBRE_USUARIO','HERRAMIENTA',
                'CODE','FECHA_ACEPTACION_TERMINOS','STATUS']
    sheet.append(reggistroA)

    print ('Se ejecuta el query')
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query("""
    SELECT  CAST(ID_SUBSCRIPCION as int64) AS ID_SUBSCRIPCION, 
            CAST(USUARIO_ID as int64) AS USUARIO_ID, 
            CAST(FECHA_ALTA as int64) AS FECHA_ALTA, 
            CAST(FECHA_ULTIMA_RENOVACION as int64) AS FECHA_ULTIMA_RENOVACION, 
            FECHA_CANCELACION, 
            FECHA_TERMINADO, 
            CAST(MONTO_SUBSCRIPCION AS INT64) AS MONTO_SUBSCRIPCION, 
            MEDIO_DE_PAGO, 
            TELEFONO, 
            PAQUETE, 
            CODIGO, 
            CENTRO_VENTAS, 
            NOMBRE_USUARIO, 
            HERRAMIENTA, 
            CODE, 
            CAST(FECHA_ACEPTACION_TERMINOS AS INT64) AS FECHA_ACEPTACION_TERMINOS, 
            STATUS 
    FROM amco-cv-qa.GRAL.FAC_CVMAX_ACTIVOS

    """
    )
    print ('Se obtienen los resultados del query')
    resId = query_id.result()

    print ('Inicia a escribir el resultado en el Archivo')
    for row in resId:
        reggistroA = [row.ID_SUBSCRIPCION,
                      row.USUARIO_ID,
                      row.FECHA_ALTA,
                      row.FECHA_ULTIMA_RENOVACION,
                      row.FECHA_CANCELACION,
                      row.FECHA_TERMINADO,
                      row.MONTO_SUBSCRIPCION,
                      row.MEDIO_DE_PAGO,
                      row.TELEFONO,
                      row.PAQUETE,
                      row.CODIGO,
                      row.CENTRO_VENTAS,
                      row.NOMBRE_USUARIO,
                      row.HERRAMIENTA,
                      row.CODE,
                      row.FECHA_ACEPTACION_TERMINOS,
                      row.STATUS
                     ]
        sheet.append(reggistroA)

    print ('Guarda el resultado en el archivo')
    wb.save(nomArchivo)
    gc.collect()
    print('Termino de crear la hoja de Activos')

    ################################################################################################
    ############ Cancelados ############## Cancelados ################### Cancelados ###############
    ################################################################################################
    print('Inicia a crear la hoja de Cancelados')

    sheet = wb.create_sheet('Cancelados', 1)

    ##Se escribe el cabezero##
    reggistroA=[
                'ID_SUBSCRIPCION','USUARIO_ID','FECHA_ALTA','FECHA_ULTIMA_RENOVACION',
                'FECHA_CANCELACION','FECHA_TERMINADO','MONTO_SUBSCRIPCION','MEDIO_DE_PAGO',
                'TELEFONO','PAQUETE','CODIGO','CENTRO_VENTAS','NOMBRE_USUARIO','HERRAMIENTA',
                'CODE','FECHA_ACEPTACION_TERMINOS','STATUS'
               ]
    sheet.append(reggistroA)

    client = bigquery.Client(project=PROYECTO)
    query_id = client.query("""
    SELECT  CAST(ID_SUBSCRIPCION AS INT64) AS ID_SUBSCRIPCION, 
            CAST(USUARIO_ID AS INT64) AS USUARIO_ID, 
            CAST(FECHA_ALTA AS INT64) AS FECHA_ALTA, 
            CAST(FECHA_ULTIMA_RENOVACION AS INT64) AS FECHA_ULTIMA_RENOVACION, 
            FECHA_CANCELACION, 
            CAST(FECHA_TERMINADO AS INT64) AS FECHA_TERMINADO, 
            CAST(MONTO_SUBSCRIPCION AS INT64) AS MONTO_SUBSCRIPCION, 
            MEDIO_DE_PAGO, 
            TELEFONO, 
            PAQUETE, 
            CODIGO, 
            CENTRO_VENTAS, 
            NOMBRE_USUARIO, 
            HERRAMIENTA, 
            CODE, 
            CAST(FECHA_ACEPTACION_TERMINOS AS INT64) AS FECHA_ACEPTACION_TERMINOS, 
            STATUS 
    FROM amco-cv-qa.GRAL.FAC_CVMAX_CANCELACIONES

    """
    )
    resId = query_id.result()

    print ('Inicia a escribir el resultado en el Archivo')
    for row in resId:
        reggistroA = [row.ID_SUBSCRIPCION,
                      row.USUARIO_ID,
                      row.FECHA_ALTA, 
                      row.FECHA_ULTIMA_RENOVACION, 
                      row.FECHA_CANCELACION, 
                      row.FECHA_TERMINADO, 
                      row.MONTO_SUBSCRIPCION, 
                      row.MEDIO_DE_PAGO, 
                      row.TELEFONO, 
                      row.PAQUETE, 
                      row.CODIGO, 
                      row.CENTRO_VENTAS, 
                      row.NOMBRE_USUARIO, 
                      row.HERRAMIENTA, 
                      row.CODE, 
                      row.FECHA_ACEPTACION_TERMINOS, 
                      row.STATUS
                     ]
        sheet.append(reggistroA)

    wb.save(nomArchivo)
    gc.collect()
    print('Termina de crear la hoja de Cancelados')

    ################################################################################################
    ############# Transacciones ############ Transacciones ################ Transacciones ##########
    ################################################################################################
    print('Inicia a crear la hoja de Transacciones')

    sheet = wb.create_sheet('Transacciones', 2)

    ##Se escribe el cabezero##
    reggistroA=[
                'ID_RENTA','USUARIO_ID','FECHA_RENTA','HORA_RENTA','ID_GRUPO',
                'TITULO_ADQUIRIDO','PROVEEDOR_CONTENIDO','TYPE_CONTENIDO','MONTO',
                'MEDIO_PAGO','TIPO_USUARIO','TELEFONO','FECHA_ACEPTACION_TERMINOS'
               ]
    sheet.append(reggistroA)

    client = bigquery.Client(project=PROYECTO)
    query_id = client.query("""
    SELECT  ID_RENTA as ID_RENTA,
            CAST(USUARIO_ID AS INT64) AS USUARIO_ID, 
            CAST(FECHA_RENTA AS DATETIME) AS FECHA_RENTA, 
            HORA_RENTA, 
            ID_GRUPO, 
            TITULO_ADQUIRIDO, 
            PROVEEDOR_CONTENIDO, 
            TYPE_CONTENIDO, 
            CAST(MONTO AS INT64) AS MONTO, 
            MEDIO_PAGO, 
            TIPO_USUARIO, 
            TELEFONO, 
            CAST(FECHA_ACEPTACION_TERMINOS AS INT64) AS FECHA_ACEPTACION_TERMINOS 
    FROM amco-cv-qa.GRAL.FAC_CVMAX_TRANSACCIONES

    """
    )
    resId = query_id.result()

    print ('Inicia a escribir el resultado en el Archivo')
    for row in resId:
        reggistroA = [row.ID_RENTA,
                      row.USUARIO_ID, 
                      row.FECHA_RENTA, 
                      row.HORA_RENTA, 
                      row.ID_GRUPO, 
                      row.TITULO_ADQUIRIDO, 
                      row.PROVEEDOR_CONTENIDO, 
                      row.TYPE_CONTENIDO, 
                      row.MONTO, 
                      row.MEDIO_PAGO, 
                      row.TIPO_USUARIO, 
                      row.TELEFONO, 
                      row.FECHA_ACEPTACION_TERMINOS 
                     ]
        sheet.append(reggistroA)

    wb.save(nomArchivo)
    gc.collect()
    print('Termina de crear la hoja de Transacciones')

    ################################################################################################
    ############# Usuarios Unicos Transacciones ############ Usuarios Unicos Transacciones #########
    ################################################################################################
    print('Inicia a crear la hoja de Usuarios Unicos Transacciones')

    sheet = wb.create_sheet('Usuarios Unicos Transacciones', 3)

    ##Se escribe el cabezero##
    reggistroA=[
                'USUARIOS_UNICOS_TRANSACCIONES'
               ]
    sheet.append(reggistroA)

    client = bigquery.Client(project=PROYECTO)
    query_id = client.query("""
    SELECT  COUNT(DISTINCT(CAST(USUARIO_ID AS INT64))) AS USUARIOS_UNICOS_TRANSACCIONES
    FROM amco-cv-qa.GRAL.FAC_CVMAX_TRANSACCIONES
    """
    )
    resId = query_id.result()

    print ('Inicia a escribir el resultado en el Archivo')
    for row in resId:
        reggistroA = [
                      row.USUARIOS_UNICOS_TRANSACCIONES
                     ]
        sheet.append(reggistroA)

    wb.save(nomArchivo)

    wb.remove(wb['Sheet'])
    #wb.remove_sheet(Sheet)
    wb.save(nomArchivo)
    gc.collect()
    print('Termina de crear la hoja de Usuarios Unicos Transacciones')

    print ('Se termino de generar el archivo')
    
    ##############################################################
    ##############################################################
    ##############################################################
    ##################### Carga de archivos ######################
    
    # Nombre del proyecto
    PROYECTO = "amco-cv-qa"
    BUCKET = 'amco-cv-qa-txt'


    # Path de lectura del archivo
    source_file_name = path_arc+'ArchivosTXT/'+nombreArchivo
    # Destino y nombre del archivo
    destination_blob_name = "CVMAX/" + nombreArchivo

    storage_client = storage.Client(project=PROYECTO)
    bucket = storage_client.bucket(BUCKET)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print(
        "Archivo {} cargado en {}.".format(
            source_file_name, destination_blob_name
        )
    )