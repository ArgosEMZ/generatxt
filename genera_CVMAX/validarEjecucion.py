from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os
import logging

def validaCarga(diasAtras,key_path,path_arc):
    
    logging.basicConfig(level = logging.INFO, filename = path_arc+'txt_Log.log', format= '[%(levelname)s] (%(threadName)-s %(message)s')
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    #fecha = datetime.now() + timedelta(days=-diasAtras)
    #fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cv-qa"
    BUCKET = 'amco-cv-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    v_per_carga = 0
    
    #diasAtrasQ = diasAtras + 1
    
    REG_CARGAS_CVMAX = 0
    EST_CARGAS_CVMAX = ''
    FD_CARGAS_CVMAX = ''
    REG_CVMAX_ACTIVOS = 0
    EST_CVMAX_ACTIVOS = ''
    FD_CVMAX_ACTIVOS = ''
    REG_CVMAX_TRANSACCIONES = 0
    EST_CVMAX_TRANSACCIONES = ''
    FD_CVMAX_TRANSACCIONES = ''
    REG_CVMAX_CANCELACIONES = 0
    EST_CVMAX_CANCELACIONES = ''
    FD_CVMAX_CANCELACIONES = ''
    REG_CVMAX_LUNES = 0
    EST_CVMAX_LUNES = ''
    FD_CVMAX_LUNES = ''
    
    
    fechaMax = datetime.now() + timedelta(days=-100)
    ultimoPro = ''
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cv-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'TXT_CVMAX_LUNES'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, (REGISTROS_AFECTADOS + 1) as REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cv-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'DW_MASTER_LOOKER'
        --AND REGISTROS_AFECTADOS > 0
        AND ESTADO_CARGA = 'FIN'
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        """
    )
    resId = query_id.result()
    
    for reg in resId:
        # Asignacion de variables 
        if reg.NOMBRE_CARGA == 'DW_MASTER_LOOKER':
            REG_CARGAS_CVMAX = reg.REGISTROS_AFECTADOS
            EST_CARGAS_CVMAX = reg.ESTADO_CARGA
            FD_CARGAS_CVMAX = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_CVMAX_LUNES':
            REG_CVMAX_LUNES = reg.REGISTROS_AFECTADOS
            EST_CVMAX_LUNES = reg.ESTADO_CARGA
            FD_CVMAX_LUNES = reg.FECHA_CARGA
            
        if reg.FECHA_CARGA >= fechaMax:
            fechaMax = reg.FECHA_CARGA
            ultimoPro = reg.NOMBRE_CARGA
    
    logging.info('Ultimo proceso ejecutado: ' + ultimoPro)
    logging.info('Hora de ultima actividad del proceso: ' + str(fechaMax))
    
    if ultimoPro != 'TXT_CVMAX_LUNES':
        if EST_CVMAX_LUNES == 'INICIO':
            logging.info('El proceso de generacion de TXT esta corriendo')
        else:
            logging.info('El proceso se puede ejecutar despues de evaluar que todas las tablas tienen registros')
            if REG_CARGAS_CVMAX > 0:
                logging.info('Se valido que todas las tablas estan cargadas....')
                logging.info('Inserta en la ETL Log para avisar del inicio de la generacion de los TXT')
                client = bigquery.Client(project=PROYECTO)
                query_id = client.query(
                    """
                    call GRAL.SP_INSERT_INICIO_ETL_LOG ('TXT_CVMAX_LUNES',
                                                        'Inicio TXT', 
                                                        'ST_REP', 
                                                        'ST_REP',
                                                        'CRON', 
                                                        'LINUX', 
                                                        0, 
                                                        'Inicia el proceso de generar los TXT', 
                                                        'Inicia todo correcto', 
                                                        0) 
                    """
                    )
                
                resId = query_id.result()
                logging.info('Se inicia la generacion de los TXT =D')
                v_per_carga = 1
            else:
                logging.info('Los TXT no se pueden ejecutar por que no todas las tablas tienen registros')
             
    elif ultimoPro == 'TXT_CVMAX_LUNES' and EST_CVMAX_LUNES == 'INICIO':
        logging.info('El proceso de generacion de TXT esta corriendo')
        
    elif ultimoPro == 'TXT_CVMAX_LUNES' and EST_CVMAX_LUNES == 'FIN':
        logging.info('El proceso de generacion de TXT ya se ejecuto')
        
    return v_per_carga
