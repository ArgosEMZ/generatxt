from datetime import date, datetime, timedelta
from numpy import random
from time import sleep
import os
import threading
import logging
import sys

import crea_csv_activos as crea_csv_activos
import cargaGCP as cargaGCP
import confirmaCargaGCP as confirmaCargaGCP
import validarEjecucion as validarEjecucion

sysVarDias = sys.argv[1]

###### Desarollo ###### 
#path_arc = "/home/jupyter/script_TXT/genera_CVMAX/"
#key_path = "/home/jupyter/accesos/amco-cv-qa-d7be3f5c7e10.json"

###### Produccion ###### ArchivosTXT
path_arc = "/home/txtamco/generatxt/genera_CVMAX/"
key_path = "/home/txtamco/accesos/amco-cv-qa-d7be3f5c7e10.json"

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path

logging.basicConfig(level = logging.INFO, filename = path_arc+'txt_Log.log', format= '[%(levelname)s] (%(threadName)-s %(message)s')

# Variable que nos indica con que fecha se tiene que generar el archivo (Numero de dias atras)
diasAtras = int(sysVarDias)

######### Muestra un print con la informacion a procesar ###########
fecha = datetime.now() + timedelta(days=-diasAtras)
fecha = fecha.strftime("%Y%m%d")
diasAtrasQ = diasAtras + 1
fechaQ = datetime.now() + timedelta(days=-diasAtrasQ)
fechaQ = fechaQ.strftime("%Y%m%d")
logging.info('El archivo se creara con la fecha: ' + fecha)
logging.info('El query se ejecuta con la fecha: ' + fechaQ)
v_per_carga = 0

v_per_carga = validarEjecucion.validaCarga(diasAtras, key_path, path_arc)

logging.info('El resultado de la funcion de PY es: ' + str(v_per_carga))

if v_per_carga == 1:
    ################### Limpia espacio de trabajo ######################
    os.system('mkdir '+path_arc+'ArchivosTXT')
    os.system('cd '+path_arc+'ArchivosTXT/; '+ 'rm -r '+path_arc+'ArchivosTXT/*')

    ####################### Genera los TXT #############################
    logging.info("Inicia a crear los TXT")
    t1 = threading.Thread(name="crea_csv_activos", target=crea_csv_activos.crea_csv_activos, args=(diasAtras,))
    t1.start()
    t1.join()

    logging.info("Termino de crear los TXT")

    ############# Confirma la carga de los archivos a GCP ################
    logging.info("Manda archivo de confirmacion a GCP")
    confirmaCargaGCP.confirmaCargaGCP(diasAtras, key_path, path_arc)