from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def ejecutaSPCargaHis(diasAtras, key_path, path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cv-qa"
    BUCKET = 'amco-cv-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    diasAtrasQ = diasAtras + 1
    
    client = bigquery.Client(project=PROYECTO)
    
    query_id = client.query("""call GRAL.SP_TXT_VISUALIZACIONES_HIST(0)""")
    resId = query_id.result()
    for reg in resId:
        print (reg)
    
    query_id = client.query("""call GRAL.SP_TXT_USUARIOS_ELIMINADOS_HIST(0)""")
    resId = query_id.result()
    for reg in resId:
        print (reg)
    
    query_id = client.query("""call GRAL.SP_TXT_USUARIOS_HIST(0)""")
    resId = query_id.result()
    for reg in resId:
        print (reg)
    
    query_id = client.query("""call GRAL.SP_TXT_TRANSACCIONES_HIST(0)""")
    resId = query_id.result()
    for reg in resId:
        print (reg)
    
    query_id = client.query("""call GRAL.SP_TXT_SUSCRIPCIONES_DIARIAS_HIST(0)""")
    resId = query_id.result()
    for reg in resId:
        print (reg)
    
    query_id = client.query("""call GRAL.SP_TXT_SUSCRIPCIONES_HIST(0)""")
    resId = query_id.result()
    for reg in resId:
        print (reg)
    
    query_id = client.query("""call GRAL.SP_TXT_CATALOGO_CV_HIST(0)""")
    resId = query_id.result()
    for reg in resId:
        print (reg)
    
    query_id = client.query("""call GRAL.SP_TXT_TEMPORADA_CV_HIST(0)""")
    resId = query_id.result()
    for reg in resId:
        print (reg)
