from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os
import logging

def validaCarga(diasAtras,key_path,path_arc):
    
    logging.basicConfig(level = logging.INFO, filename = path_arc+'txt_Log.log', format= '[%(levelname)s] (%(threadName)-s %(message)s')
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    #fecha = datetime.now() + timedelta(days=-diasAtras)
    #fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cv-qa"
    BUCKET = 'amco-cv-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    v_per_carga = 0
    
    #diasAtrasQ = diasAtras + 1
    
    REG_TXT_VIS = 0
    REG_TXT_SUS_DIARIAS = 0
    REG_TXT_SUS = 0
    REG_TMP_TEM = 0
    REG_TXT_TRAN = 0
    REG_TXT_USR_ELI = 0
    REG_TXT_USR = 0
    REG_TXT_CAT = 0
    REG_TXT_CV_DIARIO = 0
    
    EST_TXT_VIS = ''
    EST_TXT_SUS_DIARIAS = ''
    EST_TXT_SUS = ''
    EST_TMP_TEM = ''
    EST_TXT_TRAN = ''
    EST_TXT_USR_ELI = ''
    EST_TXT_USR = ''
    EST_TXT_CAT = ''
    EST_TXT_CV_DIARIO = ''
    
    FD_TXT_VIS = ''
    FD_TXT_SUS_DIARIAS = ''
    FD_TXT_SUS = ''
    FD_TMP_TEM = ''
    FD_TXT_TRAN = ''
    FD_TXT_USR_ELI = ''
    FD_TXT_USR = ''
    FD_TXT_CAT = ''
    FD_TXT_CV_DIARIO = ''
    
    
    fechaMax = datetime.now() + timedelta(days=-100)
    ultimoPro = ''
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cv-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'TXT_VISUALIZACIONES_CORTE'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cv-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'TXT_SUSCRIPCIONES_DIARIAS_CORTE'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cv-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'TXT_SUSCRIPCIONES_CORTE'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, (REGISTROS_AFECTADOS+1) as REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cv-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'TXT_TEMPORADA_CV_CORTE'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cv-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'TXT_TRANSACCIONES_CORTE'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cv-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'TXT_USUARIOS_ELIMINADOS_CORTE'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cv-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'TXT_USUARIOS_CORTE'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cv-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'TXT_CATALOGO_CV_CORTE'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cv-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'TXT_CV_DIARIO'
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        ORDER BY FECHA_CARGA DESC
        """
    )
    resId = query_id.result()
    
    for reg in resId:
        # Asignacion de variables 
        if reg.NOMBRE_CARGA == 'TXT_VISUALIZACIONES_CORTE':
            REG_TXT_VIS = reg.REGISTROS_AFECTADOS
            EST_TXT_VIS = reg.ESTADO_CARGA
            FD_TXT_VIS = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_SUSCRIPCIONES_DIARIAS_CORTE':
            REG_TXT_SUS_DIARIAS = reg.REGISTROS_AFECTADOS
            EST_TXT_SUS_DIARIAS = reg.ESTADO_CARGA
            FD_TXT_SUS_DIARIAS = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_SUSCRIPCIONES_CORTE':
            REG_TXT_SUS = reg.REGISTROS_AFECTADOS
            EST_TXT_SUS = reg.ESTADO_CARGA
            FD_TXT_SUS = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_TEMPORADA_CV_CORTE':
            REG_TMP_TEM = reg.REGISTROS_AFECTADOS
            EST_TMP_TEM = reg.ESTADO_CARGA
            FD_TMP_TEM = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_TRANSACCIONES_CORTE':
            REG_TXT_TRAN = reg.REGISTROS_AFECTADOS
            EST_TXT_TRAN = reg.ESTADO_CARGA
            FD_TXT_TRAN = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_USUARIOS_ELIMINADOS_CORTE':
            REG_TXT_USR_ELI = reg.REGISTROS_AFECTADOS
            EST_TXT_USR_ELI = reg.ESTADO_CARGA
            FD_TXT_USR_ELI = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_USUARIOS_CORTE':
            REG_TXT_USR = reg.REGISTROS_AFECTADOS
            EST_TXT_USR = reg.ESTADO_CARGA
            FD_TXT_USR = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_CATALOGO_CV_CORTE':
            REG_TXT_CAT = reg.REGISTROS_AFECTADOS
            EST_TXT_CAT = reg.ESTADO_CARGA
            FD_TXT_CAT = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_CV_DIARIO':
            REG_TXT_CV_DIARIO = reg.REGISTROS_AFECTADOS
            EST_TXT_CV_DIARIO = reg.ESTADO_CARGA
            FD_TXT_CV_DIARIO = reg.FECHA_CARGA
            
        if reg.FECHA_CARGA >= fechaMax:
            fechaMax = reg.FECHA_CARGA
            ultimoPro = reg.NOMBRE_CARGA
    
    logging.info('Ultimo proceso ejecutado: ' + ultimoPro)
    logging.info('Hora de ultima actividad del proceso: ' + str(fechaMax))
    
    if ultimoPro != 'TXT_CV_DIARIO':
        if EST_TXT_CV_DIARIO == 'INICIO':
            logging.info('El proceso de generacion de TXT esta corriendo')
        else:
            logging.info('El proceso se puede ejecutar despues de evaluar que todas las tablas tienen registros')
            if REG_TXT_VIS > 0 and REG_TXT_SUS_DIARIAS > 0 and REG_TXT_SUS > 0 and REG_TMP_TEM > 0 and REG_TXT_TRAN > 0 and REG_TXT_USR_ELI > 0 and REG_TXT_USR > 0 and REG_TXT_CAT > 0:
                logging.info('Se valido que todas las tablas estan cargadas....')
                logging.info('Inserta en la ETL Log para avisar del inicio de la generacion de los TXT')
                client = bigquery.Client(project=PROYECTO)
                query_id = client.query(
                    """
                    call GRAL.SP_INSERT_INICIO_ETL_LOG ('TXT_CV_DIARIO',
                                                        'Inicio TXT', 
                                                        'ST_REP', 
                                                        'ST_REP',
                                                        'CRON', 
                                                        'LINUX', 
                                                        0, 
                                                        'Inicia el proceso de generar los TXT', 
                                                        'Inicia todo correcto', 
                                                        0) 
                    """
                    )
                
                resId = query_id.result()
                logging.info('Se inicia la generacion de los TXT =D')
                v_per_carga = 1
            else:
                logging.info('Los TXT no se pueden ejecutar por que no todas las tablas tienen registros')
             
    elif ultimoPro == 'TXT_CV_DIARIO' and EST_TXT_CV_DIARIO == 'INICIO':
        logging.info('El proceso de generacion de TXT esta corriendo')
        
    elif ultimoPro == 'TXT_CV_DIARIO' and EST_TXT_CV_DIARIO == 'FIN':
        logging.info('El proceso de generacion de TXT ya se ejecuto')
        
    return v_per_carga
