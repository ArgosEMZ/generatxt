from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_archivos(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    fechaQ = datetime.now() + timedelta(days=-(diasAtras+1))
    fechaQ = fechaQ.strftime("%Y-%m-%d") # 2022-04-06
    PROYECTO = "amco-cv-qa"
    BUCKET = 'amco-cv-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        select  ID_PAIS as ID_PAIS, -- ID_TIENDA
                DS_PAIS as DS_PAIS
        from amco-cv-qa.GRAL.DIM_PAIS_TXT
        where id_pais not in (760, 759)
        """
    )
    resId = query_id.result()
    
    for reg in resId:
        
        nombreArchivo = "Visualizaciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5 = "Visualizaciones_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl = "Visualizaciones_"+reg.DS_PAIS+"_"+fecha+".ctl"
        nombreZip = "Visualizaciones_"+reg.DS_PAIS+"_"+fecha+".zip"

        #Query para obtener los datos
        client = bigquery.Client(project=PROYECTO)
        query_job = client.query(
            """
            select  DISTINCT(
                    CONCAT('"',
                    IFNULL(REPLACE(CAST(PAIS AS STRING),'"','""'),''),'"|',
                    IFNULL(REPLACE(CAST(ID_CLIENTE AS STRING),'"','""'),''),'|"',
                    IFNULL(REPLACE(CAST(TX_NOMBRE AS STRING),'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(TX_ESTUDIO AS STRING),'"','""'),''),'"|',
                    IFNULL(REPLACE(CAST(FORMAT_DATETIME("%d/%m/%Y %H:%M:%S", COD_FECHA_VISUALIZACION) AS STRING),'"','""'),''),'|',
                    IFNULL(REPLACE(CAST(FORMAT_DATETIME("%d/%m/%Y %H:%M:%S", FECHA_PAIS) AS STRING),'"','""'),''),'|"',
                    IFNULL(REPLACE(CAST(TX_DISP_CATEGORIA_ORIGINAL AS STRING),'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(TX_DISP_MODELO_ORIGINAL AS STRING),'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(TX_DISP_TIPO_ORIGINAL AS STRING),'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(TX_DISP_VERSION_ORIGINAL AS STRING),'"','""'),''),'"|',
                    IFNULL(REPLACE(CAST(COD_PARTNER_OPERACION AS STRING),'"','""'),''),'|"',
                    IFNULL(REPLACE(CAST(TIPO_OPERACION AS STRING),'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(TX_CUENTA AS STRING),'"','""'),''),'"|',
                    IFNULL(REPLACE(CAST(ID_GRUPO AS STRING),'"','""'),''),'|',
                    IFNULL(REPLACE(CAST(PRECIO AS STRING),'"','""'),''),'|"',
                    IFNULL(REPLACE(CAST(MEDIO_PAGO_NOMBRE AS STRING),'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(ABONO AS STRING),'"','""'),''),'"|',
                    IFNULL(REPLACE(CAST(FORMAT_DATETIME("%d/%m/%Y %H:%M:%S", FECHA_ULT_VIS) AS STRING),'"','""'),''),'|',
                    IFNULL(REPLACE(CAST(ULT_VIS AS STRING),'"','""'),''),'|',
                    IFNULL(REPLACE(CAST(FORMAT_DATETIME("%d/%m/%Y %H:%M:%S", FECHA_MAX_VIS) AS STRING),'"','""'),''),'|',
                    IFNULL(REPLACE(CAST(MAX_VIS AS STRING),'"','""'),''),'|"',
                    IFNULL(REPLACE(CAST(TX_DISP_FABRICANTE_ORIGINAL AS STRING),'"','""'),''),'"|',
                    IFNULL(REPLACE(CAST(USUARIO_PERFIL AS STRING),'"','""'),''),'|',
                    IFNULL(REPLACE(CAST(COD_PARTNER_CALCULADO AS STRING),'"','""'),''),'|',
                    IFNULL(REPLACE(CAST(MEDIO_PAGO_CALCULADO AS STRING),'"','""'),''),'|',
                    IFNULL(REPLACE(CAST(ABONO_CALCULADO AS STRING),'"','""'),''),'|',
                    IFNULL(REPLACE(CAST(USUARIO_PRUEBA  AS STRING),'"','""'),'')
                    )) AS dato
            from amco-cv-qa.GRAL.TXT_VISUALIZACIONES_CORTE
            where (ID_PAIS = """+str(reg.ID_PAIS)+""")
            --where (ID_PAIS = 441)
            and DATETIME_TRUNC(FECHA_CORTE, DAY) = '"""+str(fechaQ)+"""'
            """
        )

        results = query_job.result()

        f = open(path_arc+'ArchivosTXT/' + nombreArchivo, 'w' ,encoding='UTF8')
        ## Primero escribir el cabecero
        f.write('"PAIS"|"ID_CLIENTE"|"TX_NOMBRE"|"TX_ESTUDIO"|"COD_FECHA_VISUALIZACION"|"FECHA_PAIS"|"TX_DISP_CATEGORIA_ORIGINAL"|"TX_DISP_MODELO_ORIGINAL"|"TX_DISP_TIPO_ORIGINAL"|"TX_DISP_VERSION_ORIGINAL"|"COD_PARTNER_OPERACION"|"TIPO_OPERACION"|"TX_CUENTA"|"ID_GRUPO"|"PRECIO"|"MEDIO_PAGO_NOMBRE"|"ABONO"|"FECHA_ULT_VIS"|"ULT_VIS"|"FECHA_MAX_VIS"|"MAX_VIS"|"TX_DISP_FABRICANTE_ORIGINAL"|"USUARIO_PERFIL"|"COD_PARTNER_OPERACION_CALC"|"MEDIO_PAGO_CALCULADO"|"ABONO_CALCULADO"|"USUARIO_PRUEBA"')
        f.write('\r\n')    
            
        for row in results:
            registro = row.dato
            f.write(registro)
            f.write('\r\n')
            totalReg += 1
            
        f.close()    
        print ("Se genero el archivo correctamente: " + nombreArchivo)
        print ("Total de registros " + str(totalReg))
    
        ################### Generar archivo CTL ######################
        os.system("echo \""+str(totalReg)+"\" >> "+path_arc+"ArchivosTXT/"+nombreCtl)

        totalReg = 0
