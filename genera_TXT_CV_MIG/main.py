from datetime import date, datetime, timedelta
from numpy import random
from time import sleep
import os
import threading
import logging
import sys

import TXT_SUSCRIPCIONES_DIARIAS as SUSCRIPCIONES_DIARIAS
import TXT_SUSCRIPCIONES as SUSCRIPCIONES
import TXT_USUARIOS as USUARIOS
import TXT_USUARIOS_ELIMINADOS as USUARIOS_ELIMINADOS
import TXT_TRANSACCIONES as TRANSACCIONES
import TXT_VISUALIZACIONES as VISUALIZACIONES
import TXT_CATALOGO_CV as CATALOGO_CV
import TXT_TEMPORADA_CV as TEMPORADA_CV

import generaZIP as generaZIP
import cargaZipGCP as cargaZipGCP
import generaMd5 as generaMd5
import renombraArchivos as renombraArchivos
import confirmaCargaGCP as confirmaCargaGCP
import validarEjecucion as validarEjecucion
import ejecutaSPCargaHis as ejecutaSPCargaHis


sysVarDias = sys.argv[1]

###### Desarollo ###### 
#path_arc = "/home/jupyter/script_TXT/genera_TXT_CV_MIG/"
#key_path = "/home/jupyter/accesos/amco-cv-qa-d7be3f5c7e10.json"

###### Produccion ###### ArchivosTXT
path_arc = "/home/txtamco/generatxt/genera_TXT_CV_MIG/"
key_path = "/home/txtamco/accesos/amco-cv-qa-d7be3f5c7e10.json"

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path

logging.basicConfig(level = logging.INFO, filename = path_arc+'txt_Log.log', format= '[%(levelname)s] (%(threadName)-s %(message)s')

# Variable que nos indica con que fecha se tiene que generar el archivo (Numero de dias atras)
diasAtras = int(sysVarDias)

######### Muestra un print con la informacion a procesar ###########
fecha = datetime.now() + timedelta(days=-diasAtras)
fecha = fecha.strftime("%Y%m%d")
diasAtrasQ = diasAtras + 1
fechaQ = datetime.now() + timedelta(days=-diasAtrasQ)
fechaQ = fechaQ.strftime("%Y%m%d")
logging.info('El archivo se creara con la fecha: ' + fecha)
logging.info('El query se ejecuta con la fecha: ' + fechaQ)
v_per_carga = 0

v_per_carga = validarEjecucion.validaCarga(diasAtras, key_path, path_arc)

logging.info('El resultado de la funcion de PY es: ' + str(v_per_carga))

## if v_per_carga == 1:
if v_per_carga == 1 or int(sysVarDias)>0 :
    ################### Limpia espacio de trabajo ######################
    os.system('mkdir '+path_arc+'ArchivosTXT')
    os.system('cd '+path_arc+'ArchivosTXT/; '+ 'rm -r '+path_arc+'ArchivosTXT/*')

    ####################### Genera los TXT #############################
    logging.info("Inicia a crear los TXT")
    t1 = threading.Thread(name="SUSCRIPCIONES_DIARIAS", target=SUSCRIPCIONES_DIARIAS.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t2 = threading.Thread(name="SUSCRIPCIONES", target=SUSCRIPCIONES.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t3 = threading.Thread(name="USUARIOS", target=USUARIOS.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t4 = threading.Thread(name="USUARIOS_ELIMINADOS", target=USUARIOS_ELIMINADOS.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t5 = threading.Thread(name="TRANSACCIONES", target=TRANSACCIONES.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t6 = threading.Thread(name="VISUALIZACIONES", target=VISUALIZACIONES.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t7 = threading.Thread(name="CATALOGO_CV", target=CATALOGO_CV.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t8 = threading.Thread(name="TEMPORADA_CV", target=TEMPORADA_CV.genera_archivos, args=(diasAtras, key_path, path_arc,))

    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t5.start()
    t6.start()
    t7.start()
    t8.start()

    t1.join()
    t2.join()
    t3.join()
    t4.join()
    t5.join()
    t6.join()
    t7.join()
    t8.join()

    logging.info("Termino de crear los TXT")
    ##################### Carga los archivos ###########################
    ########renombraArchivos.renombra_arc(diasAtras)
    logging.info("Inicia a generar los MD5")
    generaMd5.genera_MD5(diasAtras, key_path, path_arc)
    logging.info("Inicia a generar los ZIP")
    generaZIP.genera_zip(diasAtras, key_path, path_arc)
    logging.info("Carga los ZIP a GCP")
    cargaZipGCP.carga_zip(diasAtras, key_path, path_arc)

    ############# Confirma la carga de los archivos a GCP ################
    logging.info("Manda archivo de confirmacion a GCP")
    confirmaCargaGCP.confirmaCargaGCP(diasAtras, key_path, path_arc)
    
