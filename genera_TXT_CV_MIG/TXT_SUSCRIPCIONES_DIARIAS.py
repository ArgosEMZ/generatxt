from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_archivos(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    fechaQ = datetime.now() + timedelta(days=-(diasAtras+1))
    fechaQ = fechaQ.strftime("%Y-%m-%d") # 2022-04-06
    PROYECTO = "amco-cv-qa"
    BUCKET = 'amco-cv-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        select  ID_PAIS as ID_PAIS, -- ID_TIENDA
                DS_PAIS as DS_PAIS
        from amco-cv-qa.GRAL.DIM_PAIS_TXT
        where id_pais not in (760, 759)
        """
    )
    resId = query_id.result()
    
    for reg in resId:
    
        nombreArchivo = "SuscripcionesDiarias_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5 = "SuscripcionesDiarias_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl = "SuscripcionesDiarias_"+reg.DS_PAIS+"_"+fecha+".ctl"
        nombreZip = "SuscripcionesDiarias_"+reg.DS_PAIS+"_"+fecha+".zip"

        #Query para obtener los datos
        client = bigquery.Client(project=PROYECTO)
        query_job = client.query(
            """
            select  DISTINCT(
                    CONCAT('"',
                    IFNULL(REPLACE(CAST(PAIS AS STRING),'"','""'),''),'"|',
                    IFNULL(REPLACE(CAST(ID_CLIENTE AS STRING),'"','""'),''),'|',
                    IFNULL(REPLACE(CAST(COD_PARTNER_OPERACION AS STRING),'"','""'),''),'|',
                    IFNULL(REPLACE(CAST(FORMAT_DATETIME("%d/%m/%Y %H:%M:%S", COD_FECHA_SUSCRIPCION) AS STRING),'"','""'),''),'|',
                    IFNULL(REPLACE(CAST(FORMAT_DATETIME("%d/%m/%Y %H:%M:%S", COD_FECHA_SUSC_PAIS) AS STRING),'"','""'),''),'|',
                    IFNULL(REPLACE(CAST(PRECIO AS STRING),'"','""'),''),'|"',
                    IFNULL(REPLACE(CAST(ABONO AS STRING),'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(TX_SUSCRIPCION AS STRING),'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(MEDIO_PAGO_NOMBRE AS STRING),'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(TX_CUENTA AS STRING),'"','""'),''),'"|',
                    IFNULL(REPLACE(CAST(ID_ORIGEN_PAGO AS STRING),'"','""'),''),'|"',
                    IFNULL(REPLACE(CAST(TX_ORIGEN_PAGO AS STRING),'"','""'),''),'"|',
                    IFNULL(REPLACE(CAST(CANT_RENOVACIONES AS STRING),'"','""'),''),'|"',
                    IFNULL(REPLACE(CAST(USUARIO_ALTA AS STRING),'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(TIPO_USUARIO AS STRING),'"','""'),''),'"|',
                    IFNULL(REPLACE(CAST(USUARIO_PERFIL AS STRING),'"','""'),''),'|"',
                    IFNULL(REPLACE(CAST(PRODUCT_ID AS STRING),'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(TX_PRODUCT_ID_NOMBRE AS STRING),'"','""'),''),'"|',
                    IFNULL(REPLACE(CAST(SUSCRITO_ESPECIAL AS STRING),'"','""'),''),'|',
                    IFNULL(REPLACE(CAST(USUARIO_PRUEBA AS STRING),'"','""'),'')
                    )) AS dato
            --from amco-cv-qa.GRAL.TXT_SUSCRIPCIONES_DIARIAS
            from amco-cv-qa.TXT.TXT_SUSCRIPCIONES_DIARIAS
            where FECHA_CORTE = '"""+str(fechaQ)+"""'
            and (ID_PAIS = """+str(reg.ID_PAIS)+""")
            --and (ID_PAIS = 441)
            """
        )

        results = query_job.result()

        f = open(path_arc+'ArchivosTXT/' + nombreArchivo, 'w' ,encoding='UTF8')
        ## Primero escribir el cabecero
        f.write('"PAIS"|"ID_CLIENTE"|"COD_PARTNER_OPERACION"|"COD_FECHA_SUSCRIPCION"|"COD_FECHA_SUSC_PAIS"|"PRECIO"|"ABONO"|"TX_SUSCRIPCION"|"MEDIO_PAGO_NOMBRE"|"TX_CUENTA"|"ID_ORIGEN_PAGO"|"TX_ORIGEN_PAGO"|"CANT_RENOVACIONES"|"USUARIO_ALTA"|"TIPO_USUARIO"|"USUARIO_PERFIL"|"PRODUCT_ID"|"TX_PRODUCT_ID_NOMBRE"|"SUSCRITO_ESPECIAL"|"USUARIO_PRUEBA"')
        f.write('\r\n')    
            
        for row in results:
            registro = row.dato
            f.write(registro)
            f.write('\r\n')
            totalReg += 1
            
        f.close()    
        print ("Se genero el archivo correctamente: " + nombreArchivo)
        print ("Total de registros " + str(totalReg))
    
        ################### Generar archivo CTL ######################
        os.system("echo \""+str(totalReg)+"\" >> "+path_arc+"ArchivosTXT/"+nombreCtl)

        totalReg = 0
