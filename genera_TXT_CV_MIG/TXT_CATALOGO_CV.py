from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_archivos(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    fechaQ = datetime.now() + timedelta(days=-(diasAtras+1))
    fechaQ = fechaQ.strftime("%Y-%m-%d") # 2022-04-06
    PROYECTO = "amco-cv-qa"
    BUCKET = 'amco-cv-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        select  ID_PAIS as ID_PAIS, -- ID_TIENDA
                DS_PAIS as DS_PAIS
        from amco-cv-qa.GRAL.DIM_PAIS_TXT
        where id_pais not in (760, 759)
        """
    )
    resId = query_id.result()
    
    for reg in resId:
        
        nombreArchivo = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5 = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".ctl"
        nombreZip = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".zip"

        #Query para obtener los datos
        client = bigquery.Client(project=PROYECTO)
        query_job = client.query(
            """
            select  DISTINCT(
                    CONCAT(
                    IFNULL(REPLACE(CAST(ID_GRUPO AS STRING),'"','""')  ,''),'|"',
                    IFNULL(REPLACE(CAST(TITULO AS STRING) ,'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(TITULO_ESP AS STRING) ,'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(CATEGORIA AS STRING) ,'"','""'),''),'"|',
                    IFNULL(REPLACE(CAST(NUMERO_TEMPORADA AS STRING) ,'"','""'),''),'|',
                    IFNULL(REPLACE(CAST(NUMERO_CAPITULO AS STRING) ,'"','""'),''),'|"',
                    IFNULL(REPLACE(CAST(NOMBRE_CAPITULO AS STRING) ,'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(NOMBRE_CAPITULO_ESP AS STRING) ,'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(DURACION_TOTAL AS STRING) ,'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(ESTUDIO AS STRING) ,'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(PROVEEDOR AS STRING) ,'"','""'),''),'"|"',
                    IFNULL(REPLACE(CAST(TYPE AS STRING) ,'"','""'),''),'"|',
                    IFNULL(REPLACE(CAST(PROGRAM_ID AS STRING) ,'"','""'),''),'|"',
                    IFNULL(REPLACE(CAST(TX_NOMBRE_SERIE AS STRING) ,'"','""'),''),'"'
                    )) AS dato
            --from amco-cv-qa.GRAL.TXT_CATALOGO_CV
            from amco-cv-qa.TXT.TXT_CATALOGO_CV
            where FECHA_CORTE = '"""+str(fechaQ)+"""' 
            and (ID_PAIS = """+str(reg.ID_PAIS)+""")
            --and (ID_PAIS = 441)
            """
        )

        results = query_job.result()

        f = open(path_arc+'ArchivosTXT/' + nombreArchivo, 'w' ,encoding='UTF8')
        ## Primero escribir el cabecero
        f.write('"ID_GRUPO"|"TITULO"|"TITULO_ESP"|"CATEGORIA"|"NUMERO_TEMPORADA"|"NUMERO_CAPITULO"|"NOMBRE_CAPITULO"|"NOMBRE_CAPITULO_ESP"|"DURACION_TOTAL"|"ESTUDIO"|"PROVEEDOR"|"TYPE"|"PROGRAM_ID"|"TX_NOMBRE_SERIE"')
        f.write('\r\n')    
            
        for row in results:
            registro = row.dato
            f.write(registro)
            f.write('\r\n')
            totalReg += 1
            
        f.close()    
        print ("Se genero el archivo correctamente: " + nombreArchivo)
        print ("Total de registros " + str(totalReg))
    
        ################### Generar archivo CTL ######################
        os.system("echo \""+str(totalReg)+"\" >> "+path_arc+"ArchivosTXT/"+nombreCtl)

        totalReg = 0
