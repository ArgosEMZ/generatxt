from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_archivos(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    fechaQ = datetime.now() + timedelta(days=-(diasAtras+1))
    fechaQ = fechaQ.strftime("%Y-%m-%d") # 2022-04-06
    PROYECTO = "amco-cv-qa"
    BUCKET = 'amco-cv-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        select  ID_PAIS as ID_PAIS, -- ID_TIENDA
                DS_PAIS as DS_PAIS
        from amco-cv-qa.GRAL.DIM_PAIS_TXT
        where id_pais = 441
        """
    )
    resId = query_id.result()
    
    for reg in resId:
        
        nombreArchivo = "Temporada_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5 = "Temporada_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl = "Temporada_"+reg.DS_PAIS+"_"+fecha+".ctl"
        nombreZip = "Temporada_"+reg.DS_PAIS+"_"+fecha+".zip"

        #Query para obtener los datos
        client = bigquery.Client(project=PROYECTO)
        query_job = client.query(
            """
            select  DISTINCT(
                    CONCAT('"',
                    IFNULL(REPLACE(CAST(PAIS AS STRING),'"','""'),''), '"|',
                    IFNULL(REPLACE(CAST(COD_PARTNER_OPERACION AS STRING),'"','""'),''), '|',
                    IFNULL(REPLACE(CAST(ID_CLIENTE AS STRING),'"','""'),''), '|',
                    IFNULL(REPLACE(CAST(FORMAT_DATETIME("%d/%m/%Y %H:%M:%S", FECHA_ARG) AS STRING),'"','""'),''), '|',
                    IFNULL(REPLACE(CAST(FORMAT_DATETIME("%d/%m/%Y %H:%M:%S", FECHA_PAIS) AS STRING),'"','""'),''), '|"',
                    IFNULL(REPLACE(CAST(TX_ESTUDIO AS STRING),'"','""'),''), '"|',
                    IFNULL(REPLACE(CAST(COD_TEMPORADA AS STRING),'"','""'),''), '|"',
                    IFNULL(REPLACE(CAST(TITULO AS STRING),'"','""'),''), '"|',
                    IFNULL(REPLACE(CAST(TEMPORADA AS STRING),'"','""'),''), '|',
                    IFNULL(REPLACE(CAST(CAPITULOS AS STRING),'"','""'),''), '|"',
                    IFNULL(REPLACE(CAST(TYPE AS STRING),'"','""'),''), '"|"',
                    IFNULL(REPLACE(CAST(ABONO AS STRING),'"','""'),''), '"|"',
                    IFNULL(REPLACE(CAST(MEDIO_PAGO_NOMBRE AS STRING),'"','""'),''), '"|',
                    IFNULL(REPLACE(CAST(PRECIO AS STRING),'"','""'),''), '|"',
                    IFNULL(REPLACE(CAST(BONIFICADO AS STRING),'"','""'),''), '"|"',
                    IFNULL(REPLACE(CAST(TX_CUENTA AS STRING),'"','""'),''), '"|"',
                    IFNULL(REPLACE(CAST(TIPO_USUARIO AS STRING),'"','""'),''), '"'
                     )) AS dato
            from amco-cv-qa.TXT.TXT_TEMPORADA_CV
            where FECHA_CORTE = '"""+str(fechaQ)+"""' 
            and ID_PAIS = """+str(reg.ID_PAIS)+"""
            --and (ID_PAIS = 441)
            """
        )

        results = query_job.result()

        f = open(path_arc+'ArchivosTXT/' + nombreArchivo, 'w' ,encoding='UTF8')
        ## Primero escribir el cabecero
        f.write('"PAIS"|"COD_PARTNER_OPERACION"|"ID_CLIENTE"|"FECHA_ARG"|"FECHA_PAIS"|"TX_ESTUDIO"|"COD_TEMPORADA"|"TITULO"|"TEMPORADA"|"CAPITULOS"|"TYPE"|"ABONO"|"MEDIO_PAGO_NOMBRE"|"PRECIO"|"BONIFICADO"|"TX_CUENTA"|"TIPO_USUARIO"')
        f.write('\r\n')    
            
        for row in results:
            registro = row.dato
            f.write(registro)
            f.write('\r\n')
            totalReg += 1
            
        f.close()    
        print ("Se genero el archivo correctamente: " + nombreArchivo)
        print ("Total de registros " + str(totalReg))
    
        ################### Generar archivo CTL ######################
        os.system("echo \""+str(totalReg)+"\" >> "+path_arc+"ArchivosTXT/"+nombreCtl)

        totalReg = 0
