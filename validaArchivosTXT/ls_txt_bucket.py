from google.cloud import storage
from google.oauth2 import service_account
from google.cloud import bigquery
from datetime import date, datetime, timedelta
import os
    
###### Desarollo ###### 
#path_arc = "/home/jupyter/script_TXT/genera_TXT_CV/"
#key_path = "/home/jupyter/accesos/amco-cv-qa-d7be3f5c7e10.json"

###### Produccion ###### ArchivosTXT
path_arc = "/home/txtamco/generatxt/genera_TXT_CV/"
key_path = "/home/txtamco/accesos/amco-cv-qa-d7be3f5c7e10.json"

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path

service_account_file=key_path
credentials = service_account.Credentials.from_service_account_file(key_path)
credentials = credentials.with_scopes(['https://www.googleapis.com/auth/cloud-platform'])

"""Lists all the blobs in the bucket."""
# amco-cv-qa-txt
PROYECTO = "amco-cv-qa"
client = bigquery.Client(project=PROYECTO)
bucket_name = "amco-cv-qa-txt"
storage_client = storage.Client()
blobs = storage_client.list_blobs(bucket_name)

minAtras = 60
fechaQ = datetime.now() + timedelta(minutes=-minAtras)
fechaQ = fechaQ.strftime("%Y-%m-%d %H:%M:%S")
fechaQ = datetime.strptime(fechaQ, '%Y-%m-%d %H:%M:%S')

v_var_control = 0
v_var_limite = 2

######################### Validacion de ejecucion I #######################

totalReg = 0
#fecha = datetime.now() + timedelta(days=-diasAtras)
#fecha = fecha.strftime("%Y%m%d")
PROYECTO = "amco-cv-qa"
BUCKET = 'amco-cv-qa'
dataset_id = "GRAL"
table_id = "ETL_LOG"
v_per_carga = 0

#diasAtrasQ = diasAtras + 1

REG_LS_BUCKET = 0
EST_LS_BUCKET = ''
FD_LS_BUCKET = ''


fechaMax = datetime.now() + timedelta(days=-100)
ultimoPro = ''

client = bigquery.Client(project=PROYECTO)
query_id = client.query(
    """
    SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
    FROM (
    SELECT NOMBRE_CARGA, (REGISTROS_AFECTADOS + 1) AS REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
    FROM amco-cv-qa.GRAL.ETL_LOG
    WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
    AND NOMBRE_CARGA = 'LS_BUCKET_FUN'
    ORDER BY FECHA_CARGA DESC
    LIMIT 1
    )
    ORDER BY FECHA_CARGA DESC
    """
    )
resId = query_id.result()

for reg in resId:
    # Asignacion de variables 
    if reg.NOMBRE_CARGA == 'LS_BUCKET_FUN':
        REG_LS_BUCKET = reg.REGISTROS_AFECTADOS
        EST_LS_BUCKET = reg.ESTADO_CARGA
        FD_LS_BUCKET = reg.FECHA_CARGA

    if reg.FECHA_CARGA >= fechaMax:
        fechaMax = reg.FECHA_CARGA
        ultimoPro = reg.NOMBRE_CARGA

print ('Ultimo proceso ejecutado: ' + ultimoPro)
print ('Hora de ultima actividad del proceso: ' + str(fechaMax))

if ultimoPro != 'LS_BUCKET_FUN':
    if EST_LS_BUCKET == 'INICIO':
        print ('El proceso de generacion de TXT esta corriendo')
    else:
        print ('El proceso se puede ejecutar despues de evaluar que todas las tablas tienen registros')
        ##if REG_LS_BUCKET > 0:
        print ('Se valido que todas las tablas estan cargadas....')
        print ('Inserta en la ETL Log para avisar del inicio de la generacion de los TXT')
        client = bigquery.Client(project=PROYECTO)
        query_id = client.query(
            """
            call GRAL.SP_INSERT_INICIO_ETL_LOG ('LS_BUCKET_FUN',
                                                'Inicio TXT', 
                                                'ST_REP', 
                                                'ST_REP',
                                                'CRON', 
                                                'LINUX', 
                                                0, 
                                                'Inicia el proceso de generar los TXT', 
                                                'Inicia todo correcto', 
                                                0) 
            """
            )

        resId = query_id.result()
        print ('Se inicia la generacion de los TXT =D')
        v_per_carga = 1
        #else:
         #   print ('Los TXT no se pueden ejecutar por que no todas las tablas tienen registros')

elif ultimoPro == 'LS_BUCKET_FUN' and EST_LS_BUCKET == 'INICIO':
    print ('El proceso de generacion de TXT esta corriendo')

elif ultimoPro == 'LS_BUCKET_FUN' and EST_LS_BUCKET == 'FIN':
    print ('El proceso de generacion de TXT ya se ejecuto pero se vuelve a ejecutar')
    print ('Inserta en la ETL Log para avisar del inicio de la generacion de los TXT')
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        call GRAL.SP_INSERT_INICIO_ETL_LOG ('LS_BUCKET_FUN',
                                            'Inicio TXT', 
                                            'ST_REP', 
                                            'ST_REP',
                                            'CRON', 
                                            'LINUX', 
                                            0, 
                                            'Inicia el proceso de generar los TXT', 
                                            'Inicia todo correcto', 
                                            0) 
        """
        )

    resId = query_id.result()
    print ('Se inicia la generacion de los TXT =D')
    v_per_carga = 1

#return v_per_carga

######################### Validacion de ejecucion F #######################

if v_per_carga == 1:    

    carpetas = ["TXT/",
                "G3/",
                "LOGIN/",
                "CVMAX/"]

    tipos_archivos = ["application/zip", 
                      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", 
                      "text/csv"]

    for blob in blobs:
        if  blob.name[0:(blob.name.find('/') + 1)] in carpetas and format(blob.content_type) in tipos_archivos:
        #if  blob.name[0:(blob.name.find('/') + 1)] in carpetas and format(blob.content_type) in tipos_archivos and v_var_control < v_var_limite:
        #if  blob.name[0:(blob.name.find('/') + 1)] in carpetas and v_var_control < v_var_limite:

            srt_fecha_txt = str(format(blob.updated))[0:19]
            date_fecha_txt = datetime.strptime(srt_fecha_txt, '%Y-%m-%d %H:%M:%S')
            #print(date_fecha_txt)

            if fechaQ <= date_fecha_txt:
                #print('La fecha esta en los ultimos 30 dias')
                #print (date_fecha_txt)
                #print("Fecha Archivo: " + format(blob.updated))

                client = bigquery.Client(project=PROYECTO)
                query_id = client.query(
                    """
                    MERGE amco-cv-qa.GRAL.ARCHIVOS_TXT_BUCKET PRO
                    USING (SELECT   '"""+str(blob.name[(blob.name.find('/') + 1):300])+"""' AS ARCHIVO,
                                    CAST('"""+str(format(blob.size))+"""' AS INT64) AS SIZE,
                                    PARSE_DATETIME('%Y-%m-%d %H:%M:%S',SUBSTRING('"""+str(format(blob.updated))+"""', 0, 19)) AS FECHA,
                                   '"""+str(blob.name[0:(blob.name.find('/'))])+"""' AS TIPO
                          ) DES
                    ON  (LOWER(TRIM(PRO.ARCHIVO)) = LOWER(TRIM(DES.ARCHIVO)))
                    WHEN MATCHED THEN
                    UPDATE SET  PRO.SIZE = DES.SIZE,
                                PRO.FECHA = DES.FECHA,
                                PRO.TIPO = DES.TIPO
                    WHEN NOT MATCHED THEN
                    INSERT (ARCHIVO,SIZE,FECHA,TIPO)
                    VALUES (DES.ARCHIVO,DES.SIZE,DES.FECHA,DES.TIPO)
                    """
                    )

                resId = query_id.result()

                print('Se agrego: ' + blob.name[(blob.name.find('/') + 1):300])
                v_var_control = v_var_control + 1

client = bigquery.Client(project=PROYECTO)
query_id = client.query(
    """
    call GRAL.SP_INSERT_INICIO_ETL_LOG ('LS_BUCKET_FUN',
                                        'Fin TXT', 
                                        'ST_REP', 
                                        'ST_REP',
                                        'CRON', 
                                        'LINUX', 
                                        0, 
                                        'Finalizo de generar los TXT', 
                                        'Todo finalizo correctamente', 
                                        1) 
    """
    )
resId = query_id.result()

print ("Termino la aplicacion =D")
