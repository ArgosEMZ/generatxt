from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_zip(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cv-qa"
    BUCKET = 'amco-cv-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
                            """
                            select  ID_PAIS as ID_PAIS, -- ID_TIENDA
                                    DS_PAIS as DS_PAIS
                            from amco-cv-qa.GRAL.DIM_PAIS_TXT
                            where id_pais not in (760, 759)
                            """
                           )
    resId = query_id.result()
    
    for reg in resId:
        nombreZip = "txt_"+reg.DS_PAIS+"_"+fecha+".zip"
        
        nombreArchivo_S = "SuscripcionesDiarias_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_S     = "SuscripcionesDiarias_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl_S     = "SuscripcionesDiarias_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_C = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_C     = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl_C     = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_D = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_D     = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl_D     = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_R = "Usuarios_Eliminados_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_R     = "Usuarios_Eliminados_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl_R     = "Usuarios_Eliminados_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_U = "Transacciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_U     = "Transacciones_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl_U     = "Transacciones_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_P = "Visualizaciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_P     = "Visualizaciones_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl_P     = "Visualizaciones_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_Y = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_Y     = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl_Y     = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".ctl"
    
        nombreArchivo_T = "Temporada_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_T     = "Temporada_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl_T     = "Temporada_"+reg.DS_PAIS+"_"+fecha+".ctl"
    
        ##############################################################
        ################### Generar archivo ZIP ###################### nombreCtl

        if reg.DS_PAIS == 'MEXICO':
            os.system('cd '+path_arc+'ArchivosTXT/; '+
                      'zip '+nombreZip+
                      ' '+nombreArchivo_S+' '+nombreMD5_S+' '+nombreCtl_S+
                      ' '+nombreArchivo_C+' '+nombreMD5_C+' '+nombreCtl_C+
                      ' '+nombreArchivo_D+' '+nombreMD5_D+' '+nombreCtl_D+
                      ' '+nombreArchivo_R+' '+nombreMD5_R+' '+nombreCtl_R+
                      ' '+nombreArchivo_U+' '+nombreMD5_U+' '+nombreCtl_U+
                      ' '+nombreArchivo_P+' '+nombreMD5_P+' '+nombreCtl_P+
                      ' '+nombreArchivo_Y+' '+nombreMD5_Y+' '+nombreCtl_Y+
                      ' '+nombreArchivo_T+' '+nombreMD5_T+' '+nombreCtl_T
                     )
        else:
            os.system('cd '+path_arc+'ArchivosTXT/; '+
                      'zip '+nombreZip+
                      ' '+nombreArchivo_S+' '+nombreMD5_S+' '+nombreCtl_S+
                      ' '+nombreArchivo_C+' '+nombreMD5_C+' '+nombreCtl_C+
                      ' '+nombreArchivo_D+' '+nombreMD5_D+' '+nombreCtl_D+
                      ' '+nombreArchivo_R+' '+nombreMD5_R+' '+nombreCtl_R+
                      ' '+nombreArchivo_U+' '+nombreMD5_U+' '+nombreCtl_U+
                      ' '+nombreArchivo_P+' '+nombreMD5_P+' '+nombreCtl_P+
                      ' '+nombreArchivo_Y+' '+nombreMD5_Y+' '+nombreCtl_Y
                     )
    
    print ("Se termino de generar los ZIP")