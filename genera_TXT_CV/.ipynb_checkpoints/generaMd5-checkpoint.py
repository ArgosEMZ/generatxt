from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_MD5(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cv-qa"
    BUCKET = 'amco-cv-qa'
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        select  ID_PAIS as ID_PAIS, -- ID_TIENDA
                DS_PAIS as DS_PAIS
        from amco-cv-qa.GRAL.DIM_PAIS_TXT
        where id_pais not in (760, 759)
        """
    )
    resId = query_id.result()
    
    print ("Inicia a generar los MD5_")
    for reg in resId:
        
        nombreArchivo_S = "SuscripcionesDiarias_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_S     = "SuscripcionesDiarias_"+reg.DS_PAIS+"_"+fecha+".md5_"

        nombreArchivo_C = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_C     = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".md5_"
        
        nombreArchivo_D = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_D     = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".md5_"
        
        nombreArchivo_R = "Usuarios_Eliminados_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_R     = "Usuarios_Eliminados_"+reg.DS_PAIS+"_"+fecha+".md5_"
        
        nombreArchivo_U = "Transacciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_U     = "Transacciones_"+reg.DS_PAIS+"_"+fecha+".md5_"
        
        nombreArchivo_P = "Visualizaciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_P     = "Visualizaciones_"+reg.DS_PAIS+"_"+fecha+".md5_"
        
        nombreArchivo_Y = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_Y     = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".md5_"
        
        nombreArchivo_T = "Temporada_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_T     = "Temporada_"+reg.DS_PAIS+"_"+fecha+".md5_"
        
        ################### Generar archivo MD5 ######################
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_S+"  | awk '{ print $1 }' > "+nombreMD5_S)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_C+"  | awk '{ print $1 }' > "+nombreMD5_C)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_D+"  | awk '{ print $1 }' > "+nombreMD5_D)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_R+"  | awk '{ print $1 }' > "+nombreMD5_R)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_U+"  | awk '{ print $1 }' > "+nombreMD5_U)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_P+"  | awk '{ print $1 }' > "+nombreMD5_P)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_Y+"  | awk '{ print $1 }' > "+nombreMD5_Y)
        
        if reg.DS_PAIS == 'MEXICO':
            os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_T+"  | awk '{ print $1 }' > "+nombreMD5_T)
        
    print ("Se termino de generar los MD5_")
