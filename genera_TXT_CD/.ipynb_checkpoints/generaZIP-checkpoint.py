from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_zip(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = -0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cd-qa"
    BUCKET = 'amco-cd-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
                            """
                            select  ID_PAIS as ID_PAIS, -- ID_TIENDA
                                    DS_PAIS as DS_PAIS
                            from amco-cd-qa.GRAL.DIM_PAIS_TXT
        where id_pais not in (760)
                            """
                           )
    resId = query_id.result()
    
    for reg in resId:
        nombreZip = "txt_"+reg.DS_PAIS+"_"+fecha+".zip"
        
        nombreArchivo_S = "Usuarios_Eliminados_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_S     = "Usuarios_Eliminados_"+reg.DS_PAIS+"_"+fecha+".md5"
        nombreCtl_S     = "Usuarios_Eliminados_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_C = "Estado_Medio_Pago_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_C     = "Estado_Medio_Pago_"+reg.DS_PAIS+"_"+fecha+".md5"
        nombreCtl_C     = "Estado_Medio_Pago_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_D = "Consumos_Pyme_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_D     = "Consumos_Pyme_"+reg.DS_PAIS+"_"+fecha+".md5"
        nombreCtl_D     = "Consumos_Pyme_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_R = "Consumos_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_R     = "Consumos_"+reg.DS_PAIS+"_"+fecha+".md5"
        nombreCtl_R     = "Consumos_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_U = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_U     = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".md5"
        nombreCtl_U     = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_P = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_P     = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".md5"
        nombreCtl_P     = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".ctl"
    
        ##############################################################
        ################### Generar archivo ZIP ###################### nombreCtl

        if reg.DS_PAIS == 'MEXICO':
            os.system('cd '+path_arc+'ArchivosTXT/; '+
                  'zip '+nombreZip+
                  ' '+nombreArchivo_S+' '+nombreMD5_S+' '+nombreCtl_S+
                  ' '+nombreArchivo_C+' '+nombreMD5_C+' '+nombreCtl_C+
                  ' '+nombreArchivo_D+' '+nombreMD5_D+' '+nombreCtl_D+
                  ' '+nombreArchivo_R+' '+nombreMD5_R+' '+nombreCtl_R+
                  ' '+nombreArchivo_U+' '+nombreMD5_U+' '+nombreCtl_U+
                  ' '+nombreArchivo_P+' '+nombreMD5_P+' '+nombreCtl_P
                 )
        else:
            os.system('cd '+path_arc+'ArchivosTXT/; '+
                  'zip '+nombreZip+
                  ' '+nombreArchivo_S+' '+nombreMD5_S+' '+nombreCtl_S+
                  ' '+nombreArchivo_C+' '+nombreMD5_C+' '+nombreCtl_C+
                  ' '+nombreArchivo_R+' '+nombreMD5_R+' '+nombreCtl_R+
                  ' '+nombreArchivo_U+' '+nombreMD5_U+' '+nombreCtl_U+
                  ' '+nombreArchivo_P+' '+nombreMD5_P+' '+nombreCtl_P
                 )
    
    print ("Se termino de generar los ZIP")
