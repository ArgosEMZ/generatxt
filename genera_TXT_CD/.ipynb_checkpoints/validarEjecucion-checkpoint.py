from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os
import logging

def validaCarga(diasAtras,key_path,path_arc):
    
    logging.basicConfig(level = logging.INFO, filename = path_arc+'txt_Log.log', format= '[%(levelname)s] (%(threadName)-s %(message)s')
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    #fecha = datetime.now() + timedelta(days=-diasAtras)
    #fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cd-qa"
    BUCKET = 'amco-cd-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    v_per_carga = 0
    
    #diasAtrasQ = diasAtras + 1
    
    REG_TXT_CMN = 0
    REG_TXT_OPAG = 0
    REG_TXT_MPAG = 0
    REG_TXT_PROD = 0
    REG_TXT_EMP = 0
    REG_TXT_CLIO = 0
    REG_TXT_CLIMEP = 0
    REG_TXT_ESTPAG = 0
    REG_TXT_PAG = 0
    REG_TXT_CD_DIA = 0
    EST_TXT_CD_DIA = ''
    FD_TXT_CD_DIA = ''
    
    
    
    fechaMax = datetime.now() + timedelta(days=-100)
    ultimoPro = ''
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'FAC_CONSUMO_MASIVO_NEGOCIO'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, (REGISTROS_AFECTADOS + 1) as REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'DIM_ORIGEN_PAGO'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, (REGISTROS_AFECTADOS + 1) as REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'DIM_MEDIO_PAGO'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, (REGISTROS_AFECTADOS + 1) as REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'DIM_PRODUCTO_CD'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, (REGISTROS_AFECTADOS + 1) as REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'DIM_EMPLEADO'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'DIM_CLIENTE_ORIGEN'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'DIM_CLIENTE_MEDIO_PAGO'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'FAC_ESTADO_PAGOS'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'FAC_PAGOS'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cd-qa.GRAL.ETL_LOG
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL -0 DAY), DAY)
        AND NOMBRE_CARGA = 'TXT_CD_DIARIO'
        --AND REGISTROS_AFECTADOS > 0 
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        ORDER BY FECHA_CARGA DESC
        """
    )
    resId = query_id.result()
    
    for reg in resId:
        # Asignacion de variables 
        if reg.NOMBRE_CARGA == 'FAC_CONSUMO_MASIVO_NEGOCIO':
            REG_TXT_CMN = reg.REGISTROS_AFECTADOS
            EST_TXT_CMN = reg.ESTADO_CARGA
            FD_TXT_CMN = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'DIM_ORIGEN_PAGO':
            REG_TXT_OPAG = reg.REGISTROS_AFECTADOS
            EST_TXT_OPAG = reg.ESTADO_CARGA
            FD_TXT_OPAG = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'DIM_MEDIO_PAGO':
            REG_TXT_MPAG = reg.REGISTROS_AFECTADOS
            EST_TXT_MPAG = reg.ESTADO_CARGA
            FD_TXT_MPAG = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'DIM_PRODUCTO_CD':
            REG_TXT_PROD = reg.REGISTROS_AFECTADOS
            EST_TXT_PROD = reg.ESTADO_CARGA
            FD_TXT_PROD = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'DIM_EMPLEADO':
            REG_TXT_EMP = reg.REGISTROS_AFECTADOS
            EST_TXT_EMP = reg.ESTADO_CARGA
            FD_TXT_EMP = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'DIM_CLIENTE_ORIGEN':
            REG_TXT_CLIO = reg.REGISTROS_AFECTADOS
            EST_TXT_CLIO = reg.ESTADO_CARGA
            FD_TXT_CLIO = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'DIM_CLIENTE_MEDIO_PAGO':
            REG_TXT_CLIMEP = reg.REGISTROS_AFECTADOS
            EST_TXT_CLIMEP = reg.ESTADO_CARGA
            FD_TXT_CLIMEP = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'FAC_ESTADO_PAGOS':
            REG_TXT_ESTPAG = reg.REGISTROS_AFECTADOS
            EST_TXT_ESTPAG = reg.ESTADO_CARGA
            FD_TXT_ESTPAG = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'FAC_PAGOS':
            REG_TXT_PAG = reg.REGISTROS_AFECTADOS
            EST_TXT_PAG = reg.ESTADO_CARGA
            FD_TXT_PAG = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_CD_DIARIO':
            REG_TXT_CD_DIA = reg.REGISTROS_AFECTADOS
            EST_TXT_CD_DIA = reg.ESTADO_CARGA
            FD_TXT_CD_DIA = reg.FECHA_CARGA
            
        if reg.FECHA_CARGA >= fechaMax:
            fechaMax = reg.FECHA_CARGA
            ultimoPro = reg.NOMBRE_CARGA
            
    
    logging.info('Ultimo proceso ejecutado: ' + ultimoPro)
    logging.info('Hora de ultima actividad del proceso: ' + str(fechaMax))
    
    if ultimoPro != 'TXT_CD_DIARIO':
        if EST_TXT_CD_DIA == 'INICIO':
            logging.info('El proceso de generacion de TXT esta corriendo')
        else:
            logging.info('El proceso se puede ejecutar despues de evaluar que todas las tablas tienen registros')
            if REG_TXT_CMN > 0 and REG_TXT_OPAG > 0 and REG_TXT_MPAG > 0 and REG_TXT_PROD > 0 and REG_TXT_EMP > 0 and REG_TXT_CLIO > 0 and REG_TXT_CLIMEP > 0 and REG_TXT_ESTPAG > 0 and REG_TXT_PAG > 0:
                logging.info('Se valido que todas las tablas estan cargadas....')
                logging.info('Inserta en la ETL Log para avisar del inicio de la generacion de los TXT')
                client = bigquery.Client(project=PROYECTO)
                query_id = client.query(
                    """
                    call GRAL.SP_INSERT_INICIO_ETL_LOG ('TXT_CD_DIARIO',
                                                        'Inicio TXT', 
                                                        'ST_REP', 
                                                        'ST_REP',
                                                        'CRON', 
                                                        'LINUX', 
                                                        0, 
                                                        'Inicia el proceso de generar los TXT', 
                                                        'Inicia todo correcto', 
                                                        0) 
                    """
                    )
                
                resId = query_id.result()
                logging.info('Se inicia la generacion de los TXT =D')
                v_per_carga = 1
            else:
                logging.info('Los TXT no se pueden ejecutar por que no todas las tablas tienen registros')
             
    elif ultimoPro == 'TXT_CD_DIARIO' and EST_TXT_CD_DIA == 'INICIO':
        logging.info('El proceso de generacion de TXT esta corriendo')
        
    elif ultimoPro == 'TXT_CD_DIARIO' and EST_TXT_CD_DIA == 'FIN':
        logging.info('El proceso de generacion de TXT ya se ejecuto')
        
    return v_per_carga
