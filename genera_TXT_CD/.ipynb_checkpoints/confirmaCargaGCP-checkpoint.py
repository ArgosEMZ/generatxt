from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def confirmaCargaGCP(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = -1
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    
    PROYECTO = "amco-cd-qa"
    BUCKET = 'amco-cd-qa-txt'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    client = bigquery.Client(project=PROYECTO)
    
    ###################### Crea archivos G3 ######################
    nombreArchivo = "TXT_BQ_CD_"+fecha+""
    print ("Se inicia la crecion del archivo CD: " + nombreArchivo)
    with open(path_arc+'ArchivosTXT/' + nombreArchivo, 'w' ,encoding='UTF8') as f:
        writer = csv.writer(f, delimiter='|')
        registro = [fecha]
        writer.writerow(registro)
    
    
    ##################### Carga de archivos ######################
    # Nombre del proyecto
    PROYECTO = "amco-cd-qa"
    # Nombre del bucket 
    bucket_name = "amco-cd-qa-txt"
    # Path de lectura del archivo
    source_file_name = path_arc+'ArchivosTXT/'+nombreArchivo
    # Destino y nombre del archivo
    destination_blob_name = "TXT/" + nombreArchivo

    storage_client = storage.Client(project=PROYECTO)
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print(
        "Archivo {} cargado en {}.".format(
            source_file_name, destination_blob_name
        )
    )
    
    print ('Inserta en la ETL Log para avisar del fin de la generacion de los TXT')
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        call GRAL.SP_INSERT_INICIO_ETL_LOG ('TXT_CD_DIARIO',
                                            'Fin TXT', 
                                            'ST_REP', 
                                            'ST_REP',
                                            'CRON', 
                                            'LINUX', 
                                            0, 
                                            'Finalizo de generar los TXT', 
                                            'Todo finalizo correctamente', 
                                            1) 
        """
        )

    resId = query_id.result()

if __name__ == "__main__":
    genera_archivos()
