from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_zip(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = -0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cv-qa"
    BUCKET = 'amco-cv-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
                            """
                            select  ID_PAIS as ID_PAIS, -- ID_TIENDA
                                    DS_PAIS as DS_PAIS
                            from amco-cv-qa.GRAL.DIM_PAIS_TXT
                            where id_pais not in (760, 759)
                            """
                           )
    resId = query_id.result()
    
    for reg in resId:
        nombreZip = "txt_login_registro_"+reg.DS_PAIS+"_"+fecha+".zip"
        
        nombreArchivo_S = "txt_login_registro_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_S     = "txt_login_registro_"+reg.DS_PAIS+"_"+fecha+".md5"
        nombreCtl_S     = "txt_login_registro_"+reg.DS_PAIS+"_"+fecha+".ctl"
    
        ##############################################################
        ################### Generar archivo ZIP ###################### nombreCtl

        os.system('cd '+path_arc+'ArchivosTXT/; '+
                  'zip '+nombreZip+
                  ' '+nombreArchivo_S+' '+nombreMD5_S+' '+nombreCtl_S
                 )
    
    print ("Se termino de generar los ZIP")
