from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_MD5(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cv-qa"
    BUCKET = 'amco-cv-qa'
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        select  ID_PAIS as ID_PAIS, -- ID_TIENDA
                DS_PAIS as DS_PAIS
        from amco-cv-qa.GRAL.DIM_PAIS_TXT
        where id_pais not in (760, 759)
        """
    )
    resId = query_id.result()
    
    print ("Inicia a generar los MD5")
    for reg in resId:
        
        nombreArchivo_S = "txt_login_registro_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_S     = "txt_login_registro_"+reg.DS_PAIS+"_"+fecha+".md5"
        
        ################### Generar archivo MD5 ######################
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_S+"  | awk '{ print $1 }' > "+nombreMD5_S)
        
    print ("Se termino de generar los MD5")
