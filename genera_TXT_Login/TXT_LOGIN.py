from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_archivos(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cv-qa"
    BUCKET = 'amco-cv-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    diasAtrasQ = diasAtras + 1
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        select  ID_PAIS as ID_PAIS, -- ID_TIENDA
                DS_PAIS as DS_PAIS
        from amco-cv-qa.GRAL.DIM_PAIS_TXT
        where id_pais not in (760, 759)
        """
    )
    resId = query_id.result()
    
    for reg in resId:
        
        nombreArchivo = "txt_login_registro_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5 = "txt_login_registro_"+reg.DS_PAIS+"_"+fecha+".md5"
        nombreCtl = "txt_login_registro_"+reg.DS_PAIS+"_"+fecha+".ctl"
        nombreZip = "txt_login_registro_"+reg.DS_PAIS+"_"+fecha+".zip"

        #Query para obtener los datos
        client = bigquery.Client(project=PROYECTO)
        query_job = client.query(
            """
            select  CONCAT(
                    IFNULL(REPLACE(CAST(FORMAT_DATETIME("%d/%m/%Y %H:%M:%S", FECHA) AS STRING),'"','""')  ,''),'|',
                    IFNULL(REPLACE(CAST(TXT_CATEGORIA_DISPOSITIVO AS STRING),'"','""')  ,''),'|',
                    IFNULL(REPLACE(CAST(TX_FABRICANTE_DISPOSITIVO AS STRING),'"','""')  ,''),'|',
                    IFNULL(REPLACE(CAST(TX_MODELO_DISPOSITIVO AS STRING),'"','""')  ,''),'|',
                    IFNULL(REPLACE(CAST(TX_SO_DISPOSITIVO AS STRING),'"','""')  ,''),'|',
                    IFNULL(REPLACE(CAST(EMAIL AS STRING),'"','""')  ,''),'|',
                    IFNULL(REPLACE(CAST(PAIS AS STRING),'"','""')  ,''),'|"',
                    IFNULL(REPLACE(CAST(trim(replace(MSG, ' ', ''), ' ') AS STRING),'"','""')  ,''),'"|',
                    IFNULL(REPLACE(CAST(TYC AS STRING),'"','""')  ,''),'|',
                    IFNULL(REPLACE(CAST(NOMBRE AS STRING),'"','""')  ,''),'|',
                    IFNULL(REPLACE(CAST(APELLIDO AS STRING),'"','""')  ,''),'|',
                    IFNULL(REPLACE(CAST(ID_CLIENTE AS STRING),'"','""')  ,''),'|',
                    IFNULL(REPLACE(CAST(IP_ORIGEN AS STRING),'"','""')  ,''),'|',
                    IFNULL(REPLACE(CAST(PORT AS STRING),'"','""')  ,''),'|',
                    IFNULL(REPLACE(CAST(TIPO_EVENTO AS STRING),'"','""')  ,''),'|',
                    IFNULL(REPLACE(CAST(CODIGO_ERROR AS STRING),'"','""')  ,''),'|',
                    IFNULL(REPLACE(CAST(DESCRIPCION_ERROR AS STRING),'"','""')  ,''),'|',
                    IFNULL(REPLACE(CAST(TELEFONO AS STRING),'"','""')  ,''),''
                    ) AS dato
            from amco-cv-qa.LOGIN_REGISTRO.TXT_LOGIN
            where PAIS = '"""+str(reg.DS_PAIS)+"""'
            AND DATETIME_TRUNC(FECHA, DAY) = DATETIME_TRUNC(DATETIME_SUB(CURRENT_DATETIME(), INTERVAL """+str(diasAtrasQ)+""" DAY), DAY)
            --where (ID_PAIS = 441)
            """
        )

        results = query_job.result()

        f = open(path_arc+'ArchivosTXT/' + nombreArchivo, 'w' ,encoding='UTF-8')
        ## Primero escribir el cabecero
        f.write('"FECHA"|"TXT_CATEGORIA_DISPOSITIVO"|"TX_FABRICANTE_DISPOSITIVO"|"TX_MODELO_DISPOSITIVO"|"TX_SO_DISPOSITIVO"|"EMAIL"|"PAIS"|"MSG"|"TYC"|"NOMBRE"|"APELLIDO"|"ID_CLIENTE"|"IP_ORIGEN"|"PORT"|"TIPO_EVENTO"|"CODIGO_ERROR"|"DESCRIPCION_ERROR"|"TELEFONO"')
        f.write('\r\n')    
            
        for row in results:
            registro = row.dato
            f.write(registro)
            f.write('\r\n')
            totalReg += 1
            
        f.close()    
        print ("Se genero el archivo correctamente: " + nombreArchivo)
        print ("Total de registros " + str(totalReg))
    
        ################### Generar archivo CTL ######################
        os.system("echo \""+str(totalReg)+"\" >> "+path_arc+"ArchivosTXT/"+nombreCtl)

        totalReg = 0
