SELECT D.*,
            CASE
                WHEN TIPO_ABONO = 'SVOD' AND (UPPER(TX_ABONO_NOMBRE) LIKE '%7 DÍAS%' OR UPPER(TX_ABONO_NOMBRE) LIKE '%7 DIAS%')
                     THEN '7 Dias'
                WHEN TIPO_ABONO = 'SVOD' AND (UPPER(TX_ABONO_NOMBRE) LIKE '%30 DIAS%' OR UPPER(TX_ABONO_NOMBRE) LIKE '%30 DÍAS%')
                     THEN '30 Dias'
                WHEN TIPO_ABONO = 'SVOD' AND (UPPER(TX_ABONO_NOMBRE) LIKE '%365 DIAS%' OR UPPER(TX_ABONO_NOMBRE) LIKE '%365 DÍAS%' OR UPPER(TX_ABONO_NOMBRE) LIKE '%EVENTO INDYCAR%')
                     THEN '365 Dias'
                WHEN TIPO_ABONO = 'SVOD' AND (UPPER(TX_ABONO_NOMBRE) NOT LIKE '%DIAS%' OR UPPER(TX_ABONO_NOMBRE) LIKE '%DÍAS%')
                     THEN '30 Dias'
                                ELSE ''
                END AS PERIODO
FROM
        (
         SELECT DISTINCT
                TX_ABONO_NOMBRE,
                                CASE
                                        WHEN TX_TARIFA_TIPO IN ('M','T') AND (UPPER(TX_ABONO_NOMBRE) LIKE '%COMPRA%' OR UPPER(TX_ABONO_NOMBRE) LIKE '%DESCARGA%')
                                                THEN 'EST'
                                        WHEN TX_TARIFA_TIPO = 'M' AND (UPPER(TX_ABONO_NOMBRE) LIKE '%RENTA%')
                                                THEN 'TVOD'
                                        WHEN TX_TARIFA_TIPO = 'P' AND (UPPER(TX_ABONO_NOMBRE) LIKE '%LINEAL%' OR UPPER(TX_ABONO_NOMBRE) LIKE '%VIVO%')
                                                THEN 'LINEAL'
                                        ELSE 'SVOD'
                                END AS TIPO_ABONO
         FROM PAG.DW_TARIFA
         WHERE COD_FECHA = DATE_TRUNC(parse_date('%Y%m%d','{date}'),MONTH) 
   ORDER BY TX_ABONO_NOMBRE
        )D;