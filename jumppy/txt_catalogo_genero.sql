select distinct SQL1.ID_GRUPO ID_GRUPO, SQL1.TITULO TITULO, SQL1.TITULO_ESP TITULO_ESP, SQL1.CATEGORIA CATEGORIA, SQL1.NUMERO_TEMPORADA NUMERO_TEMPORADA, SQL1.NUMERO_CAPITULO NUMERO_CAPITULO, SQL1.NOMBRE_CAPITULO NOMBRE_CAPITULO, SQL1.NOMBRE_CAPITULO_ESP NOMBRE_CAPITULO_ESP, SQL1.DURACION_TOTAL DURACION_TOTAL, SQL1.ESTUDIO ESTUDIO, SQL1.PROVEEDOR PROVEEDOR, SQL1.TYPE TYPE, SQL1.PROGRAM_ID PROGRAM_ID, SQL1.TX_NOMBRE_SERIE TX_NOMBRE_SERIE, SQL1.GENEROS GENEROS
 from (SELECT CAT.*
       FROM
           (SELECT DISTINCT CAT.Id_Grupo,
                            CAT.Tx_Nombre AS Titulo,
                            CAT.Nombre_Esp AS Titulo_Esp,
                            CASE WHEN CAT.Cod_Serie IS NOT NULL THEN 'Serie'
                                 WHEN CAT.LiveType = 1 THEN 'Canal en Vivo'
                                 WHEN CAT.LiveType = 2 THEN 'Evento en Vivo'
                                 ELSE 'Pelicula'
                            END AS Categoria,
                            CAT.Nro_Temporada AS Numero_Temporada,
                            CAT.Nro_Capitulo AS Numero_Capitulo,
                            CAT.CAP_Nombre_Original AS Nombre_Capitulo,
                            CAT.CAP_Nombre_ESP AS Nombre_Capitulo_Esp,
                            CAT.Duracion_Grupo AS Duracion_Total,
                            EST.TX_ESTUDIO AS Estudio,
                            PROV.NOMBRE AS Proveedor,
                            Catalogo_Type.Tx_Tipo AS Type,
                            CAT.Cod_Program AS Program_Id,
                            TX_NOMBRE_SERIE,
                            gen.generos
              FROM VIS.DW_CATALOGO CAT
             LEFT JOIN (
                         select id_grupo,string_agg(genero,';' order by genero desc) as generos
                        from (
                            select distinct id_grupo,lower(tx_nombre_genero) as genero
                              from VIS.DW_CATALOGO_GENERO
                             where cod_fecha = Cod_Fecha
                               and lower(tx_nombre_genero) not like '%type%'
                               and lower(tx_nombre_genero) not like '%/%'
                               and id_genero < 100000
                            )
                        group by id_grupo
                        ) gen on (gen.id_grupo = cat.id_grupo)
                    INNER JOIN VIS.DW_ESTUDIO EST
                    ON CAT.Cod_Estudio = EST.Id_Estudio
                    AND CAT.Cod_Fecha = EST.Cod_Fecha
                    INNER JOIN VIS.DW_PROVEEDOR PROV
                    ON CAT.Id_Proveedor = PROV.Id_Proveedor
                    AND CAT.Cod_Fecha = PROV.Cod_Fecha
                    INNER JOIN TXT.DW_TXT_CAT_TYPE Catalogo_Type------------------Cambio por el TICKET   BI-2280  junio 03 de 2020
                    ON CAT.Id_Grupo = Catalogo_Type.Id_Grupo
                    AND parse_date('%Y%m%d','{date}') BETWEEN Catalogo_Type.Fecha_Desde AND Catalogo_Type.Fecha_Hasta
                  --INNER JOIN
                   --    (SELECT Id_Grupo, Id_Tipo, Tx_tipo,
                     --          TO_DATE(Cod_Fecha_Diario,'YYYYMMDD')-1 Fecha_Desde,
                       --        CASE WHEN NVL(TO_DATE(LEAD(Cod_fecha_Diario) OVER (PARTITION BY Id_Grupo ORDER BY Id_Grupo,Cod_Fecha_Diario),'YYYYMMDD'),TO_DATE(Cod_Fecha_Diario,'YYYYMMDD')) = TO_DATE(Cod_Fecha_Diario,'YYYYMMDD')
                       --            THEN TO_DATE('31/12/9999','DD/MM/YYYY')
                         --          ELSE  NVL(TO_DATE(LEAD(Cod_fecha_Diario) OVER (PARTITION BY Id_Grupo ORDER BY Id_Grupo,Cod_Fecha_Diario),'YYYYMMDD'),'31/12/9999')-2
                       --        END Fecha_Hasta
                       --FROM
                       --(
--select id_grupo,id_tipo,tx_tipo,min(cod_fecha_Insert) AS Cod_Fecha_Diario
 --from dw.dw_catalogo_type
 --group by id_grupo,id_tipo,tx_tipo
 --order by id_grupo,id_tipo,tx_tipo,4 ASC) A
                       --ORDER BY Id_Grupo, Fecha_Desde) Catalogo_Type
                    --ON CAT.Id_Grupo = Catalogo_Type.Id_Grupo
                    --AND to_date('&2','YYYYMMDD') BETWEEN Catalogo_Type.Fecha_Desde AND Catalogo_Type.Fecha_Hasta
             WHERE CAT.Cod_Fecha = DATE_TRUNC(parse_date('%Y%m%d','{date}'), MONTH)
             GROUP BY
                   CAT.Id_Grupo,
                   CAT.Tx_Nombre,
                   CAT.Nombre_Esp,
                   CASE WHEN CAT.Cod_Serie IS NOT NULL THEN 'Serie'
                        WHEN CAT.LiveType = 1 THEN 'Canal en Vivo'
                        WHEN CAT.LiveType = 2 THEN 'Evento en Vivo'
                        ELSE 'Pelicula'
                                          END,
                   CAT.Nro_Temporada,
                   CAT.Nro_Capitulo,
                   CAT.CAP_Nombre_Original,
                   CAT.CAP_Nombre_ESP,
                   CAT.Duracion_Grupo,
                   EST.TX_ESTUDIO,
                   PROV.NOMBRE,
                   Catalogo_Type.Tx_Tipo,
                   CAT.Cod_Program,
                   TX_NOMBRE_SERIE,
                   gen.generos
             ) CAT
       INNER JOIN
           (SELECT DISTINCT Id_Grupo, Cod_Pais
              FROM VIS.DW_CATALOGO_PAIS
             WHERE Cod_Fecha = DATE_TRUNC(parse_date('%Y%m%d','{date}'), MONTH)) CAT_PAIS
         ON CAT.Id_Grupo = CAT_PAIS.Id_Grupo
       WHERE CAT_PAIS.Cod_Pais IN ({id_pais})) SQL1