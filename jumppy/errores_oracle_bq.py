# Este script ontiene los archivos err de 10.11.16.21
import paramiko,sys,os,re,fileinput,csv,glob
from scp import SCPClient
import logging
from big_query import bigqueryProduct
from zipfile import ZipFile

logging.basicConfig(stream=sys.stderr, level=logging.INFO)

def createSSHClient(server, port, user, password):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(server, port=port, username=user, password=password,key_filename='bi_dwh')
    return client

if __name__ == "__main__":
	directory = r"tmp"
	files = glob.glob('tmp/*')
	for f in files:
		os.remove(f)
	os.rmdir(directory)
	os.mkdir(directory)
	directory="/u02/external/"
	ssh = createSSHClient("10.11.16.21", 22, "oracle", "oraware")
	ftp = ssh.open_sftp()
	files = ftp.listdir(directory)
	scp = SCPClient(ssh.get_transport())
	connbi = bigqueryProduct("amco-cv-qa","accesskey/amco-cv-qa-d7be3f5c7e10.json")
	r = re.compile('(?<=[0-9,])(D,+)(?<=[0-9,])')
	r2 = re.compile('(?<=[0-9])(DD,)(?<=[0-9,])')
	r3 = re.compile('\n(?!D)')
	for file in files :
		if(file.endswith(".err") ):
			print(file.split(".")[0])
			scp.get(directory+file,"tmp/"+file)
			with open("tmp/"+file, 'r+',encoding='latin-1') as f:
			    text = f.read()
			    text = r.sub('\nD,', text)
			    text = r2.sub('\nD,', text)
			    text = r3.sub('',text)
			    comas_conteo=re.findall(",",text)
			    lineas_conteo=re.findall("\n",text)
			    columnas_prom=len(comas_conteo) if len(comas_conteo) == 0 or len(lineas_conteo) == 0 else round(len(comas_conteo)/len(lineas_conteo))
			    print(columnas_prom)
			    f.seek(0)
			    f.write(text)
			    f.truncate()
			connbi.set_csv_bulk("tmp/"+file,"ST_CARGAS_ORACLE_ERROR",file.split(".")[0],columnas_prom)