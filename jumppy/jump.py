# Este obitene archivos de jump
import hashlib,os,multiprocessing,argparse,ssl
from zipfile import ZipFile
import wget,json,datetime,glob,re,csv,shutil,ftplib
from big_query import bigqueryProduct
from urllib import request, parse


def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def get_text_file(fname):
	ctl_b = open(fname)
	ctl = ctl_b.read()
	ctl_b.close()
	return ctl 

def create_text_file(fname,text):
	f = open(fname, "a")
	f.write(text)
	f.close()

def send_file(fichero_origen):
	s = ftplib.FTP("dl.ftp.jumptvs.com", "claro", "8A5EcH89KCRm77sY")
	try:
		f = open(fichero_origen, 'rb')
		s.storbinary('STOR ' + fichero_origen, f)
		f.close()
		s.quit()
	except:
		print("No se ha podido encontrar el fichero " + fichero_origen)

def createdirectory(cc):
	outdir = 'tempo/'+cc
	if not os.path.exists(outdir):
		os.mkdir(outdir)
	outdir = 'zipdest/'+cc
	if not os.path.exists(outdir):
		os.mkdir(outdir)

def genera_archivo(cc,row):
	global now,archivos,conexion
	createdirectory(cc)
	for attribute,arch in archivos["archivos"].items():
		file_aux = filename = arch["nombre"].replace("{fecha}",now.strftime("%Y%m%d")).replace("{nombre_pais}",row["nombre"])
		print("\nProcesando " + filename+" "+now.strftime("%Y%m%d"))
		sql = get_text_file(arch["archivosql"])
		fechaf = now - datetime.timedelta(days=1)
		sql = sql.replace("{date}",fechaf.strftime("%Y%m%d")).replace("{id_pais}",row["codigo"])
		data = conexion.get_data(sql)
		print("\nDatos del dia "+fechaf.strftime("%Y%m%d"))	
		filename = "tempo/"+cc+"/"+filename
		data.to_csv(filename+".csv",sep="|", encoding='utf-8-sig',quoting=csv.QUOTE_ALL,doublequote=False,index=False)
		md5_file = md5(filename+".csv")
		ctl = get_text_file(filename+".csv")
		lineas_conteo=re.findall("\n",ctl)
		f = create_text_file(filename+".md5", md5_file)
		f = create_text_file(filename+".ctl", str(len(lineas_conteo)))
		print("\n Archivo creado : "+filename)
		with ZipFile('temp/txt_'+row['nombre']+'_'+now.strftime("%Y%m%d")+'.zip','a') as zip:
			zip.write(filename+".csv",file_aux+".csv" )
			zip.write(filename+".md5",file_aux+".md5")
			zip.write(filename+".ctl",file_aux+".ctl")
	shutil.move('temp/txt_'+row['nombre']+'_'+now.strftime("%Y%m%d")+'.zip','zipdest/'+cc)
	print("\nArchivo esta en : zipdest/"+cc+"/"+row['nombre']+'_'+now.strftime("%Y%m%d")+'.zip')
	#send_file('zipdest/txt_'+row['nombre']+'_'+now.strftime("%Y%m%d")+'.zip')

def multiprocess_main(attribute,row):
	global archivos,conexion
	url = "http://10.20.5.31/"
	print("Pais a procesar "+ row["nombre"])
	file_name = "txt_"+row["nombre"]+"_"+now.strftime("%Y%m%d")+".zip"
	print(file_name)
	if("zipdest/"+attribute+"/"+file_name not in glob.glob('zipdest/'+attribute+"/*")):
		if "temp/"+attribute+"/"+file_name in glob.glob('temp/'+attribute+"/*"):
			print("Archivo ya existe en temp")
		else:
			try:
				url_tmp = url+row["dir"]+"/"+file_name
				wget.download(url_tmp, out="temp/")
				genera_archivo(attribute,row)
			except Exception as e:
				print("Error descargando el archivo "+ url_tmp)
	else:
		print("El archivo se encuentra en zipdest")

if __name__ == "__main__":
	global now,archivos,conexion
	parser = argparse.ArgumentParser()
	parser.add_argument('--fecha', dest='fecha', required=False,
                        help='fecha a procesar con formato %Y-%m-%d ejemplo: 2022-05-30',
                        default=str(datetime.datetime.now().strftime("%Y-%m-%d")))
	parser.add_argument('--dias', dest='dias', required=False,
                        help='dias anteriores a procesar a partir de la fecha, ejemplo: 3',
                        default=1)
	parser.add_argument('--pais', dest='pais', required=False,
                        help='pais a procesar , ejemplo: mx',
                        default="all")
	args = parser.parse_args()
	genera = False
	archivos = json.load(open('conf.json'))
	args.fecha = datetime.datetime.strptime(args.fecha, "%Y-%m-%d")
	for i in range(1,int(args.dias)+1):
		now = args.fecha - datetime.timedelta(days=(int(i)-1))
		paises = archivos["paises"]
		conexion = bigqueryProduct("amco-cv-qa","/home/txtamco/accesos/amco-cv-qa-d7be3f5c7e10.json")
		process = []
		for attribute,row in paises.items():
			procesar = False
			if(args.pais == "all"):
				procesar = True
			else:
				if(args.pais == attribute):
					procesar = True
			if(procesar):
				p = multiprocessing.Process(target=multiprocess_main,args=(attribute,row))
				p.start()
				process.append(p)
		for p in process:
			p.join()