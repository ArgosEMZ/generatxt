from sqlalchemy import create_engine, MetaData, Table, and_
from sqlalchemy.sql import select
import pandas as pd
from sqlalchemy import text
import json
from google.oauth2 import service_account

class bigqueryProduct:

	engine = None
	project = None
	columns = None
	table= None
	
	def __init__(self,project,keyfile):
		engine_path = 'bigquery://' + project
		self.project = project
		self.engine = create_engine(engine_path,credentials_path=keyfile)

	def get_data(self,query):
		return pd.read_sql_query(query,self.engine)

	def ex_data(self,query):
		sql = text(query)
		return self.engine.execute(sql)

	def set_csv_bulk(self,file,dataset,tabla,columnas):
		credentials = service_account.Credentials.from_service_account_file(
        filename="accesskey/amco-cv-qa-d7be3f5c7e10.json",
        scopes=['https://www.googleapis.com/auth/cloud-platform'])
		columnas_text = []
		for colum in range(columnas+2):
			columnas_text.append("columna"+(str(colum)))
		print(columnas)
		print(columnas_text)
		columnas_text.append("FECHA_INSERT")
		file = pd.read_csv(file,header=None,encoding='latin-1',names=columnas_text,error_bad_lines=False,engine='python', dtype=str)
		print(file.head())
		#file.set_axis(columnas_text,axis=1)
		file["FECHA_INSERT"] = pd.Timestamp("now") 
		file.to_gbq(dataset+'.'+tabla, 
                 "amco-cv-qa",
                 chunksize=10000, 
                 if_exists='append',credentials=credentials
                 )

	def getConteotable(self,dataset,table):
		sql = "SELECT count(1) as conteo FROM "+dataset+"."+table+" "
		exe = pd.read_sql_query(sql,self.engine)
		reg = 0
		if(exe.empty):
			reg = 0
		else:
			for v in exe['conteo']:
				reg = v
		return reg

	def getConteotableFecha(self,dataset,table):
		sql = """SELECT count(1) as conteo FROM """+dataset+"""."""+table+""" WHERE DATE(FECHA_CORTE) = CURRENT_DATE("America/Mexico_City") -1 """
		print(sql)
		exe = pd.read_sql_query(sql,self.engine)
		reg = 0
		if(exe.empty):
			reg = 0
		else:
			for v in exe['conteo']:
				reg = v
		return reg

	def getInfotable(self,dataset,table):
		sql = "SELECT column_name,data_type,ordinal_position FROM "+dataset+".INFORMATION_SCHEMA.COLUMNS where table_catalog='"+self.project+"'  and table_schema='"+dataset+"' and table_name='"+table+"' "
		self.columns = pd.read_sql_query(sql,self.engine)
		self.table = table
		return self.columns

	def columnsTojson(self):
		return self.columns.to_dict(orient='records')

	def getModelJson(self,alias,carpeta,tabla):
		archivo = "Models/"+ str(alias) +"/"+str(carpeta)+"/"+str(tabla)+".json"
		j1 = json.loads(open(archivo).read())
		return j1
	
	def getColumns(self,schema,alias,carpeta,tabla):
		data = self.getModelJson(alias,carpeta,tabla)
		data = pd.json_normalize(data[schema]["columns"])
		columnas1  = ",".join(data["column_name"].tolist())
		return columnas1

	def queryPage(self,dataset,table,columnas,inicio,final):
		query = """select MD5(CONCAT("""+columnas+""")) from """+dataset+"""."""+table
		query_add = query
		query_add = query_add.replace("@inicio",str(inicio))
		query_add = query_add.replace("@final",str(final))	
		return query_add