WITH suscripciones AS
  (SELECT dp.pais,
          cs.id_cliente,
          cs.cod_partner_operacion,
           case
             WHEN cs.val_precio=0
                   and cs.cant_renovaciones <= CASE WHEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0) >= coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                                                    THEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0)
                                                    ELSE coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                                                END
                   and cs.cant_renovaciones = 0
                   and t.MEDIO_PAGO_NOMBRE<>'Pincode'
                   and CASE WHEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0) >= coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                            THEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0)
                            ELSE coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                       END = 1
                   then 1
             WHEN cs.val_precio=0
                   and cs.cant_renovaciones <= CASE WHEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0) >= coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                                                    THEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0)
                                                    ELSE coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                                                END
                   and cs.cant_renovaciones = 0
                   and t.MEDIO_PAGO_NOMBRE<>'Pincode'
                   and CASE WHEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0) >= coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                            THEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0)
                            ELSE coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                       END > 1
                   then 2
             when cs.val_precio=0
                   and cs.cant_renovaciones <= CASE WHEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0) >= coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                                                    THEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0)
                                                    ELSE coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                                               END
                   and CASE WHEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0) >= coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                            THEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0)
                            ELSE coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                       END>0
                   and t.MEDIO_PAGO_NOMBRE<>'Pincode'
                   then 3
             when cs.val_precio=0
                   and cs.cant_renovaciones <=CASE WHEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0) >= coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                                                    THEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0)
                                                    ELSE coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                                               END
                   and CASE WHEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0) >= coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                            THEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0)
                            ELSE coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                            END>0
                   and t.MEDIO_PAGO_NOMBRE='Pincode'
                   then 3
             when cs.val_precio<>0
                   and cs.cant_renovaciones>=CASE WHEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0) >= coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                                                    THEN coalesce(P.VAL_PERIODOS_TOTALES_PROMO,0)
                                                    ELSE coalesce(t.VAL_TARIFA_CANT_PERIOD_BONIF,0)
                                               END
                   then 4
             when (cs.val_precio=0 AND Cant_renovaciones = 0)
                 THEN 2
             when (cs.val_precio=0 AND Cant_renovaciones > 0 AND t.Id_Abono IN (980,1920))
                 THEN 3
             when (cs.val_precio>0 AND Cant_renovaciones = 0 AND t.Id_Abono  IN (980,1920))
                 THEN 1
             when
                (cs.val_precio>0)
                THEN 4
             when
                (cs.val_precio=0)
                THEN 3
             else
                5
        end Product_Type,
          row_number() OVER (PARTITION BY cs.id_cliente,
                                          t.Id_Abono
                             ORDER BY Id_Abono,
                                      cs.Cod_Partner_Operacion DESC, cod_FECHA_SUSC_pais DESC) AS rnk
   FROM PAG.DW_CLIENTE_SUSCRIPCION cs
   INNER JOIN PAG.DW_TARIFA t ON cs.cod_Tarifa = t.id_tarifa AND t.cod_fecha =  DATE_TRUNC(parse_date('%Y%m%d','{date}'),MONTH) 
   INNER JOIN CTE.DW_CLIENTE_FNAL c ON c.id_cliente = cs.id_cliente and c.cod_fecha = DATE_TRUNC(parse_date('%Y%m%d','{date}'),MONTH) 
   INNER JOIN GRAL.DIM_PAIS_ORACLE dp ON dp.id_pais_nat = c.cod_pais
                                AND dp.fecha_baja IS NULL
   LEFT OUTER JOIN PAG.DW_CLIENTE_PROMOCION CP
        ON cs.Cod_Partner_Operacion = CP.Cod_Partner_Operacion
        And cs.Id_Cliente = CP.Id_Cliente
        And cs.Cod_Fecha = CP.Cod_Fecha
   LEFT OUTER JOIN PAG.DW_PROMOCION P
        ON CP.Cod_Promocion = P.Id_Promocion
        And CP.Cod_Fecha = P.Cod_Fecha
   WHERE tx_suscripcion IN ('ADQ','REN')
     AND cs.cod_fecha IN (DATE_TRUNC(parse_date('%Y%m%d','{date}'),MONTH)  ,
                          DATE_ADD(DATE_TRUNC(parse_date('%Y%m%d','{date}'),MONTH), INTERVAL -1 MONTH )  ,
                          DATE_ADD(DATE_TRUNC(parse_date('%Y%m%d','{date}'),MONTH), INTERVAL 1 MONTH ))  
     AND cs.cod_fecha_susc_pais BETWEEN DATETIME_ADD(parse_datetime("%Y%m%d %H:%M","{date} 00:00"),INTERVAL -30 DAY) AND parse_datetime("%Y%m%d %H:%M","{date} 23:59")
     AND c.cod_pais = {id_pais})
SELECT s.Pais, s.Id_cliente, s.Cod_partner_operacion, s.Product_Type
FROM suscripciones s
WHERE s.rnk = 1
;