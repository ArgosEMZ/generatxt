from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_archivos(diasAtras, key_path, path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cm-qa"
    BUCKET = 'amco-cm-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    diasAtrasQ = diasAtras + 1
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        SELECT  TIE.ID_TIENDA as ID_TIENDA,
                TIE.TXT_TIENDA, 
                PAI.DS_PAIS as DS_PAIS
        FROM amco-cm-qa.GRAL.DIM_TIENDA TIE
        JOIN amco-cm-qa.GRAL.DIM_PAIS_TXT pai ON TIE.TXT_ISO_PAIS = pai.ID_REGION
        """
    )
    resId = query_id.result()
    
    for reg in resId:
    
        nombreArchivo = "Descargas_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5 = "Descargas_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl = "Descargas_"+reg.DS_PAIS+"_"+fecha+".ctl"
        nombreZip = "Descargas_"+reg.DS_PAIS+"_"+fecha+".zip"

        #Query para obtener los datos
        client = bigquery.Client(project=PROYECTO)
        query_job = client.query(
            """
            select  DISTINCT 
                    cast(TXT_DESCARGAS_VW.PAIS AS STRING) AS PAIS,
                    cast(TXT_DESCARGAS_VW.ID_USUARIO AS STRING) AS ID_USUARIO,
                    cast(TXT_DESCARGAS_VW.ID_SUBSCRIPTION AS STRING) AS ID_SUBSCRIPTION,
                    cast(TXT_DESCARGAS_VW.FECHA AS STRING) AS FECHA,
                    cast(TXT_DESCARGAS_VW.FECHA_LOCAL AS STRING) AS FECHA_LOCAL,
                    cast(TXT_DESCARGAS_VW.ID_CANCION AS STRING) AS ID_CANCION,
                    cast(TXT_DESCARGAS_VW.TXT_PRODUCTO AS STRING) AS TXT_PRODUCTO,
                    cast(TXT_DESCARGAS_VW.TXT_TIPO_PAGO AS STRING) AS TXT_TIPO_PAGO,
                    cast(TXT_DESCARGAS_VW.PRECIO AS STRING) AS PRECIO,
                    cast(TXT_DESCARGAS_VW.TX_DISP_FABRICANTE_ORIGINAL AS STRING) AS TX_DISP_FABRICANTE_ORIGINAL,
                    cast(TXT_DESCARGAS_VW.TX_DISP_MODELO_ORIGINAL AS STRING) AS TX_DISP_MODELO_ORIGINAL,
                    cast(TXT_DESCARGAS_VW.SISTEMA_OPERATIVO_DISPOSITVO AS STRING) AS SISTEMA_OPERATIVO_DISPOSITVO,
                    cast(TXT_DESCARGAS_VW.BONIFICADO AS STRING) AS BONIFICADO,
                    cast(TXT_DESCARGAS_VW.TXT_MODALIDAD_PAGO AS STRING) AS TXT_MODALIDAD_PAGO 
            from amco-cm-qa.ST_PAG.TXT_DESCARGAS_VW TXT_DESCARGAS_VW  
            where (1=1) 
            And (TXT_DESCARGAS_VW.ID_TIENDA = """+str(reg.ID_TIENDA)+""")
            --And (TXT_DESCARGAS_VW.ID_TIENDA = 273)
            And DATETIME_TRUNC(TXT_DESCARGAS_VW.FECHA_LOCAL, DAY) = DATE_TRUNC(DATE_SUB(CURRENT_DATETIME('America/Mexico_City'), INTERVAL """+str(diasAtrasQ)+""" DAY), DAY)
            --And DATETIME_TRUNC(TXT_DESCARGAS_VW.FECHA_LOCAL, DAY) = DATE_TRUNC(DATE_SUB(CURRENT_DATETIME('America/Mexico_City'), INTERVAL 2 DAY), DAY)
            """
        )

        results = query_job.result()

        with open(path_arc+'ArchivosTXT/' + nombreArchivo, 'w' ,encoding='UTF8') as f:
            
            for row in results:
                ## Primero escribir el cabecero
                if totalReg == 0:
                    writer = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC, delimiter='|', quotechar='\"', lineterminator='\n')
                    registro = ["PAIS","ID_USUARIO","ID_SUBSCRIPTION","FECHA","FECHA_LOCAL","ID_CANCION","TXT_PRODUCTO","TXT_TIPO_PAGO","PRECIO","TX_DISP_FABRICANTE_ORIGINAL","TX_DISP_MODELO_ORIGINAL","SISTEMA_OPERATIVO_DISPOSITVO","BONIFICADO","TXT_MODALIDAD_PAGO"]
                    writer.writerow(registro)
                    
                registro = [row.PAIS, row.ID_USUARIO, row.ID_SUBSCRIPTION, row.FECHA, row.FECHA_LOCAL, row.ID_CANCION, row.TXT_PRODUCTO, row.TXT_TIPO_PAGO, row.PRECIO, row.TX_DISP_FABRICANTE_ORIGINAL, row.TX_DISP_MODELO_ORIGINAL, row.SISTEMA_OPERATIVO_DISPOSITVO, row.BONIFICADO, row.TXT_MODALIDAD_PAGO]

                writer = csv.writer(f,quoting=csv.QUOTE_NONNUMERIC,delimiter='|', quotechar='\"', lineterminator='\n')
                writer.writerow(registro)
                totalReg += 1
        print ("Se genero el archivo correctamente: " + nombreArchivo)
        print ("Total de registros " + str(totalReg))
    
        ################### Generar archivo CTL ######################
        os.system("echo \""+str(totalReg)+"\" >> "+path_arc+"ArchivosTXT/"+nombreCtl)
        totalReg = 0