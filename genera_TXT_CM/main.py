from datetime import date, datetime, timedelta
from numpy import random
from time import sleep
import os
import threading
import logging
import sys

import txt_suscripciones as suscripciones
import txt_catalogo as catalogo
import txt_descargas as descargas
import txt_reproducciones as reproducciones
import txt_usuarios as usuarios
import txt_catalogo_playlist as catalogo_playlist
import txt_catalogo_canciones_playlist as catalogo_canciones_playlist

import sp_txt_cargas_rep as sp_txt_cargas_rep
import generaZIP as generaZIP
import cargaZipGCP as cargaZipGCP
import generaMd5 as generaMd5
import renombraArchivos as renombraArchivos
import confirmaCargaGCP as confirmaCargaGCP
import validarEjecucion as validarEjecucion

sysVarDias = sys.argv[1]

###### Desarollo ###### 
#path_arc = "/home/jupyter/script_TXT/genera_TXT_CM/"
#key_path = "/home/jupyter/accesos/amco-cm-qa-0e07a4f6f481.json"

###### Produccion ###### ArchivosTXT
path_arc = "/home/txtamco/generatxt/genera_TXT_CM/"
key_path = "/home/txtamco/accesos/amco-cm-qa-0e07a4f6f481.json"

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path

logging.basicConfig(level = logging.INFO, filename = path_arc+'txt_Log.log', format= '[%(levelname)s] (%(threadName)-s %(message)s')

sleeptime = random.uniform(1, 60)
print ("sleeping de:"+ str(sleeptime)+ "segundos")
logging.info("sleeping de:"+ str(sleeptime)+ "segundos")
sleep(sleeptime)
print("sleeping termino")
logging.info("sleeping termino")

# Variable que nos indica con que fecha se tiene que generar el archivo (Numero de dias atras)
diasAtras = int(sysVarDias)

######### Muestra un print con la informacion a procesar ###########
fecha = datetime.now() + timedelta(days=-diasAtras)
fecha = fecha.strftime("%Y%m%d")
diasAtrasQ = diasAtras + 1
fechaQ = datetime.now() + timedelta(days=-diasAtrasQ)
fechaQ = fechaQ.strftime("%Y%m%d")
logging.info('El archivo se creara con la fecha: ' + fecha)
logging.info('El query se ejecuta con la fecha: ' + fechaQ)
v_per_carga = 0

v_per_carga = validarEjecucion.validaCarga(diasAtras, key_path, path_arc)

logging.info('El resultado de la funcion de PY es: ' + str(v_per_carga))

if v_per_carga == 1 or int(sysVarDias) > 0:
    ################### Limpia espacio de trabajo ######################
    os.system('mkdir '+path_arc+'ArchivosTXT')
    os.system('cd '+path_arc+'ArchivosTXT/; '+ 'rm -r '+path_arc+'ArchivosTXT/*')
    
    ############## Ejecuta SP ST_REP.SP_TXT_CARGAS_REP #################
    logging.info('Ejecuta el SP ST_REP.SP_TXT_CARGAS_REP()')
    sp_txt_cargas_rep.ejecuta_SP(diasAtras, key_path, path_arc)

    ####################### Genera los TXT #############################
    
    logging.info('Inicia a crear los TXT')
    t1 = threading.Thread(name="suscripciones", target=suscripciones.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t2 = threading.Thread(name="usuarios", target=usuarios.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t3 = threading.Thread(name="reproducciones", target=reproducciones.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t4 = threading.Thread(name="descargas", target=descargas.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t5 = threading.Thread(name="catalogo", target=catalogo.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t6 = threading.Thread(name="catalogo_playlist", target=catalogo_playlist.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t7 = threading.Thread(name="catalogo_canciones_playlist", target=catalogo_canciones_playlist.genera_archivos, args=(diasAtras, key_path, path_arc,))

    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t5.start()
    t6.start()
    t7.start()

    t1.join()
    t2.join()
    t3.join()
    t4.join()
    t5.join()
    t6.join()
    t7.join()

    logging.info('Termino de generar los archivos TXT')
    ##################### Carga los archivos ###########################
    ##########renombraArchivos.renombra_arc(diasAtras)
    logging.info('Inicia a generar los MD5')
    generaMd5.genera_MD5(diasAtras, key_path, path_arc)
    logging.info('Genera los ZIP')
    generaZIP.genera_zip(diasAtras, key_path, path_arc)
    logging.info('Carga los ZIP')
    cargaZipGCP.carga_zip(diasAtras, key_path, path_arc)

    ############# Confirma la carga de los archivos a GCP ##############
    logging.info('Manda archivo de confirmacion a GCP')
    confirmaCargaGCP.confirmaCargaGCP(diasAtras, key_path, path_arc)