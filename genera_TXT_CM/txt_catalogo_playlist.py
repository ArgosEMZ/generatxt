from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_archivos(diasAtras, key_path, path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cm-qa"
    BUCKET = 'amco-cm-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    diasAtrasQ = diasAtras + 1
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        SELECT  TIE.ID_TIENDA as ID_TIENDA,
                TIE.TXT_TIENDA, 
                PAI.DS_PAIS as DS_PAIS
        FROM amco-cm-qa.GRAL.DIM_TIENDA TIE
        JOIN amco-cm-qa.GRAL.DIM_PAIS_TXT pai ON TIE.TXT_ISO_PAIS = pai.ID_REGION
        """
    )
    resId = query_id.result()
    
    for reg in resId:
    
        nombreArchivo = "Catalogo_Playlist_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5 = "Catalogo_Playlist_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl = "Catalogo_Playlist_"+reg.DS_PAIS+"_"+fecha+".ctl"
        nombreZip = "Catalogo_Playlist_"+reg.DS_PAIS+"_"+fecha+".zip"

        #Query para obtener los datos
        client = bigquery.Client(project=PROYECTO)
        query_job = client.query(
            """
            select  CAST(DIST_COMP.ID_PLAYLIST AS STRING) AS ID_PLAYLIST,
                    CAST(DIST_COMP.NOMBRE_PLAYLIST AS STRING) AS NOMBRE_PLAYLIST,
                    CAST(DIST_COMP.FECHA_ALTA AS STRING) AS FECHA_ALTA 
            from (SELECT DISTINCT 
                         DIM_PLAYLIST.ID_PLAYLIST AS ID_PLAYLIST ,
                         REPLACE(REPLACE(REPLACE(REPLACE(DIM_PLAYLIST.TXT_PLAYLIST, CHR(10), ''), CHR(13), ''),',',''),'"','') AS NOMBRE_PLAYLIST ,
                         DIM_PLAYLIST.FD_FECHA_CREACION AS FECHA_ALTA   
                  FROM amco-cm-qa.REP.DIM_PLAYLIST DIM_PLAYLIST,
                       amco-cm-qa.REP.FAC_CANCION_ESC_DET FAC_CANCION_ESC_DET       
                  WHERE (DIM_PLAYLIST.ID_PLAYLIST=FAC_CANCION_ESC_DET.ID_EVENT_STREAMING)
                  AND DATE_TRUNC(FAC_CANCION_ESC_DET.FD_FECHA_LOCAL, DAY) = DATE_TRUNC(DATE_SUB(CURRENT_DATETIME('America/Mexico_City'), INTERVAL """+str(diasAtrasQ)+""" DAY), DAY)
                  --AND DATE_TRUNC(FAC_CANCION_ESC_DET.FD_FECHA_LOCAL, DAY) = DATE_TRUNC(DATE_SUB(CURRENT_DATETIME('America/Mexico_City'), INTERVAL 2 DAY), DAY)
                  AND (FAC_CANCION_ESC_DET.ID_TIENDA = """+str(reg.ID_TIENDA)+""")
                  --AND (FAC_CANCION_ESC_DET.ID_TIENDA = 273)
                  ) DIST_COMP  
            where (1=1)
            """
        )

        results = query_job.result()

        with open(path_arc+'ArchivosTXT/' + nombreArchivo, 'w' ,encoding='UTF8') as f:
            
            for row in results:
                if totalReg == 0:
                    ## Primero escribir el cabecero
                    writer = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC, delimiter='|', quotechar='\"', lineterminator='\n')
                    registro = ["ID_PLAYLIST","NOMBRE_PLAYLIST","FECHA_ALTA"]
                    writer.writerow(registro)
                
                registro = [row.ID_PLAYLIST, row.NOMBRE_PLAYLIST, row.FECHA_ALTA]

                writer = csv.writer(f,quoting=csv.QUOTE_NONNUMERIC,delimiter='|', quotechar='\"', lineterminator='\n')
                writer.writerow(registro)
                totalReg += 1
        print ("Se genero el archivo correctamente: " + nombreArchivo)
        print ("Total de registros " + str(totalReg))
    
        ################### Generar archivo CTL ######################
        os.system("echo \""+str(totalReg)+"\" >> "+path_arc+"ArchivosTXT/"+nombreCtl)

        totalReg = 0