from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_archivos(diasAtras, key_path, path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cm-qa"
    BUCKET = 'amco-cm-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    diasAtrasQ = diasAtras + 1
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        SELECT  TIE.ID_TIENDA as ID_TIENDA,
                TIE.TXT_TIENDA, 
                PAI.DS_PAIS as DS_PAIS
        FROM amco-cm-qa.GRAL.DIM_TIENDA TIE
        JOIN amco-cm-qa.GRAL.DIM_PAIS_TXT pai ON TIE.TXT_ISO_PAIS = pai.ID_REGION
        """
    )
    resId = query_id.result()
    
    for reg in resId:
    
        nombreArchivo = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5 = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".ctl"
        nombreZip = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".zip"

        #Query para obtener los datos
        client = bigquery.Client(project=PROYECTO)
        query_job = client.query(
            """
            SELECT        
                    CAST(TXT_CATALOGO_VW.ID_CANCION AS STRING) AS ID_CANCION,
                    CAST(TXT_CATALOGO_VW.TXT_CANCION AS STRING) AS TXT_CANCION,
                    CAST(TXT_CATALOGO_VW.ID_ALBUM AS STRING) AS ID_ALBUM,
                    CAST(TXT_CATALOGO_VW.TXT_ALBUM AS STRING) AS TXT_ALBUM,
                    CAST(TXT_CATALOGO_VW.ID_SELLO AS STRING) AS ID_SELLO,
                    CAST(TXT_CATALOGO_VW.TXT_SELLO AS STRING) AS TXT_SELLO,
                    CAST(TXT_CATALOGO_VW.ID_DISQUERA AS STRING) AS ID_DISQUERA,
                    CAST(TXT_CATALOGO_VW.TXT_DISQUERA AS STRING) AS TXT_DISQUERA,
                    CAST(TXT_CATALOGO_VW.TXT_ARTISTAS AS STRING) AS TXT_ARTISTAS,
                    CAST(TXT_CATALOGO_VW.TXT_AUTORES AS STRING) AS TXT_AUTORES,
                    CAST(TXT_CATALOGO_VW.DURACION AS STRING) AS DURACION,
                    CAST(TXT_CATALOGO_VW.TXT_GENERO_MUSICAL AS STRING) AS TXT_GENERO_MUSICAL
            FROM amco-cm-qa.ST_REP.TXT_CATALOGO_VW TXT_CATALOGO_VW
            --WHERE (TXT_CATALOGO_VW.ID_TIENDA = 273)
            WHERE (TXT_CATALOGO_VW.ID_TIENDA = """+str(reg.ID_TIENDA)+""") 
            --AND DATE_TRUNC(TXT_CATALOGO_VW.FECHA_LOCAL, DAY) = DATE_SUB(DATE_TRUNC(CURRENT_DATETIME('America/Mexico_City'),DAY), INTERVAL 2 DAY)
            AND DATE_TRUNC(TXT_CATALOGO_VW.FECHA_LOCAL, DAY) = DATE_SUB(DATE_TRUNC(CURRENT_DATETIME('America/Mexico_City'),DAY), INTERVAL """+str(diasAtrasQ)+""" DAY)  
            """
        )

        results = query_job.result()

        with open(path_arc+'ArchivosTXT/' + nombreArchivo, 'w' ,encoding='UTF8') as f:
            
            for row in results:
                if totalReg == 0:
                    ## Primero escribir el cabecero
                    writer = csv.writer(f, delimiter='|', lineterminator='\n')
                    registro = ["ID_CANCION","TXT_CANCION","ID_ALBUM","TXT_ALBUM","ID_SELLO","TXT_SELLO","ID_DISQUERA","TXT_DISQUERA","TXT_ARTISTAS","TXT_AUTORES","DURACION","TXT_GENERO_MUSICAL"]
                    writer.writerow(registro)
                
                registro = [row.ID_CANCION, row.TXT_CANCION, row.ID_ALBUM, row.TXT_ALBUM, row.ID_SELLO, row.TXT_SELLO, row.ID_DISQUERA, row.TXT_DISQUERA, row.TXT_ARTISTAS, row.TXT_AUTORES, row.DURACION, row.TXT_GENERO_MUSICAL]

                writer = csv.writer(f,quoting=csv.QUOTE_NONNUMERIC,delimiter='|', quotechar='\"', lineterminator='\n')
                writer.writerow(registro)
                totalReg += 1
        print ("Se genero el archivo correctamente: " + nombreArchivo)
        print ("Total de registros " + str(totalReg))
    
        ################### Generar archivo CTL ######################
        os.system("echo \""+str(totalReg)+"\" >> "+path_arc+"ArchivosTXT/"+nombreCtl)

        totalReg = 0