from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_MD5(diasAtras, key_path, path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cm-qa"
    BUCKET = 'amco-cm-qa'
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        SELECT  TIE.ID_TIENDA as ID_TIENDA,
                TIE.TXT_TIENDA, 
                PAI.DS_PAIS as DS_PAIS
        FROM amco-cm-qa.GRAL.DIM_TIENDA TIE
        JOIN amco-cm-qa.GRAL.DIM_PAIS_TXT pai ON TIE.TXT_ISO_PAIS = pai.ID_REGION
        """
    )
    resId = query_id.result()
    
    print ("Inicia a generar los MD5_")
    for reg in resId:
    
        nombreArchivo_S = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_S     = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".md5_"
        
        nombreArchivo_C = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_C     = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".md5_"
        
        nombreArchivo_D = "Descargas_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_D     = "Descargas_"+reg.DS_PAIS+"_"+fecha+".md5_"
        
        nombreArchivo_R = "Reproducciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_R     = "Reproducciones_"+reg.DS_PAIS+"_"+fecha+".md5_"
        
        nombreArchivo_U = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_U     = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".md5_"
        
        nombreArchivo_P = "Catalogo_Playlist_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_P     = "Catalogo_Playlist_"+reg.DS_PAIS+"_"+fecha+".md5_"
        
        nombreArchivo_Y = "Catalogo_Canciones_Playlist_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_Y     = "Catalogo_Canciones_Playlist_"+reg.DS_PAIS+"_"+fecha+".md5_"
        
        ################### Generar archivo MD5 ######################
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_S+"  | awk '{ print $1 }' > "+nombreMD5_S)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_C+"  | awk '{ print $1 }' > "+nombreMD5_C)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_D+"  | awk '{ print $1 }' > "+nombreMD5_D)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_R+"  | awk '{ print $1 }' > "+nombreMD5_R)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_U+"  | awk '{ print $1 }' > "+nombreMD5_U)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_P+"  | awk '{ print $1 }' > "+nombreMD5_P)
        os.system("cd "+path_arc+"ArchivosTXT/; "+" md5sum "+nombreArchivo_Y+"  | awk '{ print $1 }' > "+nombreMD5_Y)
        
    print ("Se termino de generar los MD5_")