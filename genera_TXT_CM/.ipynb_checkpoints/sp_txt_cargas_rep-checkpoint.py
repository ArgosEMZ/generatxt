from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def ejecuta_SP(diasAtras, key_path, path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cm-qa"
    BUCKET = 'amco-cm-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    diasAtrasQ = diasAtras + 1
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        call ST_REP.SP_TXT_CARGAS_REP("""+str(diasAtrasQ)+""")
        """
    )
    resId = query_id.result()
    
    for reg in resId:
        
        print (reg)