from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_archivos(diasAtras, key_path, path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cm-qa"
    BUCKET = 'amco-cm-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    diasAtrasQ = diasAtras + 1
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        SELECT  TIE.ID_TIENDA as ID_TIENDA,
                TIE.TXT_TIENDA, 
                PAI.DS_PAIS as DS_PAIS
        FROM amco-cm-qa.GRAL.DIM_TIENDA TIE
        JOIN amco-cm-qa.GRAL.DIM_PAIS_TXT pai ON TIE.TXT_ISO_PAIS = pai.ID_REGION
        """
    )
    resId = query_id.result()
    
    for reg in resId:
    
        nombreArchivo = "Catalogo_Canciones_Playlist_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5 = "Catalogo_Canciones_Playlist_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl = "Catalogo_Canciones_Playlist_"+reg.DS_PAIS+"_"+fecha+".ctl"
        nombreZip = "Catalogo_Canciones_Playlist_"+reg.DS_PAIS+"_"+fecha+".zip"

        #Query para obtener los datos
        client = bigquery.Client(project=PROYECTO)
        query_job = client.query(
            """
            select  CAST(DIST_COMP.ID_CANCION AS STRING) AS ID_CANCION,
                    CAST(DIST_COMP.ID_PLAYLIST AS STRING) AS ID_PLAYLIST,
                    CAST(DIST_COMP.POSICION_PLAYLIST AS STRING) AS POSICION_PLAYLIST 
            from(SELECT DISTINCT 
                        DIM_PLAYLIST_ELEMENT.ID_CANCION AS ID_CANCION ,
                        DIM_PLAYLIST_ELEMENT.ID_PLAYLIST AS ID_PLAYLIST ,
                        DIM_PLAYLIST_ELEMENT.POSITION AS POSICION_PLAYLIST   
                 FROM amco-cm-qa.REP.DIM_PLAYLIST_ELEMENT DIM_PLAYLIST_ELEMENT ,  
                      amco-cm-qa.REP.FAC_CANCION_ESC_DET FAC_CANCION_ESC_DET       
                 WHERE (DIM_PLAYLIST_ELEMENT.ID_CANCION=FAC_CANCION_ESC_DET.ID_CANCION AND DIM_PLAYLIST_ELEMENT.ID_PLAYLIST=FAC_CANCION_ESC_DET.ID_EVENT_STREAMING) 
                 AND (FAC_CANCION_ESC_DET.ID_TIENDA = """+str(reg.ID_TIENDA)+""")
                 --AND (FAC_CANCION_ESC_DET.ID_TIENDA = 273)
                 AND DATE_TRUNC(FAC_CANCION_ESC_DET.FD_FECHA_LOCAL, DAY) = DATE_TRUNC(DATE_SUB(CURRENT_DATETIME('America/Mexico_City'), INTERVAL """+str(diasAtrasQ)+""" DAY), DAY)
                 --AND DATE_TRUNC(FAC_CANCION_ESC_DET.FD_FECHA_LOCAL, DAY) = DATE_TRUNC(DATE_SUB(CURRENT_DATETIME('America/Mexico_City'), INTERVAL 2 DAY), DAY)
                ) DIST_COMP  
            where (1=1)
            """
        )

        results = query_job.result()

        with open(path_arc+'ArchivosTXT/' + nombreArchivo, 'w' ,encoding='UTF8') as f:
            
            for row in results:
                if totalReg == 0:
                    ## Primero escribir el cabecero
                    writer = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC, delimiter='|', quotechar='\"', lineterminator='\n')
                    registro = ["ID_CANCION","ID_PLAYLIST","POSICION_PLAYLIST"]
                    writer.writerow(registro)
                
                registro = [row.ID_CANCION, row.ID_PLAYLIST, row.POSICION_PLAYLIST]

                writer = csv.writer(f,quoting=csv.QUOTE_NONNUMERIC,delimiter='|', quotechar='\"', lineterminator='\n')
                writer.writerow(registro)
                totalReg += 1
        print ("Se genero el archivo correctamente: " + nombreArchivo)
        print ("Total de registros " + str(totalReg))
    
        ################### Generar archivo CTL ######################
        os.system("echo \""+str(totalReg)+"\" >> "+path_arc+"ArchivosTXT/"+nombreCtl)

        totalReg = 0