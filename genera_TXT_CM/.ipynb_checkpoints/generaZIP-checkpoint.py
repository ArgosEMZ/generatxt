from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_zip(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = -0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cm-qa"
    BUCKET = 'amco-cm-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
                            """
                            SELECT  TIE.ID_TIENDA as ID_TIENDA,
                                    TIE.TXT_TIENDA, 
                                    PAI.DS_PAIS as DS_PAIS
                            FROM amco-cm-qa.GRAL.DIM_TIENDA TIE
                            JOIN amco-cm-qa.GRAL.DIM_PAIS_TXT pai ON TIE.TXT_ISO_PAIS = pai.ID_REGION
                            """
                           )
    resId = query_id.result()
    
    print ("Se inician a generar los ZIP")
    for reg in resId:
        nombreZip = "txt_"+reg.DS_PAIS+"_"+fecha+".zip"
        
        nombreArchivo_S = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_S     = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl_S     = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_C = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_C     = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl_C     = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_D = "Descargas_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_D     = "Descargas_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl_D     = "Descargas_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_R = "Reproducciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_R     = "Reproducciones_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl_R     = "Reproducciones_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_U = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_U     = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl_U     = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_P = "Catalogo_Playlist_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_P     = "Catalogo_Playlist_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl_P     = "Catalogo_Playlist_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_Y = "Catalogo_Canciones_Playlist_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5_Y     = "Catalogo_Canciones_Playlist_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl_Y     = "Catalogo_Canciones_Playlist_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
    
        ##############################################################
        ################### Generar archivo ZIP ###################### nombreCtl

        os.system('cd '+path_arc+'ArchivosTXT/; '+
                  'zip '+nombreZip+
                  ' '+nombreArchivo_S+' '+nombreMD5_S+' '+nombreCtl_S+
                  ' '+nombreArchivo_C+' '+nombreMD5_C+' '+nombreCtl_C+
                  ' '+nombreArchivo_D+' '+nombreMD5_D+' '+nombreCtl_D+
                  ' '+nombreArchivo_R+' '+nombreMD5_R+' '+nombreCtl_R+
                  ' '+nombreArchivo_U+' '+nombreMD5_U+' '+nombreCtl_U+
                  ' '+nombreArchivo_P+' '+nombreMD5_P+' '+nombreCtl_P+
                  ' '+nombreArchivo_Y+' '+nombreMD5_Y+' '+nombreCtl_Y
                 )
    
    print ("Se termino de generar los ZIP")