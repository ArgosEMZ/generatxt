from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_archivos(diasAtras, key_path, path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cm-qa"
    BUCKET = 'amco-cm-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    diasAtrasQ = diasAtras + 1
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        SELECT  TIE.ID_TIENDA as ID_TIENDA,
                TIE.TXT_TIENDA, 
                PAI.DS_PAIS as DS_PAIS
        FROM amco-cm-qa.GRAL.DIM_TIENDA TIE
        JOIN amco-cm-qa.GRAL.DIM_PAIS_TXT pai ON TIE.TXT_ISO_PAIS = pai.ID_REGION
        """
    )
    resId = query_id.result()
    
    for reg in resId:
    
        nombreArchivo = "Reproducciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5 = "Reproducciones_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl = "Reproducciones_"+reg.DS_PAIS+"_"+fecha+".ctl"
        nombreZip = "Reproducciones_"+reg.DS_PAIS+"_"+fecha+".zip"

        #Query para obtener los datos
        client = bigquery.Client(project=PROYECTO)
        query_job = client.query(
            """
            SELECT
                    cast(TXT_REPRODUCCIONES_VW.TXT_PAIS AS STRING) AS PAIS,
                    cast(TXT_REPRODUCCIONES_VW.ID_USUARIO AS STRING) AS ID_USUARIO,
                    cast(TXT_REPRODUCCIONES_VW.ID_CANCION AS STRING) AS ID_CANCION,
                    cast(TXT_REPRODUCCIONES_VW.FECHA_REPRODUCCION AS STRING) AS FECHA_REPRODUCCION,
                    cast(TXT_REPRODUCCIONES_VW.HORA_REPRODUCCION AS STRING) AS HORA_REPRODUCCION,
                    cast(TXT_REPRODUCCIONES_VW.FD_FECHA_LOCAL AS STRING) AS FECHA_LOCAL,
                    cast(TXT_REPRODUCCIONES_VW.ID_SUBSCRIPTION AS STRING) AS ID_SUBSCRIPTION,
                    cast(TXT_REPRODUCCIONES_VW.TXT_PRODUCTO AS STRING) AS TXT_PRODUCTO,
                    cast(TXT_REPRODUCCIONES_VW.NRO_MSISDN AS STRING) AS NRO_MSISDN,
                    cast(TXT_REPRODUCCIONES_VW.TXT_TIPO_PAGO AS STRING) AS TXT_TIPO_PAGO,
                    cast(TXT_REPRODUCCIONES_VW.TXT_NOMBRE AS STRING) AS TXT_DISP_FABRICANTE_ORIGINAL,
                    cast(TXT_REPRODUCCIONES_VW.TXT_MODEL AS STRING) AS TXT_DISP_MODELO_ORIGINAL,
                    cast(TXT_REPRODUCCIONES_VW.TXT_SISTEMA_OPERATIVO AS STRING) AS SISTEMA_OPERATIVO_DISPOSITIVO,
                    cast(TXT_REPRODUCCIONES_VW.NRO_DURACION_STR AS STRING) AS TIEMPO_REPRODUCCION,
                    cast(TXT_REPRODUCCIONES_VW.ID_PLAYLIST AS STRING) AS ID_PLAYLIST,
                    cast(TXT_REPRODUCCIONES_VW.TIPO_PLAYLIST AS STRING) AS TIPO_PLAYLIST,
                    CASE TXT_REPRODUCCIONES_VW.ID_EVENT_STREAMING_TYPE WHEN 9 THEN 'SI' ELSE 'NO' END AS EVENT_STREAMING_TYPE,
                    cast(TXT_REPRODUCCIONES_VW.ID_APLICACION AS STRING) AS ID_APLICACION,
                    cast(TXT_REPRODUCCIONES_VW.TX_APLICACION AS STRING) AS TX_APLICACION 
            FROM amco-cm-qa.ST_REP.TXT_REPRODUCCIONES_VW TXT_REPRODUCCIONES_VW  
            WHERE (TXT_REPRODUCCIONES_VW.ID_TIENDA = """+str(reg.ID_TIENDA)+""") 
            --WHERE (TXT_REPRODUCCIONES_VW.ID_TIENDA = 273)
            and DATE_TRUNC(TXT_REPRODUCCIONES_VW.FD_FECHA_LOCAL, DAY) = DATE_SUB(DATE_TRUNC(CURRENT_DATETIME('America/Mexico_City'),DAY), INTERVAL """+str(diasAtrasQ)+""" DAY)
            --and and DATE_TRUNC(TXT_REPRODUCCIONES_VW.FD_FECHA_LOCAL, DAY) = DATE_SUB(DATE_TRUNC(CURRENT_DATETIME('America/Mexico_City'),DAY), INTERVAL 2 DAY)
            """
        )

        results = query_job.result()

        with open(path_arc+'ArchivosTXT/' + nombreArchivo, 'w' ,encoding='UTF8', newline='\n') as f:
            
            for row in results:
                if totalReg == 0:
                    ## Primero escribir el cabecero
                    writer = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC, delimiter='|', quotechar='\"', lineterminator='\n')
                    registro = ["PAIS","ID_USUARIO","ID_CANCION","FECHA_REPRODUCCION","HORA_REPRODUCCION","FECHA_LOCAL","ID_SUBSCRIPTION","TXT_PRODUCTO","NRO_MSISDN","TXT_TIPO_PAGO","TXT_DISP_FABRICANTE_ORIGINAL","TXT_DISP_MODELO_ORIGINAL","SISTEMA_OPERATIVO_DISPOSITIVO","TIEMPO_REPRODUCCION","ID_PLAYLIST","TIPO_PLAYLIST","EVENT_STREAMING_TYPE","ID_APLICACION","TX_APLICACION"]
                    writer.writerow(registro)
                
                registro = [row.PAIS, row.ID_USUARIO, row.ID_CANCION, row.FECHA_REPRODUCCION, row.HORA_REPRODUCCION, row.FECHA_LOCAL, row.ID_SUBSCRIPTION, row.TXT_PRODUCTO, row.NRO_MSISDN, row.TXT_TIPO_PAGO, row.TXT_DISP_FABRICANTE_ORIGINAL, row.TXT_DISP_MODELO_ORIGINAL, row.SISTEMA_OPERATIVO_DISPOSITIVO, row.TIEMPO_REPRODUCCION, row.ID_PLAYLIST, row.TIPO_PLAYLIST, row.EVENT_STREAMING_TYPE, row.ID_APLICACION, row.TX_APLICACION]

                writer = csv.writer(f,quoting=csv.QUOTE_NONNUMERIC,delimiter='|', quotechar='\"', lineterminator='\n')
                writer.writerow(registro)
                totalReg += 1
        print ("Se genero el archivo correctamente: " + nombreArchivo)
        print ("Total de registros " + str(totalReg))
    
        ################### Generar archivo CTL ######################
        os.system("echo \""+str(totalReg)+"\" >> "+path_arc+"ArchivosTXT/"+nombreCtl)

        totalReg = 0