from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_archivos(diasAtras, key_path, path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cm-qa"
    BUCKET = 'amco-cm-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    diasAtrasQ = diasAtras + 1
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        SELECT  TIE.ID_TIENDA as ID_TIENDA,
                TIE.TXT_TIENDA, 
                PAI.DS_PAIS as DS_PAIS
        FROM amco-cm-qa.GRAL.DIM_TIENDA TIE
        JOIN amco-cm-qa.GRAL.DIM_PAIS_TXT pai ON TIE.TXT_ISO_PAIS = pai.ID_REGION
        """
    )
    resId = query_id.result()
    
    for reg in resId:
    
        nombreArchivo = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5 = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".ctl"
        nombreZip = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".zip"

        #Query para obtener los datos
        client = bigquery.Client(project=PROYECTO)
        query_job = client.query(
            """
            select  cast(DIST_COMP.PAIS      as STRING) AS PAIS,
                    cast(DIST_COMP.ID_USUARIO    as STRING)    AS ID_USUARIO,
                    cast(DIST_COMP.ID_SUSCRIPCION   as STRING)     AS ID_SUSCRIPCION,
                    cast(DIST_COMP.FD_FECHA_ULTIMA_TRANSACCION   as STRING)     AS FD_FECHA_ULTIMA_TRANSACCION,
                    cast(DIST_COMP.FD_FECHA_ULTIMA_TRANSAC_LOCAL as STRING)       AS FD_FECHA_ULTIMA_TRANSAC_LOCAL,
                    cast(DIST_COMP.VAL_PRECIO     as STRING)   AS VAL_PRECIO,
                    cast(DIST_COMP.TXT_PRODUCTO   as STRING)     AS TXT_PRODUCTO,
                    cast(DIST_COMP.TXT_SUSCRIPCION as STRING)       AS TXT_SUSCRIPCION,
                    cast(DIST_COMP.NRO_MSISDN   as STRING)     AS NRO_MSISDN,
                    cast(DIST_COMP.TXT_CANAL   as STRING)     AS TXT_CANAL,
                    cast(DIST_COMP.TX_SUSCRIPCION_BAJA   as STRING)     AS TX_SUSCRIPCION_BAJA,
                    cast(DIST_COMP.CANT_RENOVACIONES as STRING)       AS CANT_RENOVACIONES,
                    cast(DIST_COMP.ID_CANAL_BAJA     as STRING)   AS ID_CANAL_BAJA,
                    cast(DIST_COMP.TXT_CANAL_BAJA      as STRING)  AS TXT_CANAL_BAJA,
                    cast(DIST_COMP.FECHA_SUSCRIPCION   as STRING)     AS FECHA_SUSCRIPCION,
                    cast(DIST_COMP.HORA_SUSCRIPCION    as STRING)    AS HORA_SUSCRIPCION,
                    cast(DIST_COMP.MEDIO_PAGO_ASOCIADO   as STRING)     AS MEDIO_PAGO_ASOCIADO,
                    cast(DIST_COMP.TXT_MODALIDAD_PAGO  as STRING)      AS TXT_MODALIDAD_PAGO,
                    cast(DIST_COMP.FECHA_CANCELACION  as STRING)      AS FECHA_CANCELACION,
                    cast(DIST_COMP.HORA_CANCELACION   as STRING)     AS HORA_CANCELACION,
                    cast(DIST_COMP.TIPO_SERVICIO     as STRING)   AS TIPO_SERVICIO,
                    cast(DIST_COMP.REINTENTOS_COBRO   as STRING)     AS REINTENTOS_COBRO,
                    cast(DIST_COMP.PINCODE_REDIMIDO    as STRING)    AS PINCODE_REDIMIDO,
                    cast(DIST_COMP.NRO_DIAS_SUS_ACTIVA   as STRING)     AS NRO_DIAS_SUS_ACTIVA,
                    cast(DIST_COMP.ESTADO_USUARIO     as STRING)   AS ESTADO_USUARIO,
                    cast(DIST_COMP.ESTATUS_SUSCRIPCION    as STRING)    AS ESTATUS_SUSCRIPCION,
                    cast(DIST_COMP.TXT_PROMOCION     as STRING)   AS TXT_PROMOCION,
                    cast(DIST_COMP.MOTIVO_CANCELACION   as STRING)     AS MOTIVO_CANCELACION
            from(SELECT DISTINCT 
                        TXT_SUSCRIPCIONES_VW_2.PAIS AS PAIS ,
                        TXT_SUSCRIPCIONES_VW_2.ID_USUARIO AS ID_USUARIO ,
                        TXT_SUSCRIPCIONES_VW_2.ID_SUSCRIPCION AS ID_SUSCRIPCION ,
                        TXT_SUSCRIPCIONES_VW_2.FD_FECHA_ULTIMA_TRANSACCION AS FD_FECHA_ULTIMA_TRANSACCION ,
                        TXT_SUSCRIPCIONES_VW_2.FD_FECHA_ULTIMA_TRANSAC_LOCAL AS FD_FECHA_ULTIMA_TRANSAC_LOCAL ,
                        TXT_SUSCRIPCIONES_VW_2.VAL_PRECIO AS VAL_PRECIO ,
                        TXT_SUSCRIPCIONES_VW_2.TXT_PRODUCTO AS TXT_PRODUCTO ,
                        TXT_SUSCRIPCIONES_VW_2.TXT_SUSCRIPCION AS TXT_SUSCRIPCION ,
                        TXT_SUSCRIPCIONES_VW_2.NRO_MSISDN AS NRO_MSISDN ,
                        TXT_SUSCRIPCIONES_VW_2.TXT_CANAL AS TXT_CANAL ,
                        TXT_SUSCRIPCIONES_VW_2.TX_SUSCRIPCION_BAJA AS TX_SUSCRIPCION_BAJA ,
                        TXT_SUSCRIPCIONES_VW_2.CANT_RENOVACIONES AS CANT_RENOVACIONES ,
                        TXT_SUSCRIPCIONES_VW_2.ID_CANAL_BAJA AS ID_CANAL_BAJA ,
                        TXT_SUSCRIPCIONES_VW_2.TXT_CANAL_BAJA AS TXT_CANAL_BAJA ,
                        TXT_SUSCRIPCIONES_VW_2.FD_FECHA_SUSCRIPCION AS FECHA_SUSCRIPCION ,
                        TXT_SUSCRIPCIONES_VW_2.FD_HORA_SUSCRIPCION AS HORA_SUSCRIPCION ,
                        TXT_SUSCRIPCIONES_VW_2.TXT_TIPO_PAGO AS MEDIO_PAGO_ASOCIADO ,
                        TXT_SUSCRIPCIONES_VW_2.TXT_MODALIDAD_PAGO AS TXT_MODALIDAD_PAGO ,
                        TXT_SUSCRIPCIONES_VW_2.FD_FECHA_CANCELACION AS FECHA_CANCELACION ,
                        TXT_SUSCRIPCIONES_VW_2.FD_HORA_CANCELACION AS HORA_CANCELACION ,
                        TXT_SUSCRIPCIONES_VW_2.TIPO_SERVICIO AS TIPO_SERVICIO ,
                        TXT_SUSCRIPCIONES_VW_2.REINTENTOS_DE_COBRO AS REINTENTOS_COBRO ,
                        TXT_SUSCRIPCIONES_VW_2.PINCODE_REDIMIDO AS PINCODE_REDIMIDO ,
                        TXT_SUSCRIPCIONES_VW_2.DIAS_SUSCRIPCION_ACTIVA AS NRO_DIAS_SUS_ACTIVA ,
                        TXT_SUSCRIPCIONES_VW_2.ESTADO AS ESTADO_USUARIO ,
                        TXT_SUSCRIPCIONES_VW_2.ESTATUS_SUSCRIPCION AS ESTATUS_SUSCRIPCION ,
                        TXT_SUSCRIPCIONES_VW_2.TXT_PROMOCION AS TXT_PROMOCION ,
                        TXT_SUSCRIPCIONES_VW_2.MOTIVO_CANCELACION AS MOTIVO_CANCELACION   
                 FROM amco-cm-qa.ST_PAG.TXT_SUSCRIPCIONES_VW_2 TXT_SUSCRIPCIONES_VW_2
                 --WHERE(TXT_SUSCRIPCIONES_VW_2.ID_TIENDA = 273)
                 WHERE(TXT_SUSCRIPCIONES_VW_2.ID_TIENDA = """+str(reg.ID_TIENDA)+""")
                 --AND DATE_TRUNC(TXT_SUSCRIPCIONES_VW_2.FECHA, DAY) =  DATE_TRUNC(DATE_SUB(CURRENT_DATETIME('America/Mexico_City'), INTERVAL 2 DAY), DAY)
                 AND DATE_TRUNC(TXT_SUSCRIPCIONES_VW_2.FECHA, DAY) =  DATE_TRUNC(DATE_SUB(CURRENT_DATETIME('America/Mexico_City'), INTERVAL """+str(diasAtrasQ)+""" DAY), DAY)
                 ) DIST_COMP
            where (1=1)
            """
        )

        results = query_job.result()

        with open(path_arc+'ArchivosTXT/' + nombreArchivo, 'w' ,encoding='UTF8') as f:
            
            for row in results:
                if totalReg == 0:
                    ## Primero escribir el cabecero
                    writer = csv.writer(f, delimiter='|', lineterminator='\n')
                    registro = ["PAIS", "ID_USUARIO", "ID_SUSCRIPCION", "FD_FECHA_ULTIMA_TRANSACCION", "FD_FECHA_ULTIMA_TRANSAC_LOCAL", "VAL_PRECIO", "TXT_PRODUCTO", "TXT_SUSCRIPCION", "NRO_MSISDN", "TXT_CANAL", "TX_SUSCRIPCION_BAJA", "CANT_RENOVACIONES", "ID_CANAL_BAJA", "TXT_CANAL_BAJA", "FECHA_SUSCRIPCION", "HORA_SUSCRIPCION", "MEDIO_PAGO_ASOCIADO", "TXT_MODALIDAD_PAGO", "FECHA_CANCELACION", "HORA_CANCELACION", "TIPO_SERVICIO", "REINTENTOS_COBRO", "PINCODE_REDIMIDO", "NRO_DIAS_SUS_ACTIVA", "ESTADO_USUARIO", "ESTATUS_SUSCRIPCION", "TXT_PROMOCION", "MOTIVO_CANCELACION"]
                    writer.writerow(registro)
                
                registro = [row.PAIS, row.ID_USUARIO, row.ID_SUSCRIPCION, row.FD_FECHA_ULTIMA_TRANSACCION, row.FD_FECHA_ULTIMA_TRANSAC_LOCAL, row.VAL_PRECIO, row.TXT_PRODUCTO, row.TXT_SUSCRIPCION, row.NRO_MSISDN, row.TXT_CANAL, row.TX_SUSCRIPCION_BAJA, row.CANT_RENOVACIONES, row.ID_CANAL_BAJA, row.TXT_CANAL_BAJA, row.FECHA_SUSCRIPCION, row.HORA_SUSCRIPCION, row.MEDIO_PAGO_ASOCIADO, row.TXT_MODALIDAD_PAGO, row.FECHA_CANCELACION, row.HORA_CANCELACION, row.TIPO_SERVICIO, row.REINTENTOS_COBRO, row.PINCODE_REDIMIDO, row.NRO_DIAS_SUS_ACTIVA, row.ESTADO_USUARIO, row.ESTATUS_SUSCRIPCION, row.TXT_PROMOCION, row.MOTIVO_CANCELACION]

                writer = csv.writer(f,quoting=csv.QUOTE_NONNUMERIC,delimiter='|', quotechar='\"', lineterminator='\n')
                writer.writerow(registro)
                totalReg += 1
        print ("Se genero el archivo correctamente: " + nombreArchivo)
        print ("Total de registros " + str(totalReg))
    
        ################### Generar archivo CTL ######################
        os.system("echo \""+str(totalReg)+"\" >> "+path_arc+"ArchivosTXT/"+nombreCtl)

        totalReg = 0