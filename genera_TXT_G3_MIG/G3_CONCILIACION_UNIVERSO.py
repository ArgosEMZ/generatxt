from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

# Falta agregar el header del archivo

def genera_archivos(diasAtras, key_path, path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = -1
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    
    PROYECTO = "amco-cv-qa"
    BUCKET = 'amco-cv-qa-txt'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    client = bigquery.Client(project=PROYECTO)
    
    nombreArchivo = "G3_CONCILIACION_UNIVERSO_"+fecha+".csv"

    #Query para obtener los datos
    client = bigquery.Client(project=PROYECTO)
    query_job = client.query(
        """
        select  cast(VIRTUAL as string) as VIRTUAL,
                FORMAT_DATE('%Y/%m/%d', FECHA_MOVIMIENTO) as FECHA_MOVIMIENTO,
                cast(HORA_MOVIMIENTO as string) as HORA_MOVIMIENTO,
                cast(FOLIO_MOVIMIENTO as string) as FOLIO_MOVIMIENTO,
                FORMAT_DATE('%Y/%m/%d', FECHA_CONTRATACION) as FECHA_CONTRATACION,
                cast(FOLIO_CONTRATACION as string) as FOLIO_CONTRATACION,
                FORMAT_DATE('%Y/%m/%d', FECHA_BAJA) as FECHA_BAJA,
                cast(FOLIO_BAJA as string) as FOLIO_BAJA,
                cast(ITEM as string) as ITEM,
                cast(IMPORTE as string) as IMPORTE,
                cast(ESTATUS as string) as ESTATUS,
                cast(MOVIMIENTO as string) as MOVIMIENTO
        from amco-cv-qa.GRAL.G3_CONCILIACION_COMPLETO_VW
        """
    )

    results = query_job.result()

    print ("Se inicia la crecion del archivo: " + nombreArchivo)
    with open(path_arc+'ArchivosTXT/' + nombreArchivo, 'w' ,encoding='UTF8') as f:
        ## Primero escribir el cabecero
        writer = csv.writer(f, delimiter='|')
        registro = ["VIRTUAL", "FECHA_MOVIMIENTO", "HORA_MOVIMIENTO", "FOLIO_MOVIMIENTO", "FECHA_CONTRATACION", "FOLIO_CONTRATACION", "FECHA_BAJA", "FOLIO_BAJA", "ITEM", "IMPORTE", "ESTATUS", "MOVIMIENTO"]
        writer.writerow(registro)

        for row in results:
            registro = [row.VIRTUAL, row.FECHA_MOVIMIENTO, row.HORA_MOVIMIENTO, row.FOLIO_MOVIMIENTO, row.FECHA_CONTRATACION, row.FOLIO_CONTRATACION, row.FECHA_BAJA, row.FOLIO_BAJA, row.ITEM, row.IMPORTE, row.ESTATUS, row.MOVIMIENTO]

            writer = csv.writer(f, delimiter='|')
            writer.writerow(registro)
            totalReg += 1
    print ("Se genero el archivo correctamente: " + nombreArchivo)
    print ("Total de registros " + str(totalReg))

    totalReg = -1
    
    