from datetime import date, datetime, timedelta
from numpy import random
from time import sleep
import os
import threading
import logging
import sys

import G3_CONCILIACION_UNIVERSO as G3_CONCILIACION_UNIVERSO
import G3_CONCILIACION_DIARIA as G3_CONCILIACION_DIARIA

import cargaZipGCP as cargaZipGCP
import confirmaCargaGCP as confirmaCargaGCP
import validarEjecucion as validarEjecucion

sysVarDias = sys.argv[1]

###### Desarollo ###### 
#path_arc = "/home/jupyter/script_TXT/genera_TXT_G3_MIG/"
#key_path = "/home/jupyter/accesos/amco-cv-qa-d7be3f5c7e10.json"

###### Produccion ###### ArchivosTXT
path_arc = "/home/txtamco/generatxt/genera_TXT_G3_MIG/"
key_path = "/home/txtamco/accesos/amco-cv-qa-d7be3f5c7e10.json"

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path

logging.basicConfig(level = logging.INFO, filename = path_arc+'txt_Log.log', format= '[%(levelname)s] (%(threadName)-s %(message)s')

# Variable que nos indica con que fecha se tiene que generar el archivo (Numero de dias atras)
diasAtras = int(sysVarDias)

######### Muestra un print con la informacion a procesar ###########
fecha = datetime.now() + timedelta(days=-diasAtras)
fecha = fecha.strftime("%Y%m%d")
diasAtrasQ = diasAtras + 1
fechaQ = datetime.now() + timedelta(days=-diasAtrasQ)
fechaQ = fechaQ.strftime("%Y%m%d")
logging.info('El archivo se creara con la fecha: ' + fecha)
logging.info('El query se ejecuta con la fecha: ' + fechaQ)
v_per_carga = 0

v_per_carga = validarEjecucion.validaCarga(diasAtras, key_path, path_arc)

logging.info('El resultado de la funcion de PY es: ' + str(v_per_carga))

if v_per_carga == 1 or int(sysVarDias)>0 :
    ################### Limpia espacio de trabajo ######################
    os.system('mkdir '+path_arc+'ArchivosTXT')
    os.system('cd '+path_arc+'ArchivosTXT/; '+ 'rm -r '+path_arc+'ArchivosTXT/*')

    ####################### Genera los TXT #############################
    logging.info("Inicia a crear los TXT")
    t1 = threading.Thread(name="G3_CONCILIACION_UNIVERSO", target=G3_CONCILIACION_UNIVERSO.genera_archivos, args=(diasAtras, key_path, path_arc,))
    t2 = threading.Thread(name="G3_CONCILIACION_DIARIA", target=G3_CONCILIACION_DIARIA.genera_archivos, args=(diasAtras, key_path, path_arc,))

    t1.start()
    t2.start()

    t1.join()
    t2.join()

    logging.info("Termino de crear los TXT")
    ##################### Carga los archivos ###########################
    ########renombraArchivos.renombra_arc(diasAtras)
    logging.info("Carga los ZIP a GCP")
    cargaZipGCP.carga_zip(diasAtras, key_path, path_arc)

    ############# Confirma la carga de los archivos a GCP ################
    logging.info("Manda archivo de confirmacion a GCP")
    confirmaCargaGCP.confirmaCargaGCP(diasAtras, key_path, path_arc)