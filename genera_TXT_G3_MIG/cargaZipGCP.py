from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def carga_zip(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    diasAtrasQ = diasAtras + 1
    fechaQ = datetime.now() + timedelta(days=-diasAtrasQ)
    fechaQ = fechaQ.strftime("%Y%m%d")
    
    nombreArchivo = "G3_CONCILIACION_DIARIA_"+fecha+".csv"
    
    ##################### Carga de archivos ######################
    # Nombre del proyecto
    PROYECTO = "amco-cv-qa"
    # Nombre del bucket 
    bucket_name = "amco-cv-qa-txt"
    # Path de lectura del archivo
    source_file_name = path_arc+'ArchivosTXT/'+nombreArchivo
    # Destino y nombre del archivo
    destination_blob_name = "G3/" + nombreArchivo

    storage_client = storage.Client(project=PROYECTO)
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print(
        "Archivo {} cargado en {}.".format(
            source_file_name, destination_blob_name
        )
    )
    
    
    nombreArchivo = "G3_CONCILIACION_UNIVERSO_"+fecha+".csv"
    
    ##################### Carga de archivos ######################
    # Nombre del proyecto
    PROYECTO = "amco-cv-qa"
    # Nombre del bucket 
    bucket_name = "amco-cv-qa-txt"
    # Path de lectura del archivo
    source_file_name = path_arc+'ArchivosTXT/'+nombreArchivo
    # Destino y nombre del archivo
    destination_blob_name = "G3/" + nombreArchivo

    storage_client = storage.Client(project=PROYECTO)
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print(
        "Archivo {} cargado en {}.".format(
            source_file_name, destination_blob_name
        )
    )
    
    
    
    