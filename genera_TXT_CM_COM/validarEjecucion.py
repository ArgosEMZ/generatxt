from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os
import logging

def validaCarga(diasAtras, key_path, path_arc):
    
    logging.basicConfig(level = logging.INFO, filename = path_arc+'txt_Log.log', format= '[%(levelname)s] (%(threadName)-s %(message)s')
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    #fecha = datetime.now() + timedelta(days=-diasAtras)
    #fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cm-qa"
    BUCKET = 'amco-cm-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    v_per_carga = 0
    
    #diasAtrasQ = diasAtras + 1
    
    REG_TXT_CM_DIARIO = 0
    REG_TXT_CM_DIARIO_COM = 0
    
    EST_TXT_CM_DIARIO = ''
    EST_TXT_CM_DIARIO_COM = ''
    
    FD_TXT_CM_DIARIO = ''
    FD_TXT_CM_DIARIO_COM = ''
    
    fechaMax = datetime.now() + timedelta(days=-100)
    ultimoPro = ''
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, (REGISTROS_AFECTADOS + 1) as REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cm-qa.GRAL.ETL_LOG
        --WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL """+str(diasAtras)+""" DAY), DAY)
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL 0 DAY), DAY)
        AND NOMBRE_CARGA = 'TXT_CM_DIARIO'
        and ESTADO_CARGA = 'FIN'
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        UNION ALL
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM (
        SELECT NOMBRE_CARGA, REGISTROS_AFECTADOS, ESTADO_CARGA,FECHA_CARGA
        FROM amco-cm-qa.GRAL.ETL_LOG
        --WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL """+str(diasAtras)+""" DAY), DAY)
        WHERE DATETIME_TRUNC(FECHA_CARGA, DAY) = DATETIME_TRUNC(DATE_SUB(CURRENT_DATETIME(), INTERVAL 0 DAY), DAY)
        AND NOMBRE_CARGA = 'TXT_CM_DIARIO_COM'
        ORDER BY FECHA_CARGA DESC
        LIMIT 1
        )
        ORDER BY FECHA_CARGA DESC
        """
    )
    resId = query_id.result()
    
    for reg in resId:
        # Asignacion de variables 
        if reg.NOMBRE_CARGA == 'TXT_CM_DIARIO':
            REG_TXT_CM_DIARIO = reg.REGISTROS_AFECTADOS
            EST_TXT_CM_DIARIO = reg.ESTADO_CARGA
            FD_TXT_CM_DIARIO = reg.FECHA_CARGA
        elif reg.NOMBRE_CARGA == 'TXT_CM_DIARIO_COM':
            REG_TXT_CM_DIARIO_COM = reg.REGISTROS_AFECTADOS
            EST_TXT_CM_DIARIO_COM = reg.ESTADO_CARGA
            FD_TXT_CM_DIARIO_COM = reg.FECHA_CARGA
            
        if reg.FECHA_CARGA >= fechaMax:
            fechaMax = reg.FECHA_CARGA
            ultimoPro = reg.NOMBRE_CARGA
    
    logging.info('Ultimo proceso ejecutado: ' + ultimoPro)
    logging.info('Hora de ultima actividad del proceso: ' + str(fechaMax))
    
    if ultimoPro != 'TXT_CM_DIARIO_COM':
        if EST_TXT_CM_DIARIO_COM == 'INICIO':
            logging.info('El proceso de generacion de TXT esta corriendo')
        else:
            logging.info('El proceso se puede ejecutar despues de evaluar que todas las tablas tienen registros')
            if REG_TXT_CM_DIARIO > 0:
                logging.info('Se valido que todas las tablas estan cargadas....')
                logging.info('Inserta en la ETL Log para avisar del inicio de la generacion de los TXT')
                client = bigquery.Client(project=PROYECTO)
                query_id = client.query(
                    """
                    call GRAL.SP_INSERT_INICIO_ETL_LOG ('TXT_CM_DIARIO_COM',
                                                        'Inicio TXT', 
                                                        'ST_REP', 
                                                        'ST_REP',
                                                        'CRON', 
                                                        'LINUX', 
                                                        0, 
                                                        'Inicia el proceso de generar los TXT', 
                                                        'Inicia todo correcto', 
                                                        0) 
                    """
                    )
                
                resId = query_id.result()
                logging.info('Se inicia la generacion de los TXT =D')
                v_per_carga = 1
            else:
                logging.info('Los TXT no se pueden ejecutar por que no todas las tablas tienen registros')
             
    elif ultimoPro == 'TXT_CM_DIARIO_COM' and EST_TXT_CM_DIARIO_COM == 'INICIO':
        logging.info('El proceso de generacion de TXT esta corriendo')
        
    elif ultimoPro == 'TXT_CM_DIARIO_COM' and EST_TXT_CM_DIARIO_COM == 'FIN':
        logging.info('El proceso de generacion de TXT ya se ejecuto')
        
    return v_per_carga
