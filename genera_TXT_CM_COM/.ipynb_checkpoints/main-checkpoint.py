from datetime import date, datetime, timedelta
import os
import threading
import txt_usuarios_completo as usuarios_completo
import txt_suscripciones_completo as suscripciones_completo
import logging
import sys

import generaZIP as generaZIP
import cargaZipGCP as cargaZipGCP
import generaMd5 as generaMd5
import renombraArchivos as renombraArchivos
import confirmaCargaGCP as confirmaCargaGCP
import validarEjecucion as validarEjecucion

sysVarDias = sys.argv[1]

# Variable que nos indica con que fecha se tiene que generar el archivo (Numero de dias atras)
diasAtras = int(sysVarDias)

###### Desarollo ###### 
#path_arc = "/home/jupyter/script_TXT/genera_TXT_CM_COM/"
#key_path = "/home/jupyter/accesos/amco-cm-qa-0e07a4f6f481.json"

###### Produccion ###### ArchivosTXT
path_arc = "/home/txtamco/generatxt/genera_TXT_CM_COM/"
key_path = "/home/txtamco/accesos/amco-cm-qa-0e07a4f6f481.json"

######### Muestra un print con la informacion a procesar ###########
##############################################################################################################################
##############################################################################################################################
##############################################################################################################################
##############################################################################################################################
######### Generar el con para que solo corran los dias 1 y 16 de cada mes -- este tiene que correo despues de la carga diaria
logging.basicConfig(level = logging.INFO, filename = path_arc+'txt_Log.log', format= '[%(levelname)s] (%(threadName)-s %(message)s')

fecha = datetime.now() + timedelta(days=-diasAtras)
fecha = fecha.strftime("%Y%m%d")
diasAtrasQ = diasAtras + 1
fechaQ = datetime.now() + timedelta(days=-diasAtrasQ)
fechaQ = fechaQ.strftime("%Y%m%d")
print ('El archivo se creara con la fecha: ' + fecha)
print ('El query se ejecuta con la fecha: ' + fechaQ)

v_per_carga = 0

v_per_carga = validarEjecucion.validaCarga(diasAtras, key_path, path_arc)
logging.info('El resultado de la funcion de PY es: ' + str(v_per_carga))

####################### Genera los TXT #############################
if v_per_carga == 1:    
    ################### Limpia espacio de trabajo ######################
    os.system('mkdir '+path_arc+'ArchivosTXT')
    os.system('cd '+path_arc+'ArchivosTXT/; '+ 'rm -r '+path_arc+'ArchivosTXT/*')

    ####################### Genera los TXT #############################
    
    print('Inicia a generar los archivos TXT') 

    t1 = threading.Thread(name="usuarios_completo", target=usuarios_completo.genera_archivos, args=(diasAtras, key_path, path_arc, ))
    t2 = threading.Thread(name="suscripciones_completo", target=suscripciones_completo.genera_archivos, args=(diasAtras, key_path, path_arc, ))

    t1.start()
    t2.start()

    t1.join()
    t2.join()

    print('Termino de generar los archivos TXT')
    ##################### Carga los archivos ###########################
    #######renombraArchivos.renombra_arc(diasAtras)
    generaMd5.genera_MD5(diasAtras, key_path, path_arc)
    generaZIP.genera_zip(diasAtras, key_path, path_arc)
    cargaZipGCP.carga_zip(diasAtras, key_path, path_arc)

    ############# Confirma la carga de los archivos a GCP ##############
    confirmaCargaGCP.confirmaCargaGCP(diasAtras, key_path, path_arc)
