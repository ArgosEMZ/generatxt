from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_zip(diasAtras,key_path,path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = -1
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cm-qa"
    BUCKET = 'amco-cm-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
                            """
                            SELECT  TIE.ID_TIENDA as ID_TIENDA,
                                    TIE.TXT_TIENDA, 
                                    PAI.DS_PAIS as DS_PAIS
                            FROM amco-cm-qa.GRAL.DIM_TIENDA TIE
                            JOIN amco-cm-qa.GRAL.DIM_PAIS_TXT pai ON TIE.TXT_ISO_PAIS = pai.ID_REGION
                            """
                           )
    resId = query_id.result()
    
    print ("Se inician a generar los ZIP")
    for reg in resId:
        nombreZip = "txt_"+reg.DS_PAIS+"_Completo_"+fecha+".zip"
        
        nombreArchivo_S = "Usuarios_"+reg.DS_PAIS+"_Completo_"+fecha+".csv"
        nombreMD5_S     = "Usuarios_"+reg.DS_PAIS+"_Completo_"+fecha+".md5_"
        nombreCtl_S     = "Usuarios_"+reg.DS_PAIS+"_Completo_"+fecha+".ctl"
        
        nombreArchivo_C = "Suscripciones_"+reg.DS_PAIS+"_Completo_"+fecha+".csv"
        nombreMD5_C     = "Suscripciones_"+reg.DS_PAIS+"_Completo_"+fecha+".md5_"
        nombreCtl_C     = "Suscripciones_"+reg.DS_PAIS+"_Completo_"+fecha+".ctl"
        
    
        ##############################################################
        ################### Generar archivo ZIP ###################### nombreCtl

        os.system('cd '+path_arc+'ArchivosTXT/; '+
                  'zip '+nombreZip+
                  ' '+nombreArchivo_S+' '+nombreMD5_S+' '+nombreCtl_S+
                  ' '+nombreArchivo_C+' '+nombreMD5_C+' '+nombreCtl_C
                 )
    
    print ("Se termino de generar los ZIP")
