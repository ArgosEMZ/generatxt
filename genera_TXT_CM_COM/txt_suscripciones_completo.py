from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_archivos(diasAtras, key_path, path_arc):
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key_path
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cm-qa"
    BUCKET = 'amco-cm-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    diasAtrasQ = diasAtras + 1
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        SELECT  TIE.ID_TIENDA as ID_TIENDA,
                TIE.TXT_TIENDA, 
                PAI.DS_PAIS as DS_PAIS
        FROM amco-cm-qa.GRAL.DIM_TIENDA TIE
        JOIN amco-cm-qa.GRAL.DIM_PAIS_TXT pai ON TIE.TXT_ISO_PAIS = pai.ID_REGION
        """
    )
    resId = query_id.result()
    
    for reg in resId:
    
        nombreArchivo = "Suscripciones_"+reg.DS_PAIS+"_Completo_"+fecha+".csv"
        nombreMD5 = "Suscripciones_"+reg.DS_PAIS+"_Completo_"+fecha+".md5_"
        nombreCtl = "Suscripciones_"+reg.DS_PAIS+"_Completo_"+fecha+".ctl"
        nombreZip = "Suscripciones_"+reg.DS_PAIS+"_Completo_"+fecha+".zip"

        #Query para obtener los datos
        client = bigquery.Client(project=PROYECTO)
        query_job = client.query(
            """
            SELECT 
                  DISTINCT(
                  CONCAT('"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.PAIS AS STRING),'"','""'),''),'"|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.ID_USUARIO AS STRING),'"','""'),''),'"|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.ID_SUSCRIPCION AS STRING),'"','""'),''),'"|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.FD_FECHA_ULTIMA_TRANSACCION AS STRING),'"','""'),''),'"|"', -- "2021-02-14 09:11:21.0"|
                  IFNULL(REPLACE(CAST(FORMAT_DATETIME("%Y-%m-%d %H:%M:%S.0", FD_FECHA_ULTIMA_TRANSAC_LOCAL) AS STRING),'"','""'),''),'"|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.VAL_PRECIO AS STRING),'"','""'),''),'"|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.TXT_PRODUCTO AS STRING),'"','""'),''),'"|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.TXT_SUSCRIPCION AS STRING),'"','""'),''),'"|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.NRO_MSISDN AS STRING),'"','""'),''),'"|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.TXT_CANAL AS STRING),'"','""'),''),'"|',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.TXT_SUSCRIPCION_BAJA AS STRING),'"','""'),''),'|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.CANT_RENOVACIONES AS STRING),'"','""'),''),'"|',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.ID_CANAL_BAJA AS STRING),'"','""'),''),'|',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.TXT_CANAL_BAJA AS STRING),'"','""'),''),'|"',
                  IFNULL(REPLACE(CAST(FORMAT_DATETIME("%Y-%m-%d %H:%M:%S.0", PARSE_DATETIME('%d/%m/%Y %H:%M:%S', CAST(FD_FECHA_SUSCRIPCION AS STRING))) AS STRING),'"','""'),''),'"|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.FD_HORA_SUSCRIPCION AS STRING),'"','""'),''),'"|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.TXT_TIPO_PAGO AS STRING),'"','""'),''),'"|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.TXT_MODALIDAD_PAGO AS STRING),'"','""'),''),'"|',
                  IFNULL(REPLACE(CAST(FORMAT_DATETIME("%Y-%m-%d %H:%M:%S.0", FD_FECHA_CANCELACION) AS STRING),'"','""'),''),'|',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.FD_HORA_CANCELACION AS STRING),'"','""'),''),'|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.TIPO_SERVICIO AS STRING),'"','""'),''),'"|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.REINTENTOS_DE_COBRO AS STRING),'"','""'),''),'"|',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.PINCODE_REDIMIDO AS STRING),'"','""'),''),'|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.DIAS_SUSCRIPCION_ACTIVA AS STRING),'"','""'),''),'"|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.ESTADO AS STRING),'"','""'),''),'"|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.ESTATUS_SUSCRIPCION AS STRING),'"','""'),''),'"|"',
                  IFNULL(REPLACE(CAST(TXT_SUSCRIPCIONES_ACTIVAS_INI.TXT_PROMOCION AS STRING),'"','""'),''),'"|'
                  )) as dato
            FROM amco-cm-qa.ST_PAG.TXT_SUSCRIPCIONES_ACTIVAS_INI TXT_SUSCRIPCIONES_ACTIVAS_INI
            WHERE (TXT_SUSCRIPCIONES_ACTIVAS_INI.ID_TIENDA = """+str(reg.ID_TIENDA)+""" ) 
            --WHERE TXT_SUSCRIPCIONES_ACTIVAS_INI.ID_TIENDA = 268
            and TXT_SUSCRIPCIONES_ACTIVAS_INI.fd_fecha_ext = DATE_SUB(DATE_TRUNC(CURRENT_DATETIME('America/Mexico_City'),DAY), INTERVAL """+str(diasAtrasQ)+""" DAY)
            --and TXT_SUSCRIPCIONES_ACTIVAS_INI.fd_fecha_ext = DATE_SUB(DATE_TRUNC(CURRENT_DATETIME('America/Mexico_City'),DAY), INTERVAL 1 DAY)
            """
        )

        results = query_job.result()

        f = open(path_arc+'ArchivosTXT/' + nombreArchivo, 'w' ,encoding='UTF8')
        ## Primero escribir el cabecero
        f.write('PAIS|ID_USUARIO|ID_SUSCRIPCION|FD_FECHA_ULTIMA_TRANSACCION|FD_FECHA_ULTIMA_TRANSAC_LOCAL|VAL_PRECIO|TXT_PRODUCTO|TXT_SUSCRIPCION|NRO_MSISDN|TXT_CANAL|TX_SUSCRIPCION_BAJA|CANT_RENOVACIONES|ID_CANAL_BAJA|TXT_CANAL_BAJA|FECHA_SUSCRIPCION|HORA_SUSCRIPCION|MEDIO_PAGO_ASOCIADO|TXT_MODALIDAD_PAGO|FECHA_CANCELACION|HORA_CANCELACION|TIPO_SERVICIO|REINTENTOS_COBRO|PINCODE_REDIMIDO|NRO_DIAS_SUS_ACTIVA|ESTADO_USUARIO|ESTATUS_SUSCRIPCION|TXT_PROMOCION|MOTIVO_CANCELACION')
        f.write('\r\n')    
            
        for row in results:
            registro = row.dato
            f.write(registro)
            f.write('\n')
            totalReg += 1
            
        f.close()    
        print ("Se genero el archivo correctamente: " + nombreArchivo)
        print ("Total de registros " + str(totalReg))
    
        ################### Generar archivo CTL ######################
        os.system("echo \""+str(totalReg)+"\" >> "+path_arc+"ArchivosTXT/"+nombreCtl)

        totalReg = 0
