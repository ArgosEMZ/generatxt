from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def genera_archivos(diasAtras):
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-diasAtras)
    fecha = fecha.strftime("%Y%m%d")
    PROYECTO = "amco-cm-qa"
    BUCKET = 'amco-cm-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    diasAtrasQ = diasAtras + 1
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
        """
        SELECT  TIE.ID_TIENDA as ID_TIENDA,
                TIE.TXT_TIENDA, 
                PAI.DS_PAIS as DS_PAIS
        FROM amco-cm-qa.GRAL.DIM_TIENDA TIE
        JOIN amco-cm-qa.GRAL.DIM_PAIS_TXT pai ON TIE.TXT_ISO_PAIS = pai.ID_REGION
        """
    )
    resId = query_id.result()
    
    for reg in resId:
    
        nombreArchivo = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreMD5 = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".md5_"
        nombreCtl = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".ctl"
        nombreZip = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".zip"

        #Query para obtener los datos
        client = bigquery.Client(project=PROYECTO)
        query_job = client.query(
            """
            SELECT  DISTINCT 
                    CAST(TXT_USUARIOS_VW.PAIS AS STRING) PAIS,
                    CAST(TXT_USUARIOS_VW.ID_USUARIO AS STRING) ID_USUARIO,
                    CAST(TXT_USUARIOS_VW.NOMBRE ||' '|| TXT_USUARIOS_VW.APELLIDO AS STRING) NOMBRE_COMPLETO,
                    CAST(TXT_USUARIOS_VW.TX_MAIL AS STRING) TX_MAIL,
                    CAST(TXT_USUARIOS_VW.NRO_MSISDN AS STRING) NRO_MSISDN,
                    CAST(TXT_USUARIOS_VW.TX_CANAL AS STRING) TX_CANAL,
                    CAST(TXT_USUARIOS_VW.FECHA_ALTA AS STRING) FECHA_ALTA,
                    CASE WHEN DATE_TRUNC(TXT_USUARIOS_VW.FECHA_ALTA,DAY) = DATE_SUB(DATE_TRUNC(CURRENT_DATETIME('America/Mexico_City'), DAY), INTERVAL """+str(diasAtrasQ)+""" DAY) THEN '1' ELSE '0' END    INDICADOR_ALTA
            FROM amco-cm-qa.ST_CTE.TXT_USUARIOS_VW TXT_USUARIOS_VW  
            WHERE (TXT_USUARIOS_VW.ID_TIENDA = """+str(reg.ID_TIENDA)+""")
            --WHERE (TXT_USUARIOS_VW.ID_TIENDA = 273)
            And TXT_USUARIOS_VW.FECHA_LOCAL = DATE_SUB(DATE_TRUNC(CURRENT_DATETIME('America/Mexico_City'),DAY), INTERVAL """+str(diasAtrasQ)+""" DAY)
            --And DATE_TRUNC(TXT_USUARIOS_VW.FECHA_LOCAL, DAY) = DATE_SUB(DATE_TRUNC(CURRENT_DATETIME('America/Mexico_City'),DAY), INTERVAL 2 DAY)
            """
        )

        results = query_job.result()

        with open('ArchivosTXT/' + nombreArchivo, 'w' ,encoding='UTF8') as f:
            
            
            for row in results:
                if totalReg == 0:
                    ## Primero escribir el cabecero
                    writer = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC, delimiter='|', quotechar='\"', lineterminator='\n')
                    registro = ["PAIS","ID_USUARIO","NOMBRE_COMPLETO","TX_MAIL","NRO_MSISDN","TX_CANAL","FECHA_ALTA","INDICADOR_ALTA"]
                    writer.writerow(registro)
                
                registro = [row.PAIS, row.ID_USUARIO, row.NOMBRE_COMPLETO, row.TX_MAIL, row.NRO_MSISDN, row.TX_CANAL, row.FECHA_ALTA, row.INDICADOR_ALTA]

                writer = csv.writer(f,quoting=csv.QUOTE_NONNUMERIC,delimiter='|', quotechar='\"', lineterminator='\n')
                writer.writerow(registro)
                totalReg += 1
        print ("Se genero el archivo correctamente: " + nombreArchivo)
        print ("Total de registros " + str(totalReg))
    
        ################### Generar archivo CTL ######################
        os.system("echo \""+str(totalReg)+"\" >> ArchivosTXT/"+nombreCtl)

        totalReg = 0
