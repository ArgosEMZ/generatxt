from google.cloud import bigquery
from google.cloud import storage
from datetime import date, datetime, timedelta
import csv  
import os

def renombra_arc():
    
    totalReg = 0
    fecha = datetime.now() + timedelta(days=-2)
    fecha = fecha.strftime("%Y%m%d")
    fechaO = datetime.now() + timedelta(days=-1)
    fechaO = fechaO.strftime("%Y%m%d")
    PROYECTO = "amco-cm-qa"
    BUCKET = 'amco-cm-qa'
    dataset_id = "GRAL"
    table_id = "ETL_LOG"
    
    client = bigquery.Client(project=PROYECTO)
    query_id = client.query(
                            """
                            select  ID_PAIS as ID_PAIS, -- ID_TIENDA
                                    DS_PAIS as DS_PAIS
                            from amco-cm-qa.GRAL.DIM_PAIS_TXT
                            where id_pais not in (760, 759)
                            """
                           )
    resId = query_id.result()
    
    for reg in resId:
        
        nombreArchivo_S = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreCtl_S     = "Suscripciones_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_C = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreCtl_C     = "Catalogo_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_D = "Descargas_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreCtl_D     = "Descargas_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_R = "Reproducciones_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreCtl_R     = "Reproducciones_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_U = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreCtl_U     = "Usuarios_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_P = "Catalogo_Playlist_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreCtl_P     = "Catalogo_Playlist_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        nombreArchivo_Y = "Catalogo_Canciones_Playlist_"+reg.DS_PAIS+"_"+fecha+".csv"
        nombreCtl_Y     = "Catalogo_Canciones_Playlist_"+reg.DS_PAIS+"_"+fecha+".ctl"
        
        
        
        
        nombreArchivo_S_O = "Suscripciones_"+reg.DS_PAIS+"_"+fechaO+".csv"
        nombreCtl_S_O     = "Suscripciones_"+reg.DS_PAIS+"_"+fechaO+".ctl"
        
        nombreArchivo_C_O = "Catalogo_"+reg.DS_PAIS+"_"+fechaO+".csv"
        nombreCtl_C_O     = "Catalogo_"+reg.DS_PAIS+"_"+fechaO+".ctl"
        
        nombreArchivo_D_O = "Descargas_"+reg.DS_PAIS+"_"+fechaO+".csv"
        nombreCtl_D_O     = "Descargas_"+reg.DS_PAIS+"_"+fechaO+".ctl"
        
        nombreArchivo_R_O = "Reproducciones_"+reg.DS_PAIS+"_"+fechaO+".csv"
        nombreCtl_R_O     = "Reproducciones_"+reg.DS_PAIS+"_"+fechaO+".ctl"
        
        nombreArchivo_U_O = "Usuarios_"+reg.DS_PAIS+"_"+fechaO+".csv"
        nombreCtl_U_O     = "Usuarios_"+reg.DS_PAIS+"_"+fechaO+".ctl"
        
        nombreArchivo_P_O = "Catalogo_Playlist_"+reg.DS_PAIS+"_"+fechaO+".csv"
        nombreCtl_P_O     = "Catalogo_Playlist_"+reg.DS_PAIS+"_"+fechaO+".ctl"
        
        nombreArchivo_Y_O = "Catalogo_Canciones_Playlist_"+reg.DS_PAIS+"_"+fechaO+".csv"
        nombreCtl_Y_O     = "Catalogo_Canciones_Playlist_"+reg.DS_PAIS+"_"+fechaO+".ctl"
        
        
        ##############################################################
        ################### Generar archivo ZIP ###################### nombreCtl

        os.system('cd ArchivosTXT/; '+'mv '+nombreArchivo_S_O+' '+nombreArchivo_S)
        os.system('cd ArchivosTXT/; '+'mv '+nombreCtl_S_O+' '+nombreCtl_S)
        
        os.system('cd ArchivosTXT/; '+'mv '+nombreArchivo_C_O+' '+nombreArchivo_C)
        os.system('cd ArchivosTXT/; '+'mv '+nombreCtl_C_O+' '+nombreCtl_C)
        
        os.system('cd ArchivosTXT/; '+'mv '+nombreArchivo_D_O+' '+nombreArchivo_D)
        os.system('cd ArchivosTXT/; '+'mv '+nombreCtl_D_O+' '+nombreCtl_D)
        
        os.system('cd ArchivosTXT/; '+'mv '+nombreArchivo_R_O+' '+nombreArchivo_R)
        os.system('cd ArchivosTXT/; '+'mv '+nombreCtl_R_O+' '+nombreCtl_R)
        
        os.system('cd ArchivosTXT/; '+'mv '+nombreArchivo_U_O+' '+nombreArchivo_U)
        os.system('cd ArchivosTXT/; '+'mv '+nombreCtl_U_O+' '+nombreCtl_U)
        
        os.system('cd ArchivosTXT/; '+'mv '+nombreArchivo_P_O+' '+nombreArchivo_P)
        os.system('cd ArchivosTXT/; '+'mv '+nombreCtl_P_O+' '+nombreCtl_P)
        
        os.system('cd ArchivosTXT/; '+'mv '+nombreArchivo_Y_O+' '+nombreArchivo_Y)
        os.system('cd ArchivosTXT/; '+'mv '+nombreCtl_Y_O+' '+nombreCtl_Y)
        
        #print ('cd ArchivosTXT/; '+'mv '+nombreArchivo_S_O+' '+nombreArchivo_S)
    
    print ("Se termino de renombrar los archivos")
